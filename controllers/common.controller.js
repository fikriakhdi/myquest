const User = require('../models/user.model')
const Quest = require('../models/quest.model')
const UserQuest = require('../models/user_quest.model')
const Order = require('../models/order.model')
const Feature = require('../models/feature.model')
const htmlToText = require('html-to-text')
const Setting = require('../models/setting.model')
const Testimonial = require('../models/testimonial.model')
const SurveyProfiling = require('../models/survey_profiling.model')
const Message = require('../models/message.model')
const midtransClient = require('midtrans-client')
var device = require('express-device');
const PreviewQuest = require('../models/preview_quest.model')
const Faq = require('../models/faq.model')
const News = require('../models/news.model')
var smtpTransport = require('nodemailer-smtp-transport')
const Session = require('../models/session.model')
var dateFormat = require('dateformat')
var crypto = require('crypto')
var nodemailer = require('nodemailer')
const mongoose = require('mongoose')
var request = require('request')
var async = require('async')
const ObjectId = mongoose.Types.ObjectId
var fs = require('fs')
var handlebars = require('hbs')
var readHTMLFile = function (path, callback) {
  fs.readFile(path, {
    encoding: 'utf-8'
  }, function (err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};
let snap = new midtransClient.Snap({
  isProduction: true,
  serverKey: 'Mid-server-QJ0eCbcMOEl-XFpKGt74eM2A',
  clientKey: 'Mid-client-XiKTtLD7yrmd2ZNW'
});
var transporter = nodemailer.createTransport(
  smtpTransport({
    service: 'Zoho',
    // auth: {
    //   user: "myquestindo@gmail.com",
    //   pass: "imgsatujiwa"
    // }
    // host: 'smtp.zoho.com',
    // port: 465,
    // secure: true, // use SSL
    auth: {
      user: "admin@myquest.id",
      pass: "imgsatujiwa"
    }
    // secureConnection: 'false',
    // tls: {
    //   ciphers: 'SSLv3',
    //   rejectUnauthorized: false

    // }

  })
);
// let snap = new midtransClient.Snap({
//   isProduction : false,
//   serverKey : 'SB-Mid-server-sShrl8RDY1Vi7uABiNEKnnj3',
//   clientKey : 'SB-Mid-client-y-sc0eIevHaYrBJZ'
// });
function random_password() {
  var pass = (Math.random().toString(36).substr(2)).toUpperCase()
  pass = crypto.createHash('md5').update(pass).digest('hex')
  return pass.substr(0, 5)
}

//Dashboard checking
exports.main_page = function (req, res) {
  var info = ""
  var info_type = ""
  var info_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  res.locals.rating = req.flash('rating')
  if (typeof res.locals.info != 'undefined') {
    var info = res.locals.info
    var info_type = res.locals.info_type
    var info_name_title = res.locals.info_title
  }
  var user_data = req.session.user_data
  var locals = {}
  async.parallel([
    function (callback) {
      Setting.findOne({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.setting = result
        callback()
      })
    },
    function (callback) {
      Feature.find({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.feature = result
        callback()
      })
    },
    function (callback) {
      Testimonial.find({confirmed:true}).limit(5).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.testimonial = result
        callback()
      })
    },
    function (callback) {
      Faq.find({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.faq = result
        callback()
      })
    },
    function (callback) {
      News.find({}).limit(3).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.news = result
        callback()
      })
    },
    function (callback) {
      Quest.find({status:"published"}).limit(6).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.quests = result
        callback()
      })
    }
    ,
    function (callback) {
      if(user_data){
      UserQuest.find({userRef:user_data._id}).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.user_quest = result
        callback()
      })
    } else {
      callback()
    }
    }
  ], function (err) {
    console.log(locals.faq)

    console.log(req.session.user_data)
    console.log(req.session.user_login)
    //filter quests
    var finalFiltered = []
    if(locals.user_quest){
      if (locals.user_quest && locals.quests) {
        locals.quests.map((o) => {
          var answered = locals.user_quest.find((p) => p.questRef.toString() === o._id.toString())
          if (answered) {} else finalFiltered.push(o)
        })
      }
    } else {
      finalFiltered = locals.quests
    }
    res.render('home', {
      name_title: appName + " | " + "Home",
      appName: appName,
      setting_data: locals.setting,
      feature: locals.feature,
      testimonial: locals.testimonial,
      news: locals.news,
      faq: locals.faq,
      quests:finalFiltered,
      user_data: user_data,
      info: info,
      info_type: info_type,
      info_name_title: info_name_title
    })
  })
}

exports.writeRating = function (req, res) {
  var user_data = req.session.user_data
  var dataCreate = {
    name: (user_data ? user_data.name : "Anonymous"),
    caption: req.body.caption,
    image: (user_data ? (user_data.imageProfile ? user_data.imageProfile : "/img/user.png") : "/img/user.png"),
    stars: req.body.rating,
    confirmed:false,
    userRef: (user_data ? user_data._id : null),
    createdDate: Date.now(),
    createdBy: (user_data ? user_data._id : null)
  }
  Testimonial.create(dataCreate, function (err, testimonial) {
    if (err || testimonial == null) {
      req.flash('info_title', 'Information')
      req.flash('info', 'Failed to write a feedback')
      req.flash('info_type', 'error')
      res.redirect('/')
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', 'Thank you so much for your feedback!')
      req.flash('info_type', 'success')
      res.redirect('/')
    }
  })
}

//forgot password
exports.forgot_password = function (req, res) {
  res.render('forgot_password', {
    name_title: appName + " | " + "Forgot Password"
  })
}

//signin
exports.signin = function (req, res) {
  var info = ""
  var info_type = ""
  var info_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    var info = res.locals.info
    var info_type = res.locals.info_type
    var info_name_title = res.locals.info_title
  }
  var redirect = req.params.redirect
  sess = req.session
  sess.redirect = redirect
  // console.log(sess.redirect)
  if (typeof res.locals.info != 'undefined') {
    info = res.locals.info
  }
  res.render('signin', {
    name_title: appName + " | " + "SignIn",
    info: info,
    info_type: info_type,
    info_name_title: info_name_title
  })
}

//signin
exports.signin_process = function (req, res) {
  var email = req.body.email
  var password = req.body.password
  sess = req.session
  var redirect = sess.redirect
  var locals = {}
  User.authenticate(email, password, "member", function (error, user) {
    if (error || !user || user.length <= 0) {
      console.log(error, user)
      var ret = {
        status: 400,
        message: "Signin failed, please check your email/password"
      }
      return res.send(JSON.stringify(ret))
    } else {
      if (user.status === 'active') {
        locals.user = user
        var sessionData = {
          token: crypto.createHash('md5').update(req.sessionID + (new Date(user.expire))).digest('hex'),
          user: user._id,
          createdDate: Date.now(),
        }
        var options = {
          upsert: true,
          new: true,
          setDefaultsOnInsert: true
        }
        Session.findOneAndUpdate({
          user: ObjectId(user._id)
        }, sessionData, options, function (error, session) {
          if (!error || session != null) {
            console.log("token:", session.token)
            sess.user_login = email
            sess.user_data = user
            sess.key = session.token
            console.log(sess.user_data)
            console.log(sess.user_login)
            var ret = {
              status: 200,
              message: "Sign In Successful"
            }
            return res.send(JSON.stringify(ret))
          } else {
            var ret = {
              status: 400,
              message: "Sign In Failed"
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var ret = {
          status: 400,
          message: "Your account isn't active yet, please check your email to activate it"
        }
        return res.send(JSON.stringify(ret))
      }
    }
  })
}

//signup
exports.signup = function (req, res) {
  var messages = {}
  res.locals.info = req.flash('info')
  var redirect = req.params.redirect
  sess = req.session
  sess.redirect = redirect
  // console.log(sess.redirect)
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info
  }
  Setting.findOne({}, function (err, setting) {
    if (err) console.log(err)

    res.render('signup', {
      name_title: appName + " | " + "SignUp",
      settings_data: setting,
      messages: messages,
      messages_type: 'danger'
    })
  })
}

//signup process
exports.signup_process = function (req, res) {
  if (req.body.name && req.body.email && req.body.pass && req.body.re_pass) {
    if (req.body.pass == req.body.re_pass) {
      //find if email is already registered
      User.findOne({
        email: req.body.email
      }, function (err, user) {
        if (!err && user == null) {
          var dataCreate = {
            name: req.body.name,
            email: req.body.email,
            password: crypto.createHash('md5').update(req.body.pass).digest('hex'),
            status: 'unactive',
            role: 'member',
            createdDate: Date.now()
          }
          User.create(dataCreate, function (err, user) {
            if (err || user == null) {
              var ret = {
                status: 400,
                message: "Something went wrong"
              }
              return res.send(JSON.stringify(ret))
            } else {
              Setting.findOne({}, function (err, settings) {
                if (err) {
                  console.log(err)
                  var err = {
                    status: 400,
                    message: "Something went wrong"
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  //Send Email for verification
                  var BASE_DIR = __dirname.replace('\controllers', '')
                  readHTMLFile(BASE_DIR + '/assets/html_template/verification.html', function (err, html) {
                    var template = handlebars.compile(html);
                    var replacements = {
                      user_name: user.name,
                      user_email: user.email,
                      appName: appName,
                      confirmation_url: webURL + "verification/" + user._id,
                      year: new Date().getFullYear(),
                      email_admin: settings.email,
                      phone_admin: settings.phone,
                      address_admin: settings.address
                    };
                    var htmlToSend = template(replacements);
                    var mailOptions = {
                      form: 'Administrator <' + settings.email + '>',
                      to: user.email,
                      subject: appName + ' Account Activation',
                      html: htmlToSend
                    }
                    transporter.sendMail(mailOptions, function (error, info) {
                      if (error) {
                        console.log(error)
                        var err = {
                          status: 400,
                          message: "Failed to send email"
                        }
                        return res.send(JSON.stringify(err))
                      } else {
                        var ret = {
                          status: 200,
                          message: `Sign Up Successful, please check your email to activate your account.`
                        }
                        return res.send(JSON.stringify(ret))
                      }
                    })
                  })
                }
              })
            }
          })
        } else {
          var ret = {
            status: 400,
            message: "Email is already registered"
          }
          return res.send(JSON.stringify(ret))
        }
      })
    } else {
      var ret = {
        status: 400,
        message: "Password doesn't match"
      }
      return res.send(JSON.stringify(ret))
    }
  } else {
    var ret = {
      status: 400,
      message: "All fields is required"
    }
    return res.send(JSON.stringify(ret))
  }
}

//signout
exports.signout = function (req, res) {
  Session.findOneAndRemove({
    token: req.session.key
  }, function (error) {
    if (error) {
      console.log('Login Berhasil')
    } else {
      req.session.destroy(function (err) {
        if (err) {
          console.log(err)
        } else {
          res.redirect('/')
        }
      })
    }
  })
}




// POST forgot password
exports.forgot_password_process = function (req, res, next) {
  if (req.body.email) {
    Setting.findOne({}, function (err, settings) {
      if (err) console.log(err)
      User.findOne({
        email: req.body.email
      }, {}, function (err, user) {
        if (err || !user) {
          var err = {
            status: 400,
            message: "Email is not registered"
          }
          return res.send(JSON.stringify(err))
        } else {
          var newpass = random_password()
          User.findOneAndUpdate({
            _id: user._id
          }, {
            password: crypto.createHash('md5').update(newpass).digest('hex')
          }, function (error, usr) {
            if (error) {
              var err = {
                status: 400,
                message: error
              }
              return res.send(JSON.stringify(err))
            } else {
              var BASE_DIR = __dirname.replace('\controllers', '')
              readHTMLFile(BASE_DIR + '/assets/html_template/forgot_password.html', function (err, html) {
                var template = handlebars.compile(html);
                var replacements = {
                  appName: appName,
                  user_name: usr.name,
                  user_email: req.body.email,
                  new_password: newpass,
                  year: new Date().getFullYear(),
                  email_admin: settings.email,
                  phone_admin: settings.phone
                };
                var htmlToSend = template(replacements);
                var mailOptions = {
                  from: 'Administrator <' + settings.email + '>',
                  to: req.body.email,
                  subject: appName + ' Account Forgot Password',
                  html: htmlToSend
                }
                //   var mailOptions = {
                //     from: '"Our Code World " <admin@myquest.id>', // sender address (who sends)
                //     to: 'fikriakhdi22@gmail.com, dwiky.01.oke@gmail.com', // list of receivers (who receives)
                //     subject: 'Hello ', // Subject line
                //     text: 'Hello world ', // plaintext body
                //     html: '<b>Hello world </b><br> This is the first email sent with Nodemailer in Node.js' // html body
                // };
                // console.log(mailOptions)
                transporter.sendMail(mailOptions, function (error, info) {
                  if (error) {
                    console.log(error)
                    var err = {
                      status: 400,
                      message: "Failed to send the email"
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var ret = {
                      status: 200,
                      message: 'Email Sent. Please open your email to reset password'
                    }
                    return res.send(JSON.stringify(ret))
                  }
                })
              })
            }
          })
        }
      })
    })
  } else {
    var err = {
      status: 400,
      message: "Email is required"
    }
    return res.send(JSON.stringify(err))
  }
}

// GET admin reset_password
exports.reset_password = function (req, res, next) {
  User.findOne({
      email: req.params.email,
      password: req.params.rstcode
    }, {
      '__v': 0
    })
    .exec(function (error, user) {
      var baseUrl = req.headers.host + '/' + req.originalUrl.split('/')[1]
      res.render('reset_password', {
        name_title: appName + " | " + "Reset Password",
        email: req.params.email,
        postUrl: 'https://' + baseUrl + '/reset_password'
      })
    })
}

// POST admin reset_password_process
exports.reset_password_process = function (req, res, next) {
  User.findOne({
      email: req.body.email
    }, {
      '__v': 0
    })
    .exec(function (error, user) {
      if (req.body.password != '' && req.body.conf_password != '' && req.body.password == req.body.conf_password) {
        var userData = {
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          updatedDate: Date.now(),
          upeatedBy: user._id,
        }
        User.findOneAndUpdate({
          _id: user._id
        }, userData, function (error, user) {
          if (error || user == null) {
            var err = {
              status: 400,
              message: error
            }
            return res.send(JSON.stringify(err))
          } else {
            req.flash('info', 'Reset Password Success')
            req.flash('info_type', 'success')
            res.redirect('/forgot_password')
          }
        })
      } else {
        req.flash('info', 'Reset Password Failed')
        req.flash('info_type', 'danger')
        res.redirect('/forgot_password')
      }
    })
}

//verification
exports.verification = function (req, res) {
  if (req.params.id) {
    User.findOne({
      _id: req.params.id,
      status: 'unactive'
    }, function (err, user) {
      if (err || user == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid link')
        req.flash('info_type', 'error')
        res.redirect('/signin')
      } else {
        var dataUpdate = {
          status: 'active',
          updatedDate: Date.now()
        }
        User.findOneAndUpdate({
          _id: user._id
        }, dataUpdate, function (err, user) {
          if (err || user == null) {
            req.flash('info_title', 'Information')
            req.flash('info', 'Failed to activate the account')
            req.flash('info_type', 'error')
            res.redirect('/signin')
          } else {
            req.flash('info_title', 'Information')
            req.flash('info', 'Account Activation successful')
            req.flash('info_type', 'success')
            res.redirect('/signin')
          }
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/signin')
  }
}

//my_profile
exports.my_profile = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var info = ""
  var info_type = ""
  var info_title = ""
  var locals = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    info = res.locals.info
    info_type = res.locals.info_type
    info_title = res.locals.info_title
  }
  async.parallel([
    function (callback) {
      Quest.find({
        userRef: user_data._id
      }).count().exec(function (err, result) {
        if (err) return callback(err)
        locals.total_created_quest = result
        callback()
      })
    },
    function (callback) {
      UserQuest.find({
        userRef: user_data._id
      }).count().exec(function (err, result) {
        if (err) return callback(err)
        locals.total_answered_quest = result
        callback()
      })
    },
    function (callback) {
      Order.find({
        userRef: user_data._id
      }).count().exec(function (err, result) {
        if (err) return callback(err)
        locals.total_order = result
        callback()
      })
    },
    function (callback) {
      SurveyProfiling.find({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.survey_profiling = result
        callback()
      })
    }
  ], function (err) {
    if (err) console.log(err)
    res.render('my_profile', {
      appName: appName,
      name_title: appName + " | " + "My Profile",
      user_data: user_data,
      survey_profiling: locals.survey_profiling,
      total_created_quest: locals.total_created_quest,
      total_answered_quest: locals.total_answered_quest,
      total_order: locals.total_order,
      info: info,
      info_type: info_type,
      info_title: info_title
    })
  })
}

//Update PP
exports.change_pp = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data

  var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
  var dataUpdate = {
    updatedDate: Date.now(),
    updatedBy: user_data._id,
  }
  if (file_url) dataUpdate.imageProfile = file_url
  console.log(dataUpdate)
  User.findByIdAndUpdate(user_data._id, dataUpdate, function (err, user) {
    if (!err) {
      User.findById(user_data._id, function (err, user) {
        if (!err) {
          req.session.user_data = user
          req.flash('info_title', 'Information')
          req.flash('info', 'Update Profile Success')
          req.flash('info_type', 'success')
          res.redirect('/my_profile')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Update Profile Failed')
          req.flash('info_type', 'error')
          res.redirect('/my_profile')
        }
      })
    } else {
      console.log(err)
      console.log("DISINI 3")
      req.flash('info_title', 'Information')
      req.flash('info', 'Update Profile Failed')
      req.flash('info_type', 'error')
      res.redirect('/my_profile')
    }
  })
}

//update profile
exports.update_profile = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  SurveyProfiling.find({}, function (err, survey_profiling) {
    if (err || survey_profiling == null) {
      req.flash('info_title', 'Information')
      req.flash('info', 'Invalid link')
      req.flash('info_type', 'error')
      res.redirect('/signin')
    } else {
      var profiling = []
      survey_profiling.map((o) => {
        var input = req.body[o._id]
        var new_obj = {
          surveyProfiling: o._id,
          answer: input.toString()
        }
        if (input != null) profiling.push(new_obj)
      })
      var dataUpdate = {
        name: req.body.name,
        profiling: profiling,
        updatedDate: Date.now(),
        updatedBy: user_data._id,
      }
      User.findByIdAndUpdate(user_data._id, dataUpdate, function (err, user) {
        if (!err) {
          User.findById(user_data._id, function (err, user) {
            if (!err) {
              req.session.user_data = user
              req.flash('info_title', 'Information')
              req.flash('info', 'Update Profile Success')
              req.flash('info_type', 'success')
              res.redirect('/my_profile')
            } else {
              req.flash('info_title', 'Information')
              req.flash('info', 'Update Profile Failed')
              req.flash('info_type', 'error')
              res.redirect('/my_profile')
            }
          })
        } else {
          console.log(err)
          console.log("DISINI 3")
          req.flash('info_title', 'Information')
          req.flash('info', 'Update Profile Failed')
          req.flash('info_type', 'error')
          res.redirect('/my_profile')
        }
      })
    }
  })
}

//Change password process
exports.change_password = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.body.old_password && req.body.new_password) {
    var old_password = crypto.createHash('md5').update(req.body.old_password).digest('hex')
    var new_password = crypto.createHash('md5').update(req.body.new_password).digest('hex')
    User.findOne({
      _id: user_data._id,
      password: old_password
    }, function (err, user) {
      if (err || user == null) {
        var ret = {
          status: 400,
          message: "Old password doesn't match"
        }
        return res.send(JSON.stringify(ret))
      } else {
        var dataUpdate = {
          password: new_password,
          updatedDate: Date.now(),
          updatedBy: user._id
        }
        User.findOneAndUpdate({
          _id: user._id
        }, dataUpdate, function (err, user) {
          if (!err && user != null) {
            var ret = {
              status: 200,
              message: `Change Password Success`
            }
            return res.send(JSON.stringify(ret))
          } else {
            var ret = {
              status: 400,
              message: "Change Password Failed"
            }
            return res.send(JSON.stringify(ret))
          }
        })
      }
    })
  } else {
    var ret = {
      status: 400,
      message: "All fields is required"
    }
    return res.send(JSON.stringify(ret))
  }
}

//news
exports.news = function (req, res) {
  var user_data = req.session.user_data
  var locals = {}
  async.parallel([
    function (callback) {
      Setting.findOne({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.setting = result
        callback()
      })
    },
    function (callback) {
      News.find({}).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.news = result
        callback()
      })
    }
  ], function (err) {
    console.log(locals.faq)
    res.render('news_member', {
      name_title: appName + " | " + "News",
      appName: appName,
      setting_data: locals.setting,
      news: locals.news,
      faq: locals.faq,
      user_data: user_data
    })
  })
}

//single news
exports.single_news = function (req, res) {
  var user_data = req.session.user_data
  var locals = {}
  async.parallel([
    function (callback) {
      Setting.findOne({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.setting = result
        callback()
      })
    },
    function (callback) {
      News.findOne({
        _id: req.params.id
      }).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.news = result
        callback()
      })
    }
  ], function (err) {
    console.log(locals.faq)
    res.render('single_news', {
      name_title: appName + " | " + locals.news.title,
      appName: appName,
      setting_data: locals.setting,
      news: locals.news,
      faq: locals.faq,
      user_data: user_data
    })
  })
}

//send message
exports.send_message = function (req, res) {
  if (req.body.subject && req.body.email && req.body.name && req.body.message) {
    var dataCreate = {
      name: req.body.name,
      email: req.body.email,
      subject: req.body.subject,
      message: req.body.message,
      createdDate: Date.now()
    }
    Message.create(dataCreate, function (err, message) {
      if (err || message == null) {
        var ret = {
          status: 400,
          message: "Sending message failed"
        }
        return res.send(JSON.stringify(ret))
      } else {
        var ret = {
          status: 200,
          message: "Message sent!"
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var ret = {
      status: 400,
      message: "All field is required"
    }
    return res.send(JSON.stringify(ret))
  }
}

//profiling
exports.profiling = function (req, res) {
  User.findOne({
    _id: req.params.id,
    status: "unactive"
  }, function (err, user) {
    if (err || user == null) {
      req.flash('info_title', 'Information')
      req.flash('info', 'Invalid link')
      req.flash('info_type', 'error')
      res.redirect('/signin')
    } else {
      var locals = {}
      async.parallel([
        function (callback) {
          SurveyProfiling.find({}).exec(function (err, result) {
            if (err) return callback(err)
            locals.survey_profiling = result
            callback()
          })
        }
      ], function (err) {
        console.log(locals.faq)
        res.render('profiling', {
          name_title: appName + " | Profiling",
          userId: user._id,
          appName: appName,
          survey_profiling: locals.survey_profiling
        })
      })
    }
  })
}

//profiling_process
exports.profiling_process = function (req, res) {
  SurveyProfiling.find({}, function (err, survey_profiling) {
    if (err || survey_profiling == null) {
      req.flash('info_title', 'Information')
      req.flash('info', 'Invalid link')
      req.flash('info_type', 'error')
      res.redirect('/signin')
    } else {
      var profiling = []
      survey_profiling.map((o) => {
        var input = req.body[o._id]
        var new_obj = {
          surveyProfiling: o._id,
          answer: input.toString()
        }
        if (input != null) profiling.push(new_obj)
      })
      console.log(profiling)
      var dataUpdate = {
        status: "active",
        profiling: profiling,
        updatedDate: Date.now()
      }
      User.findOneAndUpdate({
        _id: req.params.id
      }, dataUpdate, function (err, user) {
        if (err || user == null) {
          console.log(err)
          req.flash('info_title', 'Information')
          req.flash('info', 'Invalid link')
          req.flash('info_type', 'error')
          res.redirect('/signin')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Activating account success, now you can use your account!')
          req.flash('info_type', 'success')
          res.redirect('/signin')
        }
      })
    }
  })
}


//Create Quest
exports.create_quest = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  Setting.findOne({}, function (err, setting) {
    if (err) console.log(err)
    res.render('prologue', {
      name_title: appName + " | Create Quest",
      user_data: user_data,
      appName: appName,
      setting_data: setting
    })
  })
}

//Plan
exports.plan = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  Setting.findOne({}, function (err, setting) {
    if (err) console.log(err)
    res.render('plan', {
      name_title: appName + " | Choose Plan",
      user_data: user_data,
      appName: appName,
      setting_data: setting
    })
  })
}

//Create Quest Type
exports.create_quest_type = function (req, res) {
  auth_redirect(req, res)
  var info = ""
  var info_type = ""
  var info_title = ""
  var locals = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    info = res.locals.info
    info_type = res.locals.info_type
    info_title = res.locals.info_title
  }
  var answered_quest = req.session.answered_quest
  var user_data = req.session.user_data
  Setting.findOne({}, function (err, setting) {
    if (err) console.log(err)
    if (req.params.type == "free") {
      if (answered_quest != null) {
        if (answered_quest.length < parseInt(setting.freeQuest)) {
          //find quest according to profiling
          //find quest according to profilingvar where = {
          var where = {
            userRef: (user_data ? user_data._id : null)
          }
          UserQuest.find(where, function (err, user_quest) {
            if (err) console.log(err)
            Quest.find({}, function (err, quest) {
              if (quest) {
                var filtered = []
                quest.map((o) => {
                  // console.log("QUEST", o)
                  if (o.profiling.length > 0) {
                    // Profiling Check
                    console.log("MASUK 1")
                    o.profiling.map((p) => {
                      console.log("MASUK 2")
                      if (user_data) {
                        console.log("MASUK 3")
                        var profiling = user_data.profiling.find((o) => o.surveyProfiling.toString() == p.surveyprofiling.toString())
                        if (profiling) {
                          console.log("MASUK 4")
                          console.log("Profiling", profiling)
                          if (p.minValue && p.maxValue) {
                            console.log("MASUK 5")
                            console.log("Masuk sini")
                            if (profiling.answer >= p.minValue && profiling.answer >= p.maxValue) {
                              console.log("MASUK 6")
                              filtered.push(o)
                            }
                          } else {
                            if (profiling.answer === p.value) {
                              console.log("Masuk sini juga")
                              filtered.push(o)
                            }
                          }
                        }
                      }
                    })
                  } else {
                    console.log("ke push")
                    filtered.push(o)
                  }
                })
                console.log("Quest", filtered)
                //Filtering Answered Quest
                var finalFiltered = []
                if (user_quest && filtered) {
                  filtered.map((o) => {
                    var answered = user_quest.find((p) => p.questRef.toString() === o._id.toString())
                    if (answered) {} else finalFiltered.push(o)
                  })
                }
                res.render('find_quest', {
                  name_title: appName + " | Create Quest (Free)",
                  user_data: user_data,
                  appName: appName,
                  quest_left: parseInt(setting.freeQuest) - answered_quest.length,
                  quest: finalFiltered,
                  info: info,
                  info_title: info_title,
                  info_type: info_type,
                  setting_data: setting
                })
              } else {
                res.render('find_quest', {
                  name_title: appName + " | Create Quest (Free)",
                  user_data: user_data,
                  appName: appName,
                  quest_left: parseInt(setting.freeQuest) - answered_quest.length,
                  quest: quest,
                  info: info,
                  info_title: info_title,
                  info_type: info_type,
                  setting_data: setting
                })
              }
            })
          })
        } else {
          req.session.answered_quest = null
          SurveyProfiling.find({}, function (err, survey_profiling) {
            if (err) console.log(err)
            res.render('create_quest', {
              name_title: appName + " | Create Quest (Free)",
              user_data: user_data,
              appName: appName,
              survey_profiling: survey_profiling,
              type_list: SurveyProfiling.schema.path('type').enumValues,
              info: info,
              info_title: info_title,
              info_type: info_type,
              setting_data: setting
            })
          })
        }
      } else {
        //find quest according to profilingvar where = {
        var where = {
          userRef: (user_data ? user_data._id : null)
        }
        UserQuest.find(where, function (err, user_quest) {
          if (err) console.log(err)
          Quest.find({}, function (err, quest) {
            if (quest) {
              var filtered = []
              quest.map((o) => {
                console.log("Disini 1")
                // Profiling Check
                if(o.profiling.length>0){
                o.profiling.map((p) => {
                  console.log("Disini 2")
                  if (user_data) {
                    console.log("Disini 3")
                    var profiling = user_data.profiling.find((o) => o.surveyProfiling.toString() == p.surveyprofiling.toString())
                    if (profiling) {
                      console.log("Disini 4")
                      console.log("Profiling", profiling)
                      if (p.minValue && p.maxValue) {
                        console.log("Masuk sini")
                        if (profiling.answer >= p.minValue && profiling.answer >= p.maxValue) {
                          filtered.push(o)
                        }
                      } else {
                        if (profiling.answer === p.value) {
                          console.log("Masuk sini juga")
                          filtered.push(o)
                        }
                      }
                    }
                  }
                })
              }  else {
                console.log("Masuk sini juga")
              filtered.push(o)
              }
              })
              console.log("Quest", filtered)
              //Filtering Answered Quest
              var finalFiltered = []
              if (user_quest && filtered) {
                filtered.map((o) => {
                  var answered = user_quest.find((p) => p.questRef.toString() === o._id.toString())
                  if (answered) {} else finalFiltered.push(o)
                })
              }
              res.render('find_quest', {
                name_title: appName + " | Create Quest (Free)",
                user_data: user_data,
                appName: appName,
                quest_left: 0,
                quest: finalFiltered,
                info: info,
                info_title: info_title,
                info_type: info_type,
                setting_data: setting
              })
            } else {
              res.render('find_quest', {
                name_title: appName + " | Create Quest (Free)",
                user_data: user_data,
                appName: appName,
                quest_left: 0,
                quest: quest,
                info: info,
                info_title: info_title,
                info_type: info_type,
                setting_data: setting
              })
            }
          })
        })
      }
    } else {
      //find Order
      Order.findOne({
        orderId: req.params.id,
        used:false,
        questRef:null,
      }, function (err, order) {
        if (err || order == null) {
          req.flash('info_title', 'Information')
          req.flash('info', 'Invalid link')
          req.flash('info_type', 'error')
          res.redirect('/')
        } else {
          if (order.status == "paid") {
            SurveyProfiling.find({}, function (err, survey_profiling) {
              if (err) console.log(err)
              res.render('create_quest', {
                name_title: appName + " | Create Quest (PAID)",
                user_data: user_data,
                appName: appName,
                survey_profiling: survey_profiling,
                type_list: SurveyProfiling.schema.path('type').enumValues,
                info: info,
                info_title: info_title,
                info_type: info_type,
                setting_data: setting
              })
            })
          } else {
            req.flash('info_title', 'Information')
            req.flash('info', 'Invalid link')
            req.flash('info_type', 'error')
            res.redirect('/')
          }
        }
      })
    }
  })
}

//Create Quest
exports.create_quest_process = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var type = (req.params.type ? req.params.type : "")
  var id = (req.params.id ? req.params.id : "")
  if (req.body.title && req.body.question) {
    req.body.question = (req.body.question instanceof Array) ? req.body.question : [req.body.question]
    req.body.type = (req.body.type instanceof Array) ? req.body.type : [req.body.type]
    req.body.required = (req.body.required instanceof Array) ? req.body.required : [req.body.required]
    req.body.number = (req.body.number instanceof Array) ? req.body.number : [req.body.number]
    req.body.custom = (req.body.custom instanceof Array) ? req.body.custom : [req.body.custom]
    req.body.data = (req.body.data instanceof Array) ? req.body.data : [req.body.data]
    req.body.carryForward = (req.body.carryForward instanceof Array) ? req.body.carryForward : [req.body.carryForward]
    req.body.randomize = (req.body.randomize instanceof Array) ? req.body.randomize : [req.body.randomize]
    var questions_raw = req.body.question
    var questions_data = []
    var profiling_data = []
    questions_raw.map((o, i) => {

      var display_logic_data = []
      if (req.body.display_logic[i]) {
        // console.log("DISPLAY LOGIC", req.body.display_logic[i])
        var parsed = JSON.parse(req.body.display_logic[i])
        // console.log(parsed)
        parsed.map((p) => {
          var temp = {
            logic_type: (p.logic_type ? p.logic_type.toString() : ""),
            question: (p.question ? p.question.toString() : ""),
            is_not: (p.is_not ? p.is_not.toString() : ""),
            is_selected: (p.is_selected ? p.is_selected.toString() : ""),
            device: (p.device ? p.device.toString() : ""),
            equal: (p.equal ? p.equal.toString() : ""),
            equal_to: (p.equal_to ? p.equal_to.toString() : ""),
            answer: (p.answer ? p.answer.toString() : ""),
            or_and: (p.or_and ? p.or_and.toString() : "")
          }
          display_logic_data.push(temp)
        })
        console.log("Index : ", i)
      }
      console.log("Display Logic : ", display_logic_data)
      var new_obj = {
        id: req.body.id_question[i],
        question: o,
        type: req.body.type[i],
        required: (req.body.required[i] == "true" ? true : false),
        number: req.body.number[i],
        custom: (req.body.custom[i] ? req.body.custom[i] : false),
        pageBreak: (req.body.pageBreak[i] == "true" ? true : false),
        randomize: (req.body.randomize[i] == "true"? true : false),
        sequence: req.body.sequence[i],
        displayLogic: display_logic_data,
      }
      if (req.body.data[i]) new_obj.data = req.body.data[i].split(",")
      if (req.body.carryForward[i]) {
        var obj = JSON.parse(req.body.carryForward[i])
        console.log(obj)
        var temp = {
          type:obj.type.toString(),
          question:obj.question.toString()
        }
        new_obj.carryForwardQuestion = temp.question
        new_obj.carryForwardType = temp.question
      }
      questions_data.push(new_obj)
    })
    if (req.body.profiling) {
      var profiling_raw = JSON.parse(req.body.profiling)
      profiling_raw.map(o => {
        var new_obj = {
          surveyprofiling: o._id,
          value: o.value,
        }
        if (o.minValue) new_obj.minValue = o.minValue
        if (o.maxValue) new_obj.minValue = o.maxValue
        profiling_data.push(new_obj)
      })
    }
    var dataCreate = {
      code: random_password().toString(),
      userRef: user_data._id,
      title: req.body.title,
      description: req.body.description,
      targetRespondent: req.body.targetRespondent,
      questions: questions_data,
      profiling: profiling_data,
      status: (req.body.save == "published" ? 'published' : 'drafted'),
      totalRespondent: 0,
      randomize: req.body.randomize,
      type: 'quest',
      createdDate: Date.now(),
      createdBy: user_data._id
    }
    Quest.create(dataCreate, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Failed to create the quest')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        if (type == "paid") {
          var dataUpdate = {
            questRef: quest._id,
            used: true,
            updatedDate: Date.now(),
            updatedBy: user_data._id
          }
          Order.findOneAndUpdate({
            orderId: id
          }, dataUpdate, function (err, order) {
            if (err || order == null) {
              console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Failed to create the quest')
              req.flash('info_type', 'error')
              res.redirect('/')
            } else {
              PreviewQuest.deleteMany({
                userRef: user_data._id
              }, function (err, preview_quest) {
                if (err) console.log(err)
                Setting.findOne({}, function (err, setting) {
                  if (err) console.log(err)
                  if (req.body.save == "published") {
                    // console.log(quest.code)
                    var code = webURL + "quest/" + dataCreate.code
                    res.render('quest_created', {
                      name_title: appName + " | Share your quest!",
                      user_data: user_data,
                      appName: appName,
                      code: code,
                      quest: quest,
                      setting_data: setting
                    })
                  } else {
                    res.render('drafted', {
                      name_title: appName + " | Your quest saved as a draft!",
                      user_data: user_data,
                      appName: appName,
                      quest: quest,
                      setting_data: setting
                    })
                  }
                })
              })
            }
          })
        } else {
          PreviewQuest.deleteMany({
            userRef: user_data._id
          }, function (err, preview_quest) {
            if (err) console.log(err)
            Setting.findOne({}, function (err, setting) {
              if (err) console.log(err)
              if (req.body.save == "published") {
                // console.log(quest.code)
                  var code = webURL + "quest/" + dataCreate.code
                res.render('quest_created', {
                  name_title: appName + " | Share your quest!",
                  user_data: user_data,
                  appName: appName,
                  code: code,
                  quest: quest,
                  setting_data: setting
                })
              } else {
                res.render('drafted', {
                  name_title: appName + " | Your quest saved as a draft!",
                  user_data: user_data,
                  appName: appName,
                  quest: quest,
                  setting_data: setting
                })
              }
            })
          })
        }
      }
    })
  }
}

//Answer Quest
exports.quest = function (req, res) {
  var user_data = req.session.user_data
  var anonymous_profiling = req.session.anonymous_profiling
  if (req.params.id) {
    Quest.findOne({
      code: req.params.id
    }).populate('userRef').exec(function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid Link')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        console.log("STEP 2")
        if (user_data) {
          //Profiling Chek
          var where = {
            questRef: quest._id.toString(),
            userRef: user_data._id
          }
          console.log(where)
          UserQuest.findOne(where, function (err, user_quest) {
            if (err) {
              req.flash('info_title', 'Information')
              req.flash('info', 'Invalid Link')
              req.flash('info_type', 'error')
              res.redirect('/')
            } else if (user_quest) {
              req.flash('info_title', 'Information')
              req.flash('info', 'You already answered the quest')
              req.flash('info_type', 'error')
              res.redirect('/')
            } else {
              console.log("DAPAT")
              //cek profiling
              var filtered = null;
              // Profiling Check
              if (quest.profiling.length > 0) {
                quest.profiling.map((p) => {
                  if (user_data) {
                    var profiling = user_data.profiling.find((o) => o.surveyProfiling.toString() == p.surveyprofiling.toString())
                    if (profiling) {
                      console.log("Profiling", profiling)
                      if (p.minValue && p.maxValue) {
                        console.log("Masuk sini")
                        if (profiling.answer >= p.minValue && profiling.answer >= p.maxValue) {
                          filtered = quest
                        }
                      } else {
                        if (profiling.answer === p.value) {
                          console.log("Masuk sini juga")
                          filtered = quest
                        }
                      }
                    }
                  }
                })

                if (filtered) {
                  Setting.findOne({}, function (err, setting) {
                    if (err) console.log(err)
                    res.render('quest', {
                      name_title: appName + " | " + quest.title,
                      current_device: req.device.type.toLowerCase(),
                      user_data: user_data,
                      appName: appName,
                      quest: filtered,
                      setting_data: setting
                    })
                  })
                } else {
                  req.flash('info_title', 'Information')
                  req.flash('info', "Your Profile does not match with the quest profiling")
                  req.flash('info_type', 'error')
                  res.redirect('/')
                }
              } else {
                Setting.findOne({}, function (err, setting) {
                  if (err) console.log(err)
                  res.render('quest', {
                    name_title: appName + " | " + quest.title,
                    current_device: req.device.type.toLowerCase(),
                    user_data: user_data,
                    appName: appName,
                    quest: quest,
                    setting_data: setting
                  })
                })
              }
            }
          })
        } else {
          Setting.findOne({}, function (err, setting) {
            if (err) console.log(err)
            if (anonymous_profiling) {
              //cek profiling
              var filtered = null;
              // Profiling Check
              if (quest.profiling.length > 0) {
                quest.profiling.map((p) => {
                  var profiling = anonymous_profiling.find((o) => o.surveyProfiling.toString() == p.surveyprofiling.toString())
                  if (profiling) {
                    console.log("Profiling", profiling)
                    if (p.minValue && p.maxValue) {
                      console.log("Masuk sini")
                      if (profiling.answer >= p.minValue && profiling.answer >= p.maxValue) {
                        filtered = quest
                      }
                    } else {
                      if (profiling.answer === p.value) {
                        console.log("Masuk sini juga")
                        filtered = quest
                      }
                    }
                  }
                })
                if (filtered) {
                  res.render('quest', {
                    name_title: appName + " | " + quest.title,
                    user_data: user_data,
                    appName: appName,
                    current_device: req.device.type.toLowerCase(),
                    quest: filtered,
                    setting_data: setting
                  })
                } else {
                  req.flash('info_title', 'Information')
                  req.flash('info', "Your Profile does not match with the quest profiling")
                  req.flash('info_type', 'error')
                  res.redirect('/')
                }
              } else {
                res.render('quest', {
                  name_title: appName + " | " + quest.title,
                  user_data: user_data,
                  appName: appName,
                  current_device: req.device.type.toLowerCase(),
                  quest: quest,
                  setting_data: setting
                })
              }
            } else {
              //Choose sign up atau isi sebagai anonymous
              res.render('anon', {
                name_title: appName + " | " + quest.title,
                appName: appName,
                quest: quest,
                setting_data: setting
              })
            }
          })
        }
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

//Preview Quest
exports.preview_quest = function (req, res) {
  var user_data = req.session.user_data
  if (req.params.id) {
    PreviewQuest.findOne({
      code: req.params.id
    }).populate('userRef').exec(function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid Link')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        console.log("STEP 2")
        Setting.findOne({}, function (err, setting) {
          if (err) console.log(err)
          res.render('preview_quest', {
            name_title: appName + " | Preview : " + quest.title,
            user_data: user_data,
            appName: appName,
            quest: quest,
            setting_data: setting
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}
//Answer Quest Process
exports.quest_process = function (req, res) {
  var user_data = req.session.user_data
  if (req.params.id) {
    Quest.findOne({
      code: req.params.id
    }, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid Link')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        if (user_data) {
          var where = {
            questRef: quest._id,
            userRef: user_data._id
          }
          UserQuest.findOne(where, function (err, user_quest) {
            if (err) {
              req.flash('info_title', 'Information')
              req.flash('info', 'Invalid Link')
              req.flash('info_type', 'error')
              res.redirect('/')
            } else if (user_quest) {
              req.flash('info_title', 'Information')
              req.flash('info', 'You already answered the quest')
              req.flash('info_type', 'error')
              res.redirect('/')
            } else {
              var answers = []
              quest.questions.map(o => {
                if (req.files) {
                  var file = req.files.find((p) => p.fieldname == [o._id])
                  if (file)
                    var answer = file.path.toString()
                  else
                    var answer = req.body[o._id].toString()
                } else var answer = req.body[o._id].toString()
                var id = req.body["id_" + o.id]
                var new_obj = {
                  id: id,
                  answer: answer
                }
                answers.push(new_obj)
              })
              var dataCreate = {
                answers: answers,
                userRef: user_data ? user_data._id : null,
                questRef: quest._id,
                respondent: user_data ? 'member' : 'anonymous',
                createdDate: Date.now(),
                createdBy: (user_data ? user_data._id : null)
              }
              UserQuest.create(dataCreate, function (err, user_quest) {
                if (err || user_quest == null) {
                  console.log(err)
                  req.flash('info_title', 'Information')
                  req.flash('info', 'Failed to answer the quest')
                  req.flash('info_type', 'error')
                  res.redirect('/')
                } else {
                  var dataUpdate = {
                    "$inc": {
                      totalRespondent: 1
                    },
                    updatedDate: Date.now(),
                    updatedBy: user_data ? user_data._id : null
                  }
                  Quest.findOneAndUpdate({
                    _id: quest._id
                  }, dataUpdate, function (err, quest) {
                    if (err || quest == null) {
                      console.log(err)
                      req.flash('info_title', 'Information')
                      req.flash('info', 'Failed to answer the quest')
                      req.flash('info_type', 'error')
                      res.redirect('/')
                    } else {
                      req.session.anonymous_profiling = null
                      // req.flash('info_title', 'Information')
                      // req.flash('info', 'Thank you for answering the quest')
                      // req.flash('info_type', 'success')
                      req.flash('rating', 'true')
                      res.redirect('/')
                    }
                  })
                }
              })
            }
          })
        } else {
          var answers = []
          quest.questions.map(o => {
            if (req.files) {
              var file = req.files.find((p) => p.fieldname == [o._id])
              if (file) {
                console.log("FILE", file)
                var answer = file.path.toString()
                console.log("FILE NAME", file.path)
              } else
                var answer = req.body[o._id]
            } else var answer = req.body[o._id]
            var id = req.body["id_" + o.id]
            var new_obj = {
              id: id,
              answer: answer
            }
            answers.push(new_obj)
          })
          var dataCreate = {
            answers: answers,
            userRef: user_data ? user_data._id : null,
            questRef: quest._id,
            respondent: user_data ? 'member' : 'anonymous',
            createdDate: Date.now(),
            createdBy: null
          }
          UserQuest.create(dataCreate, function (err, user_quest) {
            if (err || user_quest == null) {
              req.flash('info_title', 'Information')
              req.flash('info', 'Failed to answer the quest')
              req.flash('info_type', 'error')
              res.redirect('/')
            } else {
              var dataUpdate = {
                "$inc": {
                  totalRespondent: 1
                },
                updatedDate: Date.now(),
                updatedBy: null
              }
              Quest.findOneAndUpdate({
                _id: quest._id
              }, dataUpdate, function (err, quest) {
                if (err || quest == null) {
                  req.flash('info_title', 'Information')
                  req.flash('info', 'Failed to answer the quest')
                  req.flash('info_type', 'error')
                  res.redirect('/')
                } else {
                  req.session.anonymous_profiling = null
                  req.flash('info_title', 'Information')
                  req.flash('info', 'Thank you for answering the quest')
                  req.flash('info_type', 'success')
                  res.redirect('/')
                }
              })
            }
          })
        }
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

//Answer Quest
exports.answer_quest = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var info = ""
  var info_type = ""
  var info_title = ""
  var locals = {}
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    info = res.locals.info
    info_type = res.locals.info_type
    info_title = res.locals.info_title
  }
  if (req.params.id) {
    Quest.findOne({
      _id: req.params.id
    }).populate('userRef').exec(function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid Link')
        req.flash('info_type', 'error')
        res.redirect('/create_quest/free')
      } else {
        console.log("STEP 2")
        //Profiling Chek
        var where = {
          questRef: quest._id.toString(),
          userRef: (user_data ? user_data._id : null)
        }
        console.log(where)
        UserQuest.findOne(where, function (err, user_quest) {
          if (err) {
            req.flash('info_title', 'Information')
            req.flash('info', 'Invalid Link')
            req.flash('info_type', 'error')
            res.redirect('/create_quest/free')
          } else if (user_quest) {
            req.flash('info_title', 'Information')
            req.flash('info', 'You already answered the quest')
            req.flash('info_type', 'error')
            res.redirect('/create_quest/free')
          } else {
            console.log("DAPAT")
            Setting.findOne({}, function (err, setting) {
              if (err) console.log(err)
              res.render('answer_quest', {
                name_title: appName + " | " + quest.title,
                user_data: user_data,
                appName: appName,
                quest: quest,
                setting_data: setting,
                info: info,
                info_type: info_type,
                info_title: info_title
              })
            })
          }
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/create_quest/free')
  }
}

//Answer Quest Process
exports.answer_quest_process = function (req, res) {
  var user_data = req.session.user_data
  if (req.params.id) {
    Quest.findOne({
      _id: req.params.id
    }, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid Link')
        req.flash('info_type', 'error')
        res.redirect('/create_quest/free')
      } else {
        var where = {
          questRef: quest._id
        }
        if (user_data) where.userRef = user_data._id
        UserQuest.findOne(where, function (err, user_quest) {
          if (err) {
            req.flash('info_title', 'Information')
            req.flash('info', 'Invalid Link')
            req.flash('info_type', 'error')
            res.redirect('/create_quest/free')
          } else if (user_quest) {
            req.flash('info_title', 'Information')
            req.flash('info', 'You already answered the quest')
            req.flash('info_type', 'error')
            res.redirect('/create_quest/free')
          } else {
            var answers = []
            quest.questions.map(o => {
              if (req.files) {
                var file = req.files.find((p) => p.fieldname == [o._id])
                if (file) {
                  console.log("FILE", file)
                  var answer = file.path.toString()
                  console.log("FILE NAME", file.path)
                } else
                  var answer = req.body[o._id]
              } else var answer = req.body[o._id]
              var id = req.body["id_" + o.id]
              var new_obj = {
                id: id,
                answer: answer
              }
              answers.push(new_obj)
            })
            var dataCreate = {
              answers: answers,
              userRef: user_data ? user_data._id : null,
              questRef: quest._id,
              respondent: user_data ? 'member' : 'anonymous',
              createdDate: Date.now(),
              createdBy: (user_data ? user_data._id : null)
            }
            UserQuest.create(dataCreate, function (err, user_quest) {
              if (err || user_quest == null) {
                req.flash('info_title', 'Information')
                req.flash('info', 'Failed to answer the quest')
                req.flash('info_type', 'error')
                res.redirect('/')
              } else {
                var dataUpdate = {
                  "$inc": {
                    totalRespondent: 1
                  },
                  updatedDate: Date.now(),
                  updatedBy: user_data ? user_data._id : null
                }
                Quest.findOneAndUpdate({
                  _id: quest._id
                }, dataUpdate, function (err, quest) {
                  if (err || quest == null) {
                    req.flash('info_title', 'Information')
                    req.flash('info', 'Failed to answer the quest')
                    req.flash('info_type', 'error')
                    res.redirect('/create_quest/free')
                  } else {
                    if (req.session.answered_quest) {
                      var temp = req.session.answered_quest
                      temp.push(quest)
                      req.session.answered_quest = temp
                    } else {
                      req.session.answered_quest = [quest]
                    }
                    req.flash('info_title', 'Information')
                    req.flash('info', 'Thank you for answering the quest')
                    req.flash('info_type', 'success')
                    res.redirect('/create_quest/free')
                  }
                })
              }
            })
          }
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

//Anonymous profiling
exports.anonymous_profiling = function (req, res) {
  if (req.params.id) {
    SurveyProfiling.find({}, function (err, survey_profiling) {
      if (err || survey_profiling == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Something went wrong')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        res.render('anonymous_profiling', {
          name_title: appName + " | Anonymous Profiling",
          appName: appName,
          survey_profiling: survey_profiling
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

//anonymous profiling process
exports.anonymous_profiling_process = function (req, res) {
  if (req.params.id) {
    SurveyProfiling.find({}, function (err, survey_profiling) {
      if (err || survey_profiling == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid link')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        var profiling = []
        survey_profiling.map((o) => {
          var input = req.body[o._id]
          var new_obj = {
            surveyProfiling: o._id,
            answer: input.toString()
          }
          if (input != null) profiling.push(new_obj)
        })
        if (profiling) {
          req.session.anonymous_profiling = profiling
          res.redirect('/quest/' + req.params.id)
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'You have to fill all the fields')
          req.flash('info_type', 'error')
          res.redirect('/')
        }
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

//My Quest
exports.my_quest = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var info = ""
  var info_type = ""
  var info_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    var info = res.locals.info
    var info_type = res.locals.info_type
    var info_name_title = res.locals.info_title
  }
  var locals = {}
  async.parallel([
    function (callback) {
      Setting.findOne({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.setting = result
        callback()
      })
    },
    function (callback) {
      Quest.find({
        userRef: user_data._id
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.quest = result
        callback()
      })
    },
    function (callback) {
      UserQuest.find({
        userRef: user_data._id
      }).populate("questRef").exec(function (err, result) {
        if (err) return callback(err)
        locals.user_quest = result
        callback()
      })
    },
  ], function (err) {
    console.log(locals.faq)
    res.render('my_quest', {
      name_title: appName + " | " + "My Quest",
      appName: appName,
      setting_data: locals.setting,
      quest: locals.quest,
      user_quest: locals.user_quest,
      user_data: user_data,
      info: info,
      info_type: info_type,
      info_name_title: info_name_title
    })
  })
}

//Quests List
exports.quests = function (req, res) {
  // auth_redirect(req, res)
  var user_data = req.session.user_data
  var info = ""
  var info_type = ""
  var info_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    var info = res.locals.info
    var info_type = res.locals.info_type
    var info_name_title = res.locals.info_title
  }
  var locals = {}
  async.parallel([
    function (callback) {
      Setting.findOne({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.setting = result
        callback()
      })
    },
    function (callback) {
      Quest.find({}).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.quest = result
        callback()
      })
    },
    function (callback) {
      var where = {};
      if(user_data) where.userRef=user_data._id
      UserQuest.find(where).sort({
        createdDate: -1
      }).exec(function (err, result) {
        if (err) return callback(err)
        locals.user_quest = result
        callback()
      })
    }
  ], function (err) {
    var finalFiltered = []
    if (locals.user_quest && locals.quest) {
      locals.quest.map((o) => {
        var answered = locals.user_quest.find((p) => p.questRef.toString() === o._id.toString())
        if (answered) {} else finalFiltered.push(o)
      })
    }
    res.render('quests', {
      name_title: appName + " | " + "Quests",
      appName: appName,
      setting_data: locals.setting,
      quest: locals.finalFiltered,
      user_data: user_data,
      info: info,
      info_type: info_type,
      info_name_title: info_name_title
    })
  })
}

//My Order
exports.my_order = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var info = ""
  var info_type = ""
  var info_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined') {
    var info = res.locals.info
    var info_type = res.locals.info_type
    var info_name_title = res.locals.info_title
  }
  var locals = {}
  async.parallel([
    function (callback) {
      Setting.findOne({}).exec(function (err, result) {
        if (err) return callback(err)
        locals.setting = result
        callback()
      })
    },
    function (callback) {
      Order.find({
        userRef: user_data._id
      }).populate("questRef").exec(function (err, result) {
        if (err) return callback(err)
        locals.order = result
        callback()
      })
    },
  ], function (err) {
    console.log(locals.faq)
    res.render('my_order', {
      name_title: appName + " | " + "My Order",
      appName: appName,
      setting_data: locals.setting,
      order: locals.order,
      user_data: user_data,
      info: info,
      info_type: info_type,
      info_name_title: info_name_title
    })
  })
}

exports.createOrder = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data

}

exports.delete_quest_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    Quest.findOneAndRemove({
      _id: req.params.id,
      userRef: user_data._id
    }, function (error, result) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (result == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest not found')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Quest Success')
        req.flash('info_type', 'success')
        res.redirect('/my_quest')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/my_quest')
  }
}

exports.delete_order_process = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    Order.findOneAndRemove({
      _id: req.params.id,
      userRef: user_data._id
    }, function (error, result) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (result == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Order not found')
        req.flash('info_type', 'error')
        res.redirect('/my_order')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Order Success')
        req.flash('info_type', 'success')
        res.redirect('/my_order')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/my_quest')
  }
}

exports.view_quest = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    Quest.findOne({
      _id: req.params.id,
      userRef: user_data._id
    }, function (error, quest) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (quest == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest not found')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        Setting.findOne({}, function (err, setting) {
          res.render('view_quest', {
            name_title: appName + " | " + quest.title,
            appName: appName,
            setting_data: setting,
            quest: quest,
            user_data: user_data,
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/my_quest')
  }
}

exports.edit_quest = function (req, res, next) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    Quest.findOne({
      _id: req.params.id,
      userRef: user_data._id
    }).populate("profiling.surveyprofiling").exec(function (error, quest) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (quest == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest not found')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        Setting.findOne({}, function (err, setting) {
          SurveyProfiling.find({}, function (err, survey_profiling) {
            res.render('edit_quest', {
              name_title: appName + " | " + quest.title,
              appName: appName,
              setting_data: setting,
              quest: quest,
              type_list: SurveyProfiling.schema.path('type').enumValues,
              survey_profiling: survey_profiling,
              user_data: user_data,
            })
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/my_quest')
  }
}

//edit_quest_process
exports.edit_quest_process = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  var type = (req.params.type ? req.params.type : "")
  var id = (req.params.id ? req.params.id : "")
  if (req.body.title && req.body.question && req.params.id) {
    req.body.question = (req.body.question instanceof Array) ? req.body.question : [req.body.question]
    req.body.type = (req.body.type instanceof Array) ? req.body.type : [req.body.type]
    req.body.required = (req.body.required instanceof Array) ? req.body.required : [req.body.required]
    req.body.number = (req.body.number instanceof Array) ? req.body.number : [req.body.number]
    req.body.custom = (req.body.custom instanceof Array) ? req.body.custom : [req.body.custom]
    req.body.data = (req.body.data instanceof Array) ? req.body.data : [req.body.data]
    req.body.carryForward = (req.body.carryForward instanceof Array) ? req.body.carryForward : [req.body.carryForward]
    req.body.randomize = (req.body.randomize instanceof Array) ? req.body.randomize : [req.body.randomize]
    var questions_raw = req.body.question
    var questions_data = []
    var profiling_data = []
    questions_raw.map((o, i) => {

      var display_logic_data = []
      if (req.body.display_logic[i]) {
        // console.log("DISPLAY LOGIC", req.body.display_logic[i])
        var parsed = JSON.parse(req.body.display_logic[i])
        // console.log(parsed)
        parsed.map((p) => {
          var temp = {
            logic_type: (p.logic_type ? p.logic_type.toString() : ""),
            question: (p.question ? p.question.toString() : ""),
            is_not: (p.is_not ? p.is_not.toString() : ""),
            is_selected: (p.is_selected ? p.is_selected.toString() : ""),
            device: (p.device ? p.device.toString() : ""),
            equal: (p.equal ? p.equal.toString() : ""),
            equal_to: (p.equal_to ? p.equal_to.toString() : ""),
            answer: (p.answer ? p.answer.toString() : ""),
            or_and: (p.or_and ? p.or_and.toString() : "")
          }
          display_logic_data.push(temp)
        })
        console.log("Index : ", i)
      }
      console.log("Display Logic : ", display_logic_data)
      var new_obj = {
        id: req.body.id_question[i],
        question: o,
        type: req.body.type[i],
        required: (req.body.required[i] == "true" ? true : false),
        number: req.body.number[i],
        custom: (req.body.custom[i] ? req.body.custom[i] : false),
        pageBreak: (req.body.pageBreak[i] == "true" ? true : false),
        randomize: (req.body.randomize[i] == "true"? true : false),
        sequence: req.body.sequence[i],
        displayLogic: display_logic_data
      }
      if (req.body.data[i]) new_obj.data = req.body.data[i].split(",")
      if (req.body.carryForward[i]) {
        var obj = JSON.parse(req.body.carryForward[i])
        console.log(obj)
        var temp = {
          type:obj.type.toString(),
          question:obj.question.toString()
        }
        new_obj.carryForwardQuestion = temp.question
        new_obj.carryForwardType = temp.type
      }
      questions_data.push(new_obj)
    })
    if (req.body.profiling) {
      var profiling_raw = JSON.parse(req.body.profiling)
      profiling_raw.map(o => {
        var new_obj = {
          surveyprofiling: o._id,
          value: o.value,
        }
        if (o.minValue) new_obj.minValue = o.minValue
        if (o.maxValue) new_obj.minValue = o.maxValue
        profiling_data.push(new_obj)
      })
    }
    var dataUpdate = {
      userRef: user_data._id,
      title: req.body.title,
      description: req.body.description,
      targetRespondent: req.body.targetRespondent,
      questions: questions_data,
      profiling: profiling_data,
      status: (req.body.save == "published" ? 'published' : 'drafted'),
      totalRespondent: 0,
      type: 'quest',
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    Quest.findOneAndUpdate({_id:req.params.id}, dataUpdate, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Failed to update the quest')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        if (type == "paid") {
          var dataUpdate = {
            questRef: quest._id,
            updatedDate: Date.now(),
            updatedBy: user_data._id
          }
          Order.findOneAndUpdate({
            orderId: id
          }, dataUpdate, function (err, order) {
            if (err || order == null) {
              console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Failed to update the quest')
              req.flash('info_type', 'error')
              res.redirect('/my_quest')
            } else {
              PreviewQuest.deleteMany({
                userRef: user_data._id
              }, function (err, preview_quest) {
                if (err) console.log(err)
                Setting.findOne({}, function (err, setting) {
                  if (err) console.log(err)
                  req.flash('info_title', 'Information')
                  req.flash('info', 'Update quest success')
                  req.flash('info_type', 'success')
                  res.redirect('/my_quest')
                })
              })
            }
          })
        } else {
          PreviewQuest.deleteMany({
            userRef: user_data._id
          }, function (err, preview_quest) {
            if (err) console.log(err)
            Setting.findOne({}, function (err, setting) {
              if (err) console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Update quest success')
              req.flash('info_type', 'success')
              res.redirect('/my_quest')
            })
          })
        }
      }
    })
  }
}

//view_answer
exports.view_answer = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id && req.params.skip) {
    Quest.findOne({
      _id: req.params.id,
      userRef: user_data._id
    }, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Failed to view the answer')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        UserQuest.find({
          questRef: req.params.id
        }).count().exec(function (err, total_user_quest) {
          if (err) console.log(err)
          UserQuest.findOne({
            questRef: req.params.id
          }).populate("userRef").limit(1).skip(parseInt(req.params.skip) - 1).exec(function (err, user_quest) {
            if (err || user_quest == null) {
              console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Failed to view the answer')
              req.flash('info_type', 'error')
              res.redirect('/my_quest')
            } else {
              Setting.findOne({}, function (err, setting) {
                console.log(user_quest)
                res.render('view_answer', {
                  name_title: appName + " | " + quest.title,
                  appName: appName,
                  setting_data: setting,
                  skip: req.params.skip,
                  quest: quest,
                  user_quest: user_quest,
                  total_user_quest: total_user_quest,
                  user_data: user_data,
                })
              })
            }
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'ID is required')
    req.flash('info_type', 'error')
    res.redirect('/my_quest')
  }
}

//view my answer
exports.view_my_answer = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    UserQuest.findOne({
      _id: req.params.id
    }).populate("questRef").exec(function (err, user_quest) {
      if (err || user_quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Failed to view the answer')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        Setting.findOne({}, function (err, setting) {
          console.log(user_quest)
          res.render('view_my_answer', {
            name_title: appName + " | " + user_quest.questRef.title,
            appName: appName,
            setting_data: setting,
            quest: user_quest.questRe,
            user_quest: user_quest,
            user_data: user_data,
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'ID is required')
    req.flash('info_type', 'error')
    res.redirect('/my_quest')
  }
}

//Publish Quest
exports.publish_quest = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  if (req.params.id) {
    var dataUpdate = {
      status: 'published',
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    Quest.findByIdAndUpdate(req.params.id, dataUpdate, function (err, quest) {
      if (err || quest == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Something went wrong')
        req.flash('info_type', 'error')
        res.redirect('/my_quest')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest Published')
        req.flash('info_type', 'success')
        res.redirect('/my_quest')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Invalid Link')
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

//download answers bulking
exports.download_answer = function (req, res) {
  auth_redirect(req, res)
  var user_data = req.session.user_data
  Quest.findOne({
    _id: req.params.id,
    userRef: user_data._id
  }, function (err, quest) {
    if (err || quest == null) {
      req.flash('info_title', 'Information')
      req.flash('info', 'Invalid ID Quest')
      req.flash('info_type', 'success')
      res.redirect('/my_quest')
    } else {
      UserQuest.find({
        questRef: quest._id
      }).populate("userRef").exec(function (err, user_quest) {
        if (err || user_quest == null) {
          req.flash('info_title', 'Information')
          req.flash('info', 'No Answer Found')
          req.flash('info_type', 'success')
          res.redirect('/my_quest')
        } else {
          var BASE_DIR = __dirname.replace('\controllers', '')
          var dest = BASE_DIR + "/assets/exports/"
          var writeStream = fs.createWriteStream(dest + quest.code + ".xls")
          var header = "No." + "\t" + "Email" + "\t"
          quest.questions.map((o) => {
            header += removeElements(o.number) + "\t"
          })
          header += "\n"
          var row = ""
          if (user_quest) {
            user_quest.map((o, i) => {
              row += i + 1 + "\t" + (o.userRef ? o.userRef.email : "-") + "\t"
              o.answers.map((p) => {
                var question = quest.questions.find((o) => o.id.toString() == p.id.toString())
                if (question) {
                  if (question.type == "file")
                    row += webURL + p.answer.substr(6) + "\t"
                  else
                    row += p.answer + "\t"
                } else
                  row += "-" + "\t"
              })
              row += "\n"
            })
          }
          writeStream.write(header)
          writeStream.write(row)
          writeStream.close()
          res.redirect("/exports/" + quest.code + ".xls")
        }
      })
    }
  })
}

exports.paymentSuccess = function (req, res) {
  var user_data = req.session.user_data
  if (user_data) {
    var dataUpdate = {
      status: "paid",
      updatedDate: Date.now(),
      updatedBy: user_data._id
    }
    Order.findOneAndUpdate({
      orderId: req.params.id
    }, dataUpdate, function (err, order) {
      if (err || order == null) {
        var ret = {
          status: 400,
          message: "Invalid Order ID"
        }
        return res.send(JSON.stringify(ret))
      } else {
        var ret = {
          status: 200,
          message: "Payment Successful"
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var ret = {
      status: 400,
      message: "You have no access for this action"
    }
    return res.send(JSON.stringify(ret))
  }
}

exports.waiting = function (req, res) {
  var user_data = req.session.user_data
  if (user_data) {
    Order.findOne({
      orderId: req.params.id
    }, function (err, order) {
      if (err || order == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Invalid Link')
        req.flash('info_type', 'error')
        res.redirect('/')
      } else {
        Setting.findOne({}, function (err, setting) {
          if (err) console.log(err)
          res.render('waiting', {
            appName: appName,
            user_data: user_data,
            name_title: appName + " | " + "Waiting For Payment",
            order: order,
            setting_data: setting
          });
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', "You have no access for this action")
    req.flash('info_type', 'error')
    res.redirect('/')
  }
}

exports.preview_quest_process = function (req, res) {
  var user_data = req.session.user_data
  if (user_data) {
    req.body.question = (req.body.question instanceof Array) ? req.body.question : [req.body.question]
    req.body.type = (req.body.type instanceof Array) ? req.body.type : [req.body.type]
    req.body.required = (req.body.required instanceof Array) ? req.body.required : [req.body.required]
    req.body.number = (req.body.number instanceof Array) ? req.body.number : [req.body.number]
    req.body.custom = (req.body.custom instanceof Array) ? req.body.custom : [req.body.custom]
    req.body.data = (req.body.data instanceof Array) ? req.body.data : [req.body.data]
    req.body.randomize = (req.body.randomize instanceof Array) ? req.body.randomize : [req.body.randomize]
    var questions_raw = req.body.question
    var questions_data = []
    var profiling_data = []
    questions_raw.map((o, i) => {

      var display_logic_data = []
      if (req.body.display_logic[i]) {
        // console.log("DISPLAY LOGIC", req.body.display_logic[i])
        var parsed = JSON.parse(req.body.display_logic[i])
        // console.log(parsed)
        parsed.map((p) => {
          var temp = {
            logic_type: (p.logic_type ? p.logic_type.toString() : ""),
            question: (p.question ? p.question.toString() : ""),
            is_not: (p.is_not ? p.is_not.toString() : ""),
            is_selected: (p.is_selected ? p.is_selected.toString() : ""),
            device: (p.device ? p.device.toString() : ""),
            equal: (p.equal ? p.equal.toString() : ""),
            equal_to: (p.equal_to ? p.equal_to.toString() : ""),
            answer: (p.answer ? p.answer.toString() : ""),
            or_and: (p.or_and ? p.or_and.toString() : "")
          }
          display_logic_data.push(temp)
        })
        console.log("Index : ", i)
      }
      console.log("Display Logic : ", display_logic_data)
      var new_obj = {
        id: req.body.id_question[i],
        question: o,
        type: req.body.type[i],
        required: (req.body.required[i] == "true" ? true : false),
        number: req.body.number[i],
        custom: (req.body.custom[i] ? req.body.custom[i] : false),
        pageBreak: (req.body.pageBreak[i] == "true" ? true : false),
        randomize: (req.body.randomize[i] == "true"? true : false),
        sequence: req.body.sequence[i],
        displayLogic: display_logic_data
      }
      if (req.body.data[i]) new_obj.data = req.body.data[i].split(",")
      questions_data.push(new_obj)
    })
    var dataCreate = {
      code: random_password(),
      userRef: user_data._id,
      title: req.body.title,
      questions: questions_data,
      createdDate: Date.now(),
      createdBy: user_data._id
    }
    PreviewQuest.create(dataCreate, function (err, preview_quest) {
      if (err || preview_quest == null) {
        var ret = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(ret))
      } else {
        var ret = {
          status: 200,
          message: "Create Preview Success",
          redirect: webURL + "preview_quest/" + preview_quest.code
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var ret = {
      status: 400,
      message: "You have no access for this action"
    }
    return res.send(JSON.stringify(ret))
  }

}
//function auth redirect
function auth_redirect(req, res) {
  sess = req.session
  if (sess.user_login) return true
  else res.redirect('/signin')
}

var removeElements = function (text) {
  text = htmlToText.fromString(text)
  return text
}