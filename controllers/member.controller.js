// const User = require('../models/user.model');
// const Marital = require('../models/marital.model');
// const Occupation = require('../models/occupation.model');
// const AllowanceItem = require('../models/allowance_item.model');
// const Session = require('../models/session.model');
// const Employee = require('../models/employee.model');
// const Insurance = require('../models/insurance.model');
// const Allowance = require('../models/allowance.model');
// const Attendance = require('../models/attendance.model');
// const Deduction = require('../models/deduction.model');
// const Salary = require('../models/salary.model');
// const readXlsxFile = require('read-excel-file/node');
// const xlsxj = require("xlsx-to-json-lc");
// const xlsj = require("xls-to-json-lc");
// var fileExtension = require('file-extension');
// var dateFormat = require('dateformat');
// var path = require('path')
// var crypto = require('crypto');
// var request = require('request');
// var async = require('async');
// var fs = require('fs');
// var cron = require('node-cron');
// var nodemailer = require('nodemailer')
// const mongoose = require('mongoose');
// const ObjectId = mongoose.Types.ObjectId;


// //Dashboard
// exports.dashboard = function (req, res) {
//   auth_redirect(req, res);
//   var locals = {}
//   var user_data = req.session.user_data
//      async.parallel([
//       function(callback) {
//         Employee.count({}, function( err, count){
//           locals.total_employee = count
//           callback();
//       })
//       }
//     ], function(err){
//         if (err) return next(err);
//         res.render('member_dashboard', {
//           title :  appName+" | "+"Dashboard",
//           user_data : user_data,
//           total_employee : locals.total_employee,
//         })
//       })
// };


// exports.salary = function (req, res) {
//   auth_redirect(req, res);
//   var user_data = req.session.user_data;
//   var locals = {};
//   var user_list = [];
//   var messages = "";
//   var messages_type = "";
//   var messages_title = "";
//   res.locals.info = req.flash('info');
//   res.locals.info_type = req.flash('info_type');
//   res.locals.info_title = req.flash('info_title');
//   if (typeof res.locals.info != 'undefined'){
//     var messages = res.locals.info;
//     var messages_type = res.locals.info_type;
//     var messages_title = res.locals.info_title;
//   }
//   async.parallel([
//     function(callback) {
//       Salary.find({employeeRef:user_data.employeeRef}).populate("employeeRef allowanceRef deductionRef createdBy updatedBy").sort({createdDate : -1}).exec(function(err,salary){
//         if (err) return callback(err)
//         locals.salary_list = salary
//         callback()
//       })
//     }
//   ], function(err) { 
//     if (err) return next(err);
//     res.render('member_salary', {
//       name_title :  appName+" | "+"Salary",
//       user_data : user_data,
//       salary_list : locals.salary_list,
//       messages : messages,
//       messages_type : messages_type,
//       messages_title : messages_title
//     })
//   });
// };

// // exports.create_salary_process = function (req, res, next) {
// //   auth_redirect(req, res);
// //   var user_data = req.session.user_data;
// //   if (req.body.name != '' && req.body.description){
// //     var createData={
// //       name: req.body.name,
// //       description : req.body.description,
// //       createdDate: Date.now(),
// //       createdBy: user_data._id,
// //     }
// //     Marital.create(createData, function (error, salary) {
// //       if (error || salary==null) {
// //         req.flash('info_title', 'Information')
// //         req.flash('info', 'Create Salary Failed! Something Went Wrong.')
// //         req.flash('info_type', 'error')
// //         res.redirect('/admin/salary')
// //       } else {
// //         req.flash('info_title', 'Information')
// //         req.flash('info', 'Create Salary Success')
// //         req.flash('info_type', 'success')
// //         res.redirect('/admin/salary')
// //       }
// //     })
// //   } else {
// //     req.flash('info_title', 'Information')
// //     req.flash('info', 'All fields required')
// //     req.flash('info_type', 'error')
// //     res.redirect('/admin/salary')
// //   }
// // }

// // exports.single_salary = function (req, res, next) {
// //   Session.validate(req.session.key, 'admin', function (error, user_session) {
// //     if (error) {
// //       var err = { status:400, message:'Something went wrong', data: error}
// //       return res.send(JSON.stringify(err))
// //     } else if ( !user_session ){
// //       var err = { status:400, message:'Token Invalid', data: user_session}
// //       return res.send(JSON.stringify(err))
// //     } else {
// //       if (req.params.id){
// //         Marital.findOne({_id:req.params.id},{'__v':0}).exec(function(err,salary){
// //           if (err) {
// //             var err = { status:400, message:err }
// //             return res.send(JSON.stringify(err))
// //           } if (salary==null) {
// //             var err = { status:400, message:'Salary Not Found' }
// //             return res.send(JSON.stringify(err))
// //           } else {
// //             var ret = {
// //               status:200,
// //               data: salary,
// //             }
// //             return res.send(JSON.stringify(ret))
// //           }
// //         })
// //       } else {
// //         var err = { status:400, message:'Salary Id Required' }
// //         return res.send(JSON.stringify(err))
// //       }
// //     }
// //   })
// // }

// // exports.update_salary_process = function (req, res, next) {
// //   auth_redirect(req, res);
// //   var user_data = req.session.user_data;
// //   if (req.body.name != '' && req.body.description){
// //     var userData = {
// //       name: req.body.name,
// //       description : req.body.description,
// //       createdDate: Date.now(),
// //       createdBy: user_data._id,
// //     }
    
// //     Marital.findOneAndUpdate({_id: req.body.id},userData, function (error, salary) {
// //       if (error || salary==null) {
// //         req.flash('info_title', 'Information')
// //         req.flash('info', 'Edit Salary Failed')
// //         req.flash('info_type', 'error')
// //         res.redirect('/admin/salary')
// //       } else {
// //         req.flash('info', 'Edit Salary Success')
// //         req.flash('info_type', 'success')
// //         res.redirect('/admin/salary')
// //       }
// //     })
// //   } else {
// //     req.flash('info_title', 'Information')
// //     req.flash('info', 'All Fields Required')
// //     req.flash('info_type', 'error')
// //     res.redirect('/admin/salary')
// //   }
// // }

// // exports.delete_salary_process = function (req, res, next) {
// //   auth_redirect(req, res);
// //   var user_data = req.session.user_data;
// //   if (req.params.id){
// //       Marital.findByIdAndRemove(req.params.id, function (error, salary) {
// //         if (error) {
// //           var err = { status:400, message:error }
// //           return res.send(JSON.stringify(err))
// //         } if (salary==null) {
// //           req.flash('info_title', 'Information')
// //           req.flash('info', 'Salary Not Found')
// //           req.flash('info_type', 'error')
// //           res.redirect('/admin/salary')
// //         } else {
// //           req.flash('info_title', 'Information')
// //           req.flash('info', 'Delete Salary Success')
// //           req.flash('info_type', 'success')
// //           res.redirect('/admin/salary')
// //         }
// //       })
// //   } else {
// //     req.flash('info_title', 'Information')
// //     req.flash('info', 'Id Required')
// //     req.flash('info_type', 'error')
// //     res.redirect('/admin/salary')
// //   }
// // }

// exports.single_salary_payslip = function (req, res) {
//   auth_redirect(req, res);
//   var user_data = req.session.user_data;
//   var locals = {};
//   var user_list = [];
//   var messages = "";
//   var messages_type = "";
//   var messages_title = "";
//   res.locals.info = req.flash('info');
//   res.locals.info_type = req.flash('info_type');
//   res.locals.info_title = req.flash('info_title');
//   if (typeof res.locals.info != 'undefined'){
//     var messages = res.locals.info;
//     var messages_type = res.locals.info_type;
//     var messages_title = res.locals.info_title;
//   }

//   if (req.params.id){
//     async.parallel([
//       function(callback) { 
//         Salary.findById(req.params.id).populate({path : 'employeeRef headOfficeRef allowanceRef deductionRef createdBy updatedBy', populate : {path : 'occupationRef'}})
//         .sort({createdDate : -1}).exec(function(err,result){
//           if (err) return callback(err)
//           locals.payslip = result
//           console.log(result)
//           callback()
//         })
//       }
//     ], function(err) {      
//       if (err){
//         console.log(err)  
//         req.flash('info_title', 'Information')
//         req.flash('info', 'Something wrong!')
//         req.flash('info_type', 'error')
//         res.redirect('/member/salary')
//       } else {
//         res.render('member_payslip', {
//           name_title :  appName+" | "+"Payslip",
//           user_data : user_data,
//           payslip : locals.payslip,
//           messages : messages,
//           messages_type : messages_type,
//           messages_title : messages_title
//         })        
//       }
//     })
//   } else {
//     req.flash('info_title', 'Information')
//     req.flash('info', 'Id Required')
//     req.flash('info_type', 'error')
//     res.redirect('/member/salary')
//   }
// };

// function auth_redirect(req, res) {
//   sess = req.session;
//   if (sess.user_login) return true;
//   else res.redirect('/signin');
// }

// function random_password(){
//   var pass = (Math.random().toString(36).substr(2)).toUpperCase()
//   pass = crypto.createHash('md5').update(pass).digest('hex')
//   return pass
// }
