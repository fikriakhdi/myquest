const User = require('../models/user.model')
const Session = require('../models/session.model')
const Setting = require('../models/setting.model')
const Feature = require('../models/feature.model')
const htmlToText = require('html-to-text')
const Order = require('../models/order.model')
const Faq = require('../models/faq.model')
const News = require('../models/news.model')
const Message = require('../models/message.model')
const Testimonial = require('../models/testimonial.model')
const Quest = require('../models/quest.model')
const UserQuest = require('../models/user_quest.model')
const SurveyProfiling = require('../models/survey_profiling.model')
const PreviewQuest = require('../models/preview_quest.model')
// const Salary = require('../models/salary.model')
const readXlsxFile = require('read-excel-file/node')
const moment = require("moment")
const xlsxj = require("xlsx-to-json-lc")
const xlsj = require("xls-to-json-lc")
var xls2json = require('xls2json')
var fileExtension = require('file-extension')
var dateFormat = require('dateformat')
var path = require('path')
var crypto = require('crypto')
var request = require('request')
var async = require('async')
var smtpTransport = require('nodemailer-smtp-transport')
var cron = require('node-cron')
var nodemailer = require('nodemailer')
var fs = require('fs')
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
var fs = require('fs')
var handlebars = require('hbs')
var readHTMLFile = function (path, callback) {
  fs.readFile(path, {
    encoding: 'utf-8'
  }, function (err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};
var transporter = nodemailer.createTransport(
  smtpTransport({
  service: 'Zoho',
  // auth: {
  //   user: "myquestindo@gmail.com",
  //   pass: "imgsatujiwa"
  // }
  // host: 'smtp.zoho.com',
  // port: 465,
  // secure: true, // use SSL
  auth: {
      user: "admin@myquest.id",
      pass: "imgsatujiwa"
  }
  // secureConnection: 'false',
  // tls: {
  //   ciphers: 'SSLv3',
  //   rejectUnauthorized: false

  // }

})
);
// ==============================
// Sign In
// ==============================
//signin
exports.signin = function (req, res) {
  var messages = {};
  res.locals.info = req.flash('info');
  var redirect = req.params.redirect;
  sess = req.session;
  sess.redirect = redirect;
  // console.log(sess.redirect);
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info;
  }
  res.render('signin_admin', {
    appName:appName,
    name_title: appName + " | " + "SignIn Administrator",
    messages: messages,
    messages_type: 'danger'
  });
};

//signin
exports.signin_process = function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  sess = req.session;
  var redirect = sess.redirect
  var locals = {};
  console.log("BODY", req.body)
  User.authenticate(email, password, "admin", function (error, user) {
    if (error || !user || user.length <= 0) {
      console.log(error, user)
      var ret = {
        status:400,
        message:"Invalid Email/Password"
      }
      return res.send(JSON.stringify(ret))
    } else {
      locals.user = user;
      var sessionData = {
        token: crypto.createHash('md5').update(req.sessionID + (new Date(user.expire))).digest('hex'),
        user: user._id,
        createdDate: Date.now(),
      }
      var options = {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      }
      Session.findOneAndUpdate({
        user: ObjectId(user._id)
      }, sessionData, options, function (error, session) {
        if (!error || session != null) {
          console.log("token:", session.token)
          sess.admin_login = email
          sess.admin_data = user
          sess.key = session.token
          console.log(user)
          console.log('Login Berhasil')                    
          var ret = {
            status:200,
            message:"Sign In Successful"
          }
          return res.send(JSON.stringify(ret))
        } else {                   
          var ret = {
            status:200,
            message:"Sign In Successful"
          }
          return res.send(JSON.stringify(ret))  
        }
      })
    }
  })
}
exports.forgot_password = function (req, res) {
  var messages = {};
  res.locals.info = req.flash('info');
  var redirect = req.params.redirect;
  sess = req.session;
  sess.redirect = redirect;
  // console.log(sess.redirect);
  if (typeof res.locals.info != 'undefined') {
    messages = res.locals.info;
  }
  res.render('forgot_password_admin', {
    appName:appName,
    name_title: appName + " | " + "Forgot Password",
    messages: messages,
    messages_type: 'danger'
  });
};
// POST forgot password
exports.forgot_password_process = function (req, res, next) {
  if (req.body.email) {
    Setting.findOne({}, function (err, settings) {
      if (err) console.log(err)
      User.findOne({
        email: req.body.email
      }, {}, function (err, user) {
        if (err || !user) {
          var err = {
            status: 400,
            message: "Email is not registered"
          }
          return res.send(JSON.stringify(err))
        } else {
          var newpass = random_password()
          User.findOneAndUpdate({
            _id: user._id
          }, {
            password: crypto.createHash('md5').update(newpass).digest('hex')
          }, function (error, usr) {
            if (error) {
              var err = {
                status: 400,
                message: error
              }
              return res.send(JSON.stringify(err))
            } else {
              var BASE_DIR = __dirname.replace('\controllers', '')
              readHTMLFile(BASE_DIR + '/assets/html_template/forgot_password.html', function (err, html) {
                var template = handlebars.compile(html);
                var replacements = {
                  appName: appName,
                  user_name: usr.name,
                  user_email: req.body.email,
                  new_password: newpass,
                  year: new Date().getFullYear(),
                  email_admin: settings.email,
                  phone_admin: settings.phone
                };
                var htmlToSend = template(replacements);
                var mailOptions = {
                  from: 'Administrator <' + settings.email + '>',
                  to: req.body.email,
                  subject: appName + ' Account Forgot Password',
                  html: htmlToSend
                }
                transporter.sendMail(mailOptions, function (error, info) {
                  if (error) {
                    var err = {
                      status: 400,
                      message: error
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var ret = {
                      status: 200,
                      message: 'Email Sent. Please open your email to reset password'
                    }
                    return res.send(JSON.stringify(ret))
                  }
                })
              })
            }
          })
        }
      })
    })
  } else {
    var err = {
      status: 400,
      message: "Email is required"
    }
    return res.send(JSON.stringify(err))
  }
}
// ==============================
// Dashboard
// ==============================
exports.dashboard = function (req, res) {
  auth_redirect(req, res)
  var locals = {}
  var admin_data = req.session.admin_data
     async.parallel([
      function (callback) {
        Quest.find({}).count().exec(function (err, result) {
          if (err) return callback(err)
          locals.total_quest = result
          callback()
        })
      },
      function (callback) {
        User.find({}).count().exec(function (err, result) {
          if (err) return callback(err)
          locals.total_user = result
          callback()
        })
      },
      function (callback) {
        Order.find({}).count().exec(function (err, result) {
          if (err) return callback(err)
          locals.total_order = result
          callback()
        })
      },
      function (callback) {
        Message.find({}).count().exec(function (err, result) {
          if (err) return callback(err)
          locals.total_message = result
          callback()
        })
      },
      function (callback) {
        Quest.find({}).limit(5).sort({createdDate:-1}).exec(function (err, result) {
          if (err) return callback(err)
          locals.recent_quest = result
          callback()
        })
      },
      function (callback) {
        Setting.findOne({}).exec(function (err, setting) {
          if (err) return callback(err)
          locals.setting = setting
          callback()
        })
      }
    ], function(err){
      if (err) console.log(err)
      console.log(appName)
      res.render('dashboard', {
        appName:appName,
        name_title :  appName+" | "+"Dashboard",
        admin_data : admin_data,
        setting_data: locals.setting,
        total_user : locals.total_user,
        total_order: locals.total_order,
        total_quest: locals.total_quest,
        total_message: locals.total_message,
        recent_quest: locals.recent_quest
      })
    })
}

// ==============================
// Profile
// ==============================
exports.profile = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      User.findOne({_id:admin_data._id}).exec(function(err,user){
        if (err) return callback(err)
        locals.user = user
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('profile_admin', {
      appName: appName,
      name_title :  appName+" | "+"Profile",
      admin_data : admin_data,
      user : locals.user,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}

exports.change_profile = function(req, res){
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if(req.body.name){
    var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
    var dataUpdate = {
      name: req.body.name
    }
    if (file_url) dataUpdate.imageProfile = file_url
    User.findOneAndUpdate({_id:admin_data._id}, dataUpdate, function(err, user){
      if(err || user==null){
        req.flash('info_title', 'Information')
        req.flash('info', 'Update Profile Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/profile')
      } else {
        req.session.admin_data  = user
          req.flash('info_title', 'Information')
          req.flash('info', 'Update Profile Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/profile')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/profile')
  }
}
//Change password process
exports.change_password = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.old_password && req.body.new_password) {
    var old_password = crypto.createHash('md5').update(req.body.old_password).digest('hex')
    var new_password = crypto.createHash('md5').update(req.body.new_password).digest('hex')
    User.findOne({
      _id: admin_data._id,
      password: old_password
    }, function (err, user) {
      if (err || user == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Something went wrong')
        req.flash('info_type', 'error')
        res.redirect('/admin/profile')
      } else {
        var dataUpdate = {
          password: new_password,
          updatedDate: Date.now(),
          updatedBy: user._id
        }
        User.findOneAndUpdate({
          _id: user._id
        }, dataUpdate, function (err, user) {
          if (!err && user != null) {
            req.flash('info_title', 'Information')
            req.flash('info', 'Change Password Success')
            req.flash('info_type', 'success')
            res.redirect('/admin/profile')
          } else {
            req.flash('info_title', 'Information')
            req.flash('info', 'Change Password Failed')
            req.flash('info_type', 'error')
            res.redirect('/admin/profile')
          }
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields is required')
    req.flash('info_type', 'error')
    res.redirect('/admin/profile')
  }
}
// ==============================
// User
// ==============================
exports.user = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var user_list = []
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      User.find().populate("createdBy updatedBy").sort({createdDate : -1}).exec(function(err,user){
        if (err) return callback(err)
        locals.user_list = user
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('user', {
      appName: appName,
      name_title :  appName+" | "+"User",
      admin_data : admin_data,
      setting_data: locals.setting,
      user_list : locals.user_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.name && req.body.email && req.body.password && req.body.role){
    if(admin_data._id != req.body.id){
      var file_url = req.file?('/image/' +  res.req.file.filename) : ''
      var createData={
        name: req.body.name,
        email : req.body.email,
        phone : req.body.phone,
        password : crypto.createHash('md5').update(req.body.password).digest('hex'),
        role : req.body.role,
        imageProfile: file_url ? file_url : null,
        status:'active',
        createdDate: Date.now(),
        createdBy: admin_data._id,
      }
      User.create(createData, function (error, result) {
        if (error || result==null) {
          console.log(error)
          req.flash('info_title', 'Information')
          req.flash('info', 'Create User Failed')
          req.flash('info_type', 'error')
          res.redirect('/admin/user')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Create User Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/user')
        }
      })
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', "You can't edit your own account")
      req.flash('info_type', 'error')
      res.redirect('/admin/user')
    }
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/user')
  }
}
exports.single_user = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        User.findOne({_id:req.params.id},{'__v':0}).exec(function(err,user){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (user==null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: user,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.id && req.body.name && req.body.email && req.body.role){
    if(admin_data._id != req.body.id){
      var file_url = req.file?('/image/' +  res.req.file.filename) : ''
      var updateData={
        name: req.body.name,
        email : req.body.email,
        role : req.body.role,
        updatedDate: Date.now(),
        updatedBy: admin_data._id,
      }
      if(req.body.password != '') updateData.password = crypto.createHash('md5').update(req.body.password).digest('hex')
      if(file_url) updateData.imageProfile = file_url
      User.findByIdAndUpdate(req.body.id, updateData, function (error, user) {
        if (error || user==null) {
          console.log(error)
          req.flash('info_title', 'Information')
          req.flash('info', 'Edit User Failed')
          req.flash('info_type', 'error')
          res.redirect('/admin/user')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Edit User Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/user')
        }
      })
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', "You can't edit your own account")
      req.flash('info_type', 'error')
      res.redirect('/admin/user')
    }
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/user')
  }
}
exports.delete_user_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    if(admin_data._id!=req.params.id){
      User.findByIdAndRemove(req.params.id, function (error, result) {
        if (error) {
          var err = { status:400, message:error }
          return res.send(JSON.stringify(err))
        } if (result==null) {
          req.flash('info_title', 'Information')
          req.flash('info', 'User Not Nound')
          req.flash('info_type', 'error')
          res.redirect('/admin/user')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Delete User Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/user')
        }
      })
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', 'You can\'t delete your own account')
      req.flash('info_type', 'error')
      res.redirect('/admin/user')
    }
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/user')
  }
}

// ==============================
// Feature
// ==============================
exports.feature = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Feature.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.feature_list = result
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    console.log(admin_data) 
    res.render('feature', {
      appName:appName,
      name_title :  appName+" | "+"Feature",
      admin_data : admin_data,
      setting_data: locals.setting,
      feature_list : locals.feature_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_feature_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var file_url = req.file?('/image/' +  res.req.file.filename) : ''   
  if (req.body.name && file_url){
    var createData={
      name: req.body.name,
      description:req.body.description,
      imageUrlLogo: file_url ? file_url : null,
      createdDate: Date.now(),
      createdBy: admin_data._id,      
    }
    Feature.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Feature Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/feature')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Feature Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/feature')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/feature')
  }
}
exports.single_feature = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Feature.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_feature_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data

  if (req.body.id && req.body.name ){
    var updateData={
      name: req.body.name,
      description:req.body.description,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''  
    if(file_url) updateData.imageUrlLogo = file_url
    Feature.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Feature Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/feature')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Feature Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/feature')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/feature')
  }
}
exports.delete_feature_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Feature.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Feature Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/feature')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Feature Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/feature')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/feature')
  }
}

// ==============================
// Testimonial
// ==============================
exports.testimonial = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Testimonial.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.testimonial_list = result
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    console.log(admin_data) 
    res.render('testimonial', {
      appName:appName,
      name_title :  appName+" | "+"Testimonial",
      admin_data : admin_data,
      setting_data : locals.setting,
      testimonial_list : locals.testimonial_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_testimonial_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var file_url = req.file?('/image/' +  res.req.file.filename) : ''   
  if (req.body.name && file_url){
    var createData={
      name: req.body.name,
      caption:req.body.caption,
      image: file_url ? file_url : null,
      stars:req.body.stars,
      confirmed: req.body.confirmed,
      createdDate: Date.now(),
      createdBy: admin_data._id,      
    }
    Testimonial.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Testimonial Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/testimonial')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Testimonial Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/testimonial')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/testimonial')
  }
}
exports.single_testimonial = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Testimonial.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_testimonial_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data

  if (req.body.id && req.body.name ){
    var updateData={
      name: req.body.name,
      caption:req.body.caption,
      stars:req.body.stars,
      confirmed: req.body.confirmed,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''  
    if(file_url) updateData.image = file_url
    Testimonial.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Testimonial Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/testimonial')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Testimonial Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/testimonial')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/testimonial')
  }
}
exports.delete_testimonial_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Testimonial.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Testimonial Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/testimonial')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Testimonial Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/testimonial')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/testimonial')
  }
}

// ==============================
// FAQ
// ==============================
exports.faq = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Faq.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.faq_list = result
        callback()
      })
    } ,
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    } 
  ], function(err) { 
    if (err) console.log(err)
    res.render('faq', {
      name_title :  appName+" | "+"FAQ",
      admin_data : admin_data,
      setting_data : locals.setting,
      faq_list : locals.faq_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_faq_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.question && req.body.answer){
    var createData={
      question: req.body.question,
      answer: req.body.answer,
      createdDate: Date.now(),
      createdBy: admin_data._id,      
    }
    Faq.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create FAQ Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/faq')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create FAQ Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/faq')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/faq')
  }
}
exports.single_faq = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Faq.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_faq_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  console.log(req.body)
  if (req.body.id && req.body.question && req.body.answer){
    var updateData = {
      question: req.body.question,
      answer: req.body.answer,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    Faq.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit FAQ Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/faq')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit FAQ Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/faq')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/faq')
  }
}
exports.delete_faq_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Faq.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'FAQ Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/faq')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete FAQ Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/faq')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/faq')
  }
}

// ==============================
// NEWS
// ==============================
exports.news = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      News.find().populate('brand').sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.news_list = result
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('news', {
      name_title :  appName+" | "+"NEWS",
      admin_data : admin_data,
      setting_data : locals.setting,
      news_list : locals.news_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.title && req.body.subtitle && req.body.content){
    
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var createData={
      title: req.body.title,
      subtitle: req.body.subtitle,
      content: req.body.content,
      createdDate: Date.now(),
      createdBy: admin_data._id,      
    }
    if(file_url) createData.image = file_url
    News.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create News Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/news')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create News Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/news')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/news')
  }
}
exports.single_news = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        News.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.id){
    
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var updateData = {
      title: req.body.title,
      subtitle: req.body.subtitle,
      content: req.body.content,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    if(file_url) updateData.image = file_url
    News.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit News Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/news')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit News Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/news')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/news')
  }
}
exports.delete_news_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    News.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'News Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/news')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete News Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/news')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/news')
  }
}

// ==============================
// QUEST
// ==============================
exports.quest = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Quest.find().populate('createdBy').sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.quest_list = result
        callback()
      })
    },
    function(callback) {
      SurveyProfiling.find().populate('createdBy').sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.survey_profiling = result
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('quest_admin', {
      appName:appName,
      name_title :  appName+" | "+"Quest",
      admin_data : admin_data,
      setting_data : locals.setting,
      quest_list : locals.quest_list,
      type_list : SurveyProfiling.schema.path('type').enumValues,
      survey_profiling: locals.survey_profiling,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
//Create Quest
exports.create_quest = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  Setting.findOne({}, function (err, setting) {
    if (err) console.log(err)
    SurveyProfiling.find({}, function (err, survey_profiling) {
      if (err) console.log(err)
    res.render('create_quest_admin', {
      name_title: appName + " | Create Quest",
      admin_data: admin_data,
      appName: appName,
      survey_profiling:survey_profiling,
      setting_data: setting
    })
  })
})
}
//Create Quest
exports.create_quest_process = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var type = (req.params.type ? req.params.type : "")
  var id = (req.params.id ? req.params.id : "")
  if (req.body.title && req.body.question) {
    req.body.question = (req.body.question instanceof Array) ? req.body.question : [req.body.question]
    req.body.type = (req.body.type instanceof Array) ? req.body.type : [req.body.type]
    req.body.required = (req.body.required instanceof Array) ? req.body.required : [req.body.required]
    req.body.number = (req.body.number instanceof Array) ? req.body.number : [req.body.number]
    req.body.custom = (req.body.custom instanceof Array) ? req.body.custom : [req.body.custom]
    req.body.data = (req.body.data instanceof Array) ? req.body.data : [req.body.data]
    req.body.display_logic = (req.body.display_logic instanceof Array) ? req.body.display_logic : [req.body.display_logic]
    req.body.id_question = (req.body.id_question instanceof Array) ? req.body.id_question : [req.body.id_question]
    req.body.sequence = (req.body.sequence instanceof Array) ? req.body.sequence : [req.body.sequence]
    var questions_raw = req.body.question
    var questions_data = []
    var profiling_data = []
    questions_raw.map((o, i) => {
      
    var display_logic_data = []
    if(req.body.display_logic[i]){
      // console.log("DISPLAY LOGIC", req.body.display_logic[i])
      var parsed = JSON.parse(req.body.display_logic[i])
      // console.log(parsed)
      parsed.map((p)=>{
        var temp = {
          logic_type:(p.logic_type?p.logic_type.toString():""),
          question :(p.question?p.question.toString():""),
          is_not: (p.is_not?p.is_not.toString():""),
          is_selected :(p.is_selected?p.is_selected.toString():""),
          device:(p.device?p.device.toString():""),
          equal : (p.equal?p.equal.toString():""),
          equal_to: (p.equal_to?p.equal_to.toString():""),
          answer:  (p.answer?p.answer.toString():""),
          or_and:  (p.or_and?p.or_and.toString():"")
        }
        display_logic_data.push(temp)
      })
      console.log("Index : ", i)
    }
    console.log("Display Logic : ", display_logic_data)
      var new_obj = {
        id: req.body.id_question[i],
        question: o,
        type: req.body.type[i],
        required: (req.body.required[i]=="true"?true:false),
        number:req.body.number[i],
        custom:(req.body.custom[i]?req.body.custom[i]:false),
        pageBreak:(req.body.pageBreak[i]=="true"?true:false),
        sequence:req.body.sequence[i],
        displayLogic:display_logic_data
      }
      if (req.body.data[i]) new_obj.data = req.body.data[i].split(",")
      questions_data.push(new_obj)
    })
    if (req.body.profiling) {
      var profiling_raw = JSON.parse(req.body.profiling)
      profiling_raw.map(o => {
        var new_obj = {
          surveyprofiling: o._id,
          value: o.value,
        }
        if (o.minValue) new_obj.minValue = o.minValue
        if (o.maxValue) new_obj.minValue = o.maxValue
        profiling_data.push(new_obj)
      })
    }
    var dataCreate = {
      code: random_password().toString(),
      userRef: admin_data._id,
      title: req.body.title,
      description: req.body.description,
      targetRespondent: req.body.targetRespondent,
      questions: questions_data,
      profiling: profiling_data,
      randomize: req.body.randomize,
      status: (req.body.save == "published" ? 'published' : 'drafted'),
      totalRespondent: 0,
      type: 'quest',
      createdDate: Date.now(),
      createdBy: admin_data._id
    }
    Quest.create(dataCreate, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Failed to create the quest')
        req.flash('info_type', 'error')
        res.redirect('/admin/quest')
      } else {
          PreviewQuest.deleteMany({
            userRef: admin_data._id
          }, function (err, preview_quest) {
            if (err) console.log(err)
            Setting.findOne({}, function (err, setting) {
              if (err) console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Create quest successful')
              req.flash('info_type', 'success')
              res.redirect('/admin/quest')
            })
          })
      }
    })
  }
}

exports.single_quest = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Quest.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.edit_quest = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data  = req.session.admin_data
  if (req.params.id) {
    Quest.findOne({
      _id: req.params.id
    }).populate("profiling.surveyprofiling").exec(function (error, quest) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (quest == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest Not Found')
        req.flash('info_type', 'error')
        res.redirect('/admin/quest')
      } else {
        Setting.findOne({}, function (err, setting) {
          SurveyProfiling.find({}, function (err, survey_profiling) {
            res.render('edit_quest_admin', {
              appName: appName,
              name_title: appName + " | " + quest.title,
              appName: appName,
              setting_data: setting,
              quest: quest,
              type_list: SurveyProfiling.schema.path('type').enumValues,
              survey_profiling: survey_profiling,
              admin_data: admin_data,
            })
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/quest')
  }
}

exports.update_quest_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var type = (req.params.type ? req.params.type : "")
  var id = (req.params.id ? req.params.id : "")
  if (req.body.title && req.body.question && req.params.id) {
    req.body.question = (req.body.question instanceof Array) ? req.body.question : [req.body.question]
    req.body.type = (req.body.type instanceof Array) ? req.body.type : [req.body.type]
    req.body.required = (req.body.required instanceof Array) ? req.body.required : [req.body.required]
    req.body.number = (req.body.number instanceof Array) ? req.body.number : [req.body.number]
    req.body.custom = (req.body.custom instanceof Array) ? req.body.custom : [req.body.custom]
    req.body.data = (req.body.data instanceof Array) ? req.body.data : [req.body.data]
    console.log(req.body.data)
    var questions_raw = req.body.question
    var questions_data = []
    var profiling_data = []
    questions_raw.map((o, i) => {
      
    var display_logic_data = []
    if(req.body.display_logic[i]){
      // console.log("DISPLAY LOGIC", req.body.display_logic[i])
      var parsed = JSON.parse(req.body.display_logic[i])
      // console.log(parsed)
      parsed.map((p)=>{
        var temp = {
          logic_type:(p.logic_type?p.logic_type.toString():""),
          question :(p.question?p.question.toString():""),
          is_not: (p.is_not?p.is_not.toString():""),
          is_selected :(p.is_selected?p.is_selected.toString():""),
          device:(p.device?p.device.toString():""),
          equal : (p.equal?p.equal.toString():""),
          equal_to: (p.equal_to?p.equal_to.toString():""),
          answer:  (p.answer?p.answer.toString():""),
          or_and:  (p.or_and?p.or_and.toString():"")
        }
        display_logic_data.push(temp)
      })
      console.log("Index : ", i)
    }
    console.log("Display Logic : ", display_logic_data)
      var new_obj = {
        id: req.body.id_question[i],
        question: o,
        type: req.body.type[i],
        required: (req.body.required[i]=="true"?true:false),
        number:req.body.number[i],
        custom:(req.body.custom[i]?req.body.custom[i]:false),
        pageBreak:(req.body.pageBreak[i]=="true"?true:false),
        sequence:req.body.sequence[i],
        displayLogic:display_logic_data
      }
      if (req.body.data[i]) {
        new_obj.data = req.body.data[i].split(",")
        console.log("KONTOL", req.body.data[i].split(","))
      }
      console.log(new_obj)
      questions_data.push(new_obj)
    })
    if (req.body.profiling) {
      var profiling_raw = JSON.parse(req.body.profiling)
      profiling_raw.map(o => {
        var new_obj = {
          surveyprofiling: o._id,
          value: o.value,
        }
        if (o.minValue) new_obj.minValue = o.minValue
        if (o.maxValue) new_obj.minValue = o.maxValue
        profiling_data.push(new_obj)
      })
    }
    var dataUpdate = {
      userRef: admin_data._id,
      title: req.body.title,
      description: req.body.description,
      targetRespondent: req.body.targetRespondent,
      questions: questions_data,
      profiling: profiling_data,
      status: (req.body.save == "published" ? 'published' : 'drafted'),
      totalRespondent: 0,
      type: 'quest',
      updatedDate: Date.now(),
      updatedBy: admin_data._id
    }
    Quest.findOneAndUpdate({_id:req.params.id}, dataUpdate, function (err, quest) {
      if (err || quest == null) {
        console.log(err)
        req.flash('info_title', 'Information')
        req.flash('info', 'Failed to update the quest')
        req.flash('info_type', 'error')
        res.redirect('/quest')
      } else {
        if (type == "paid") {
          var dataUpdate = {
            questRef: quest._id,
            updatedDate: Date.now(),
            updatedBy: admin_data._id
          }
          Order.findOneAndUpdate({
            orderId: id
          }, dataUpdate, function (err, order) {
            if (err || order == null) {
              console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Failed to update the quest')
              req.flash('info_type', 'error')
              res.redirect('/admin/quest')
            } else {
              PreviewQuest.deleteMany({
                userRef: admin_data._id
              }, function (err, preview_quest) {
                if (err) console.log(err)
                Setting.findOne({}, function (err, setting) {
                  if (err) console.log(err)
                  req.flash('info_title', 'Information')
                  req.flash('info', 'Update quest success')
                  req.flash('info_type', 'success')
                  res.redirect('/admin/quest')
                })
              })
            }
          })
        } else {
          PreviewQuest.deleteMany({
            userRef: admin_data._id
          }, function (err, preview_quest) {
            if (err) console.log(err)
            Setting.findOne({}, function (err, setting) {
              if (err) console.log(err)
              req.flash('info_title', 'Information')
              req.flash('info', 'Update quest success')
              req.flash('info_type', 'success')
              res.redirect('/admin/quest')
            })
          })
        }
      }
    })
  }
}
exports.delete_quest_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Quest.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/quest')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Quest Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/quest')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/quest')
  }
}

exports.view_quest = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id) {
    Quest.findOne({
      _id: req.params.id
    }, function (error, quest) {
      if (error) {
        var err = {
          status: 400,
          message: error
        }
        return res.send(JSON.stringify(err))
      }
      if (quest == null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Quest not found')
        req.flash('info_type', 'error')
        res.redirect('/admin/quest')
      } else {
        Setting.findOne({}, function (err, setting) {
          res.render('view_quest_admin', {
            name_title: appName + " | " + quest.title,
            appName: appName,
            setting_data: setting,
            quest: quest,
            admin_data: admin_data,
          })
        })
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/quest')
  }
}

// ==============================
// Product
// ==============================
exports.product = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Product.find().populate('brand productgroup productcategory').sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.product_list = result
        // console.log(result)
        callback()
      })
    },
    function(callback) {
      Brand.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.brand_list = result
        callback()
      })
    },
    function(callback) {
      ProductGroup.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.product_group_list = result
        callback()
      })
    },
    function(callback) {
      // ProductCategory.find().sort({createdDate : -1}).exec(function(err,result){
      //   if (err) return callback(err)
      //   locals.product_category_list = result
      //   callback()
      // })
      ProductCategory.aggregate([
        {
          $match: {
            headOfficeRef: ObjectId(admin_data.headOfficeRef._id)
          }
        },
        {
          $group : {
            _id : "$productGroup",
            data : {
              $push:  "$$ROOT"
            }
          }
        }, 
      ]).exec(function(err, result) {    
        if (err) return callback(err)
        locals.product_category_list = result  
        callback()  
      })         
    }          
  ], function(err) { 
    if (err) console.log(err)
    res.render('product', {
      name_title :  appName+" | "+"Product",
      admin_data : admin_data,
      product_list : locals.product_list,
      product_group_list : locals.product_group_list,
      product_category_list : locals.product_category_list,
      brand_list : locals.brand_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  console.log(req.body)
  if (req.body.name && req.body.productCode && req.body.price && req.body.description && // req.body.PLUId
    req.body.brand && req.body.productgroup && req.body.productcategory){
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var createData={
      name: req.body.name,
      productCode: req.body.productCode,
      PLUId: req.body.PLUId,
      price: req.body.price,
      description: req.body.description,
      brand: req.body.brand,
      productgroup: req.body.productgroup,
      productcategory: req.body.productcategory,
      headOfficeRef : admin_data.headOfficeRef._id,
      createdDate: Date.now(),
      createdBy: admin_data._id,      
    }
    if(file_url) createData.imageProduct = file_url
    Product.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Product Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/product')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Product Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/product')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/product')
  }
}
exports.single_product = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Product.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_product_process = function (req, res, next) {
  auth_redirect(req, res)
  console.log(req.body)
  var admin_data = req.session.admin_data
  if (req.body.id && req.body.name && req.body.productCode && req.body.price && req.body.description && // req.body.PLUId
    req.body.brand && req.body.productgroup && req.body.productcategory){
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''      
    var updateData = {
      name: req.body.name,
      productCode: req.body.productCode,
      PLUId: req.body.PLUId,
      price: req.body.price,
      description: req.body.description,
      brand: req.body.brand,
      productgroup: req.body.productgroup,
      productcategory: req.body.productcategory,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    if(file_url) createData.imageProduct = file_url
    Product.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Product Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/product')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Product Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/product')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/product')
  }
}
exports.delete_product_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Product.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Product Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/product')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Product Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/product')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/product')
  }
}


// ==============================
// Article
// ==============================
exports.article = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Article.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.article_list = result
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('article', {
      name_title :  appName+" | "+"Article",
      admin_data : admin_data,
      article_list : locals.article_list,
      type_list : Article.schema.path('type').enumValues,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_article_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.title && req.body.subtitle && req.body.type && req.body.content){
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var createData={
      title: req.body.title,
      subtitle : req.body.subtitle,
      type : req.body.type,
      content : req.body.content,
      headOfficeRef : admin_data.headOfficeRef._id,
      image: file_url ? file_url : null,
      createdDate: Date.now(),
      createdBy: admin_data._id,
    }
    Article.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Article Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/article')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Article Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/article')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/article')
  }
}
exports.single_article = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Article.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_article_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.id && req.body.title && req.body.subtitle && req.body.type && req.body.content){
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var updateData = {
      title: req.body.title,
      subtitle : req.body.subtitle,
      type : req.body.type,
      content : req.body.content,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    if(file_url) updateData.image = file_url
    Article.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Article Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/article')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Article Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/article')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/article')
  }
}
exports.delete_article_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Article.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Article Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/article')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Article Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/article')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/article')
  }
}

// ==============================
// REWARD
// ==============================
exports.reward = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Reward.find().populate('storeRef').sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.reward_list = result
        callback()
      })
    },
    function(callback) {
      Store.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.store_list = result
        callback()
      })
    }    
  ], function(err) { 
    if (err) console.log(err)
    res.render('reward', {
      name_title :  appName+" | "+"Reward",
      admin_data : admin_data,
      reward_list : locals.reward_list,
      store_list : locals.store_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_reward_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.title && req.body.pointsRequired && req.body.storeRef){
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var createData={
      title: req.body.title,
      pointsRequired : req.body.pointsRequired,
      storeRef : req.body.storeRef,
      headOfficeRef : admin_data.headOfficeRef._id,
      image: file_url ? file_url : null,
      createdDate: Date.now(),
      createdBy: admin_data._id,
    }
    Reward.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Reward Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/reward')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create Reward Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/reward')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/reward')
  }
}
exports.single_reward = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Reward.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_reward_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.id && req.body.title && req.body.pointsRequired && req.body.storeRef){
    var file_url = req.file?('/image/' +  res.req.file.filename) : ''
    var updateData = {
      title: req.body.title,
      pointsRequired : req.body.pointsRequired,
      storeRef : req.body.storeRef,
      updatedDate: Date.now(),
      updatedBy: admin_data._id,
    }
    if(file_url) updateData.image = file_url
    Reward.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Reward Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/reward')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Edit Reward Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/reward')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/reward')
  }
}
exports.delete_reward_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    Reward.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Reward Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/reward')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Reward Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/reward')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/reward')
  }
}

// ==============================
// SURVEY PROFILING
// ==============================
exports.survey_profiling = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      SurveyProfiling.find().sort({createdDate : -1}).exec(function(err,result){
        if (err) return callback(err)
        locals.survey_profiling_list = result
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('survey_profiling', {
      name_title :  appName+" | "+"SurveyProfiling",
      admin_data : admin_data,
      setting_data: locals.setting,
      survey_profiling_list : locals.survey_profiling_list,
      type_list : SurveyProfiling.schema.path('type').enumValues,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_survey_profiling_process = function (req, res, next) {
  auth_redirect(req, res)
  console.log(req.body)
  var admin_data = req.session.admin_data
  if (req.body.question && req.body.type && req.body.required){

    var createData={
      question:req.body.question,
      type:req.body.type,
      data:req.body.data.split(","),
      required: (req.body.required=="true"?true:false),
      createdDate: Date.now(),
      createdBy: admin_data._id,
    }
    SurveyProfiling.create(createData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Create SurveyProfiling Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/survey_profiling')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Create SurveyProfiling Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/survey_profiling')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/survey_profiling')
  }
}
exports.single_survey_profiling = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, user_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !user_session ){
      var err = { status:400, message:'Token Invalid', data: user_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        SurveyProfiling.findById(req.params.id,{'__v':0}).exec(function(err,result){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (result == null) {
            var err = { status:400, message:'User not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data:result,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'User id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_survey_profiling_process = function (req, res, next) {
  auth_redirect(req, res)
  console.log(req.body)
  var admin_data = req.session.admin_data
  if (req.body.id && req.body.question && req.body.type && req.body.required){
    var updateData={
      question:req.body.question,
      type:req.body.type,
      data:req.body.data.split(","),
      required: (req.body.required=="true"?true:false),
      createdDate: Date.now(),
      createdBy: admin_data._id,
    }
    SurveyProfiling.findByIdAndUpdate(req.body.id, updateData, function (error, result) {
      if (error || result==null) {
        console.log(error)
        req.flash('info_title', 'Information')
        req.flash('info', 'Update Survey Profiling Failed')
        req.flash('info_type', 'error')
        res.redirect('/admin/survey_profiling')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Update Survey Profiling Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/survey_profiling')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/survey_profiling')
  }
}
exports.delete_survey_profiling_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    SurveyProfiling.findByIdAndRemove(req.params.id, function (error, result) {
      if (error) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } if (result==null) {
        req.flash('info_title', 'Information')
        req.flash('info', 'Survey Profiling Not Nound')
        req.flash('info_type', 'error')
        res.redirect('/admin/survey_profiling')
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete SurveyProfiling Success')
        req.flash('info_type', 'success')
        res.redirect('/admin/survey_profiling')
      }
    })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/survey_profiling')
  }
}


////////////////////////////////////
// SETTING
exports.setting = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var user_list = []
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Setting.findOne().exec(function(err,result){
        if (err) return callback(err)
        locals.setting = result
        callback()
      })  
    }
  ], function(err) { 
    if (err) console.log(err)
    console.log(locals.setting)
    res.render('setting', {
      appName:appName,
      name_title :  appName+" | "+"Setting",
      admin_data : admin_data,
      setting : locals.setting,
      setting_data : locals.setting,
      messages : messages,
      messages_type : messages_type,
      messages_name_title : messages_name_title
    })
  })
}
///////////////////////////////////
//UPDATE
exports.setting_common_save = function(req, res){
  auth_redirect(req,res)
  var admin_data = req.session.admin_data
  var files = req.files
  var logo = req.files['logo'] ? '/image/' + req.files['logo'][0].filename : ''
  var headerImage = req.files['headerImage'] ? '/image/' + req.files['headerImage'][0].filename : ''
  var aboutUsImage = req.files['aboutUsImage'] ? '/image/' + req.files['aboutUsImage'][0].filename : ''
  var dataUpdate = {
    phone: req.body.phone,
    address: req.body.address,
    email: req.body.email,
    headerTitle:req.body.headerTitle,
    headerCaption: req.body.headerCaption,
    aboutUsTitle:req.body.aboutUsTitle,
    aboutUsCaption:req.body.aboutUsCaption,
    termsAndCondition: req.body.termsAndCondition,
    prologue: req.body.prologue,
    price: req.body.price,
    googleAnalytic:req.body.googleAnalytic,
    freeQuest:req.body.freeQuest,
    updatedDate: Date.now(),
    updatedBy: admin_data._id,
    $setOnInsert: {
      createdDate: Date.now(),
      createdBy: admin_data._id,
    }
  }
  if(logo) dataUpdate.logo = logo
  if(headerImage) dataUpdate.headerImage = headerImage
  if(aboutUsImage) dataUpdate.aboutUsImage = aboutUsImage
  console.log(dataUpdate)
  var options = {
    upsert: true,
    new: true,
    setDefaultsOnInsert: true
  }
  Setting.findOneAndUpdate({}, dataUpdate, options, function(err, result){
    if(err || result == null) {
      req.flash('info_title', 'Information')
      req.flash('info', 'Update Setting Data Failed')
      req.flash('info_type', 'error')
      res.redirect('/admin/setting')
    } else {
      console.log(result)
      req.flash('info_title', 'Information')
      req.flash('info', 'Update Setting Data Succeeded')
      req.flash('info_type', 'success')
      res.redirect('/admin/setting')
    }
  })
}

////////////////////////////////////
//Message 

exports.message = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Message.find().exec(function(err,result){
        if (err) return callback(err)
        locals.message = result
        callback()
      })  
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    console.log(locals.setting)
    res.render('message', {
      appName:appName,
      name_title :  appName+" | "+"Message",
      admin_data : admin_data,
      setting_data : locals.setting,
      message : locals.message,
      messages : messages,
      messages_type : messages_type,
      messages_name_title : messages_name_title
    })
  })
}

//DELETE MESSAGE
exports.delete_message_process = function (req, res, next) {
  auth_redirect(req, res)
  if (req.params.id){
    Message.findByIdAndRemove(req.params.id, function (error, result) {
      if (error || result==null) {
        var err = { status:400, message:error }
        return res.send(JSON.stringify(err))
      } else {
        req.flash('info_title', 'Information')
        req.flash('info', 'Delete Data success')
        req.flash('info_type', 'success')
        res.redirect('/admin/message')
      }
      })
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin')
  }
}      

function auth_redirect(req, res) {
  sess = req.session
  if (sess.admin_login) return true
  else res.redirect('/admin/signin')
}

// Transform hours like "1:45" into the total number of minutes, "105". 
function hoursToMinutes(hours) 
{ 
    hours = hours.toString()
    var split = hours.split(":")
    var hours = parseInt(split[0])
    var minutes = parseInt(split[1])
    console.log('hours : ', hours)
    console.log('minutes : ', minutes)
    console.log('minutes_total : ', (hours*60)+minutes)
    return parseInt((hours * 60) + minutes) 
} 

// Transform minutes like "105" into hours like "1:45". 
function minutesToHours(minutes) 
{ 
    var hours = parseInt(minutes / 60)
    return hours
}  

function random_password(){
  var pass = (Math.random().toString(36).substr(2)).toUpperCase()
  pass = crypto.createHash('md5').update(pass).digest('hex')
  return pass.substr(0, 5)
}

function get_bonus_value(salary,bonus_data){
  // console.log(salary,bonus_data)
  var res = bonus_data.valueType == "percentage" ? salary*bonus_data.value/100 : bonus_data.value
  console.log("res", (typeof res == 'undefined'), res, (typeof res == 'undefined') ? 0 : res)
  if(typeof res == 'undefined') res = 0
  return res
}

//signout
exports.signout = function (req, res) {
  Session.findOneAndRemove({
    token: req.session.key
  }, function (error) {
    if (error) {
      console.log('Login Berhasil');
    } else {
      req.session.destroy(function (err) {
        if (err) {
          console.log(err);
        } else {
          res.redirect('/admin/');
        }
      });
    }
  })
};

//download answers bulking
exports.download_answer = function(req, res){
  auth_redirect(req, res)
  Quest.findOne({_id:req.params.id}, function(err, quest){
    if(err ||quest==null){
      req.flash('info_title', 'Information')
      req.flash('info', 'Invalid ID Quest')
      req.flash('info_type', 'success')
      res.redirect('/admin/quest')
    } else {
      UserQuest.find({questRef:quest._id}).populate("userRef").exec(function(err, user_quest){
        if(err || user_quest==null){
          req.flash('info_title', 'Information')
          req.flash('info', 'No Answer Found')
          req.flash('info_type', 'success')
          res.redirect('/admin/quest')
        } else {
          var BASE_DIR = __dirname.replace('\controllers', '')
          var dest = BASE_DIR+"/assets/exports/"
          var writeStream = fs.createWriteStream(dest+quest.code+".xls")
          var header="No."+"\t"+"Email"+"\t"
          quest.questions.map((o)=>{
            header+= removeElements(o.number) +"\t"
          })
          header+="\n"
          var row = ""
          user_quest.map((o, i)=>{
            row+=i+1+"\t"+o.userRef.email+"\t"
            o.answers.map((p)=>{
              var question = quest.questions.find((o)=>o.id.toString()==p.id.toString())
              if(question){
                if(question.type=="file")
                row+=webURL+p.answer.substr(6)+"\t"
                else
                row+=p.answer+"\t"
              }
              else
              row+="-"+"\t"
            })
            row+="\n"
          })
          writeStream.write(header)
          writeStream.write(row)
          writeStream.close()
          res.redirect("/exports/"+quest.code+".xls")
        }
      })
    }
  })
}

// ==============================
// Order
// ==============================
exports.order = function (req, res) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  var locals = {}
  var order_list = []
  var messages = ""
  var messages_type = ""
  var messages_name_title = ""
  res.locals.info = req.flash('info')
  res.locals.info_type = req.flash('info_type')
  res.locals.info_title = req.flash('info_title')
  if (typeof res.locals.info != 'undefined'){
    var messages = res.locals.info
    var messages_type = res.locals.info_type
    var messages_name_title = res.locals.info_title
  }
  async.parallel([
    function(callback) {
      Order.find().populate("userRef createdBy updatedBy").sort({createdDate : -1}).exec(function(err,order){
        if (err) return callback(err)
        locals.order_list = order
        callback()
      })
    },
    function(callback) {
      Setting.findOne({}).exec(function(err,setting){
        if (err) return callback(err)
        locals.setting = setting
        callback()
      })
    }
  ], function(err) { 
    if (err) console.log(err)
    res.render('order', {
      appName: appName,
      name_title :  appName+" | "+"Order",
      admin_data : admin_data,
      setting_data:locals.setting,
      order_list : locals.order_list,
      messages : messages,
      messages_type : messages_type,
      messages_name_title: messages_name_title
    })
  })
}
exports.create_order_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.name && req.body.email && req.body.password && req.body.role){
    if(admin_data._id != req.body.id){
      var file_url = req.file?('/image/' +  res.req.file.filename) : ''
      var createData={
        name: req.body.name,
        email : req.body.email,
        phone : req.body.phone,
        password : crypto.createHash('md5').update(req.body.password).digest('hex'),
        role : req.body.role,
        imageProfile: file_url ? file_url : null,
        status:'active',
        createdDate: Date.now(),
        createdBy: admin_data._id,
      }
      Order.create(createData, function (error, result) {
        if (error || result==null) {
          console.log(error)
          req.flash('info_title', 'Information')
          req.flash('info', 'Create Order Failed')
          req.flash('info_type', 'error')
          res.redirect('/admin/order')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Create Order Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/order')
        }
      })
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', "You can't edit your own account")
      req.flash('info_type', 'error')
      res.redirect('/admin/order')
    }
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/order')
  }
}
exports.single_order = function (req, res, next) {
  Session.validate(req.session.key, 'admin', function (error, order_session) {
    if (error) {
      var err = { status:400, message:'Something went wrong', data: error}
      return res.send(JSON.stringify(err))
    } else if ( !order_session ){
      var err = { status:400, message:'Token Invalid', data: order_session}
      return res.send(JSON.stringify(err))
    } else {
      if (req.params.id){
        Order.findOne({_id:req.params.id},{'__v':0}).exec(function(err,order){
          if (err) {
            var err = { status:400, message:err }
            return res.send(JSON.stringify(err))
          } if (order==null) {
            var err = { status:400, message:'Order not found' }
            return res.send(JSON.stringify(err))
          } else {
            var ret = {
              status:200,
              data: order,
            }
            return res.send(JSON.stringify(ret))
          }
        })
      } else {
        var err = { status:400, message:'Order id required' }
        return res.send(JSON.stringify(err))
      }
    }
  })
}
exports.update_order_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.body.id && req.body.name && req.body.email && req.body.role){
    if(admin_data._id != req.body.id){
      var file_url = req.file?('/image/' +  res.req.file.filename) : ''
      var updateData={
        name: req.body.name,
        email : req.body.email,
        role : req.body.role,
        updatedDate: Date.now(),
        updatedBy: admin_data._id,
      }
      if(req.body.password != '') updateData.password = crypto.createHash('md5').update(req.body.password).digest('hex')
      if(file_url) updateData.imageProfile = file_url
      Order.findByIdAndUpdate(req.body.id, updateData, function (error, order) {
        if (error || order==null) {
          console.log(error)
          req.flash('info_title', 'Information')
          req.flash('info', 'Edit Order Failed')
          req.flash('info_type', 'error')
          res.redirect('/admin/order')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Edit Order Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/order')
        }
      })
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', "You can't edit your own account")
      req.flash('info_type', 'error')
      res.redirect('/admin/order')
    }
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'All fields required')
    req.flash('info_type', 'error')
    res.redirect('/admin/order')
  }
}
exports.delete_order_process = function (req, res, next) {
  auth_redirect(req, res)
  var admin_data = req.session.admin_data
  if (req.params.id){
    if(admin_data._id!=req.params.id){
      Order.findByIdAndRemove(req.params.id, function (error, result) {
        if (error) {
          var err = { status:400, message:error }
          return res.send(JSON.stringify(err))
        } if (result==null) {
          req.flash('info_title', 'Information')
          req.flash('info', 'Order Not Nound')
          req.flash('info_type', 'error')
          res.redirect('/admin/order')
        } else {
          req.flash('info_title', 'Information')
          req.flash('info', 'Delete Order Success')
          req.flash('info_type', 'success')
          res.redirect('/admin/order')
        }
      })
    } else {
      req.flash('info_title', 'Information')
      req.flash('info', 'You can\'t delete your own account')
      req.flash('info_type', 'error')
      res.redirect('/admin/order')
    }
  } else {
    req.flash('info_title', 'Information')
    req.flash('info', 'Id Required')
    req.flash('info_type', 'error')
    res.redirect('/admin/order')
  }
}


function getPeriod(setting){ 
  var new_date = new Date()
  var month = new_date.getMonth() + 1
  var year = new_date.getFullYear()
  var start_active_period = ((month < 10) ? "0" + month : month) + "/" + ((setting.paydayDate+1 < 10) ? "0" + (setting.paydayDate+1) : setting.paydayDate+1) + "/" + year
  var end_active_period = ((month+1 < 10) ? "0" + (month+1) : month+1) + "/" + ((setting.paydayDate < 10) ? "0" + setting.paydayDate : setting.paydayDate) + "/" + year
  
  return { start_active_period:start_active_period, end_active_period:end_active_period }
}

exports.preview_quest_process = function (req, res) {
  var admin_data = req.session.admin_data
  if (admin_data) {
    req.body.question = (req.body.question instanceof Array) ? req.body.question : [req.body.question]
    req.body.type = (req.body.type instanceof Array) ? req.body.type : [req.body.type]
    req.body.required = (req.body.required instanceof Array) ? req.body.required : [req.body.required]
    req.body.number = (req.body.number instanceof Array) ? req.body.number : [req.body.number]
    req.body.custom = (req.body.custom instanceof Array) ? req.body.custom : [req.body.custom]
    req.body.data = (req.body.data instanceof Array) ? req.body.data : [req.body.data]
    req.body.display_logic = (req.body.display_logic instanceof Array) ? req.body.display_logic : [req.body.display_logic]
    req.body.id_question = (req.body.id_question instanceof Array) ? req.body.id_question : [req.body.id_question]
    req.body.sequence = (req.body.sequence instanceof Array) ? req.body.sequence : [req.body.sequence]
    var questions_raw = req.body.question
    var questions_data = []
    var profiling_data = []
    questions_raw.map((o, i) => {
      console.log(o)
      var display_logic_data = []
      if(req.body.display_logic[i]){
        // console.log("DISPLAY LOGIC", req.body.display_logic[i])
        var parsed = JSON.parse(req.body.display_logic[i])
        // console.log(parsed)
        parsed.map((p)=>{
          var temp = {
            logic_type:(p.logic_type?p.logic_type.toString():""),
            question :(p.question?p.question.toString():""),
            is_not: (p.is_not?p.is_not.toString():""),
            is_selected :(p.is_selected?p.is_selected.toString():""),
            device:(p.device?p.device.toString():""),
            equal : (p.equal?p.equal.toString():""),
            equal_to: (p.equal_to?p.equal_to.toString():""),
            answer:  (p.answer?p.answer.toString():""),
            or_and:  (p.or_and?p.or_and.toString():"")
          }
          display_logic_data.push(temp)
        })
        console.log("Index : ", i)
      }
      console.log("Display Logic : ", display_logic_data)
        var new_obj = {
          id: req.body.id_question[i],
          question: o,
          type: req.body.type[i],
          required: (req.body.required[i]=="true"?true:false),
          number:req.body.number[i],
          custom:(req.body.custom[i]?req.body.custom[i]:false),
          sequence:req.body.sequence[i],
          displayLogic:display_logic_data
        }
        if (req.body.data[i]) new_obj.data = req.body.data[i].split(",")
        questions_data.push(new_obj)
      })
    var dataCreate = {
      code: random_password(),
      userRef: admin_data._id,
      title: req.body.title,
      questions: questions_data,
      createdDate: Date.now(),
      createdBy: admin_data._id
    }
    PreviewQuest.create(dataCreate, function (err, preview_quest) {
      if (err || preview_quest == null) {
        var ret = {
          status: 400,
          message: "Something went wrong"
        }
        return res.send(JSON.stringify(ret))
      } else {
        var ret = {
          status: 200,
          message: "Create Preview Success",
          redirect: webURL + "preview_quest/" + preview_quest.code
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var ret = {
      status: 400,
      message: "You have no access for this action"
    }
    return res.send(JSON.stringify(ret))
  }

}

var removeElements = function(text) {
  text = htmlToText.fromString(text)
  return text
}