const User = require('../models/user.model')
const Session = require('../models/session.model')
const Setting = require('../models/setting.model')
const UserQuest = require('../models/user_quest.model')
const Order = require('../models/order.model')
const News = require('../models/news.model')
const Testimonial = require('../models/testimonial.model')
const Feature = require('../models/feature.model')
const Quest = require('../models/quest.model')
// const Salary = require('../models/salary.model')
var smtpTransport = require('nodemailer-smtp-transport')
var fs = require('fs')
const readXlsxFile = require('read-excel-file/node')
var QRCode = require('qrcode')
const moment = require("moment")
const xlsxj = require("xlsx-to-json-lc")
const xlsj = require("xls-to-json-lc")
var xls2json = require('xls2json')
var fileExtension = require('file-extension')
var dateFormat = require('dateformat')
var path = require('path')
var crypto = require('crypto')
var request = require('request')
const midtransClient = require('midtrans-client')
var async = require('async')
var fs = require('fs')
var cron = require('node-cron')
var nodemailer = require('nodemailer')
const mongoose = require('mongoose')
var handlebars = require('hbs')
const ObjectId = mongoose.Types.ObjectId
var readHTMLFile = function (path, callback) {
  fs.readFile(path, {
    encoding: 'utf-8'
  }, function (err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};
Order.create
// let snap = new midtransClient.Snap({
//     isProduction : false,
//     serverKey : 'SB-Mid-server-sShrl8RDY1Vi7uABiNEKnnj3',
//     clientKey : 'SB-Mid-client-y-sc0eIevHaYrBJZ'
//   });
let snap = new midtransClient.Snap({
  isProduction : true,
  serverKey : 'Mid-server-QJ0eCbcMOEl-XFpKGt74eM2A',
  clientKey : 'Mid-client-XiKTtLD7yrmd2ZNW'
});
var transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail',
  auth: {
    user: "myquestindo@gmail.com",
    pass: "imgsatujiwa"
  }
  // secureConnection: 'false',
  // tls: {
  //   ciphers: 'SSLv3',
  //   rejectUnauthorized: false
  // }
}));

//signin
exports.signin = function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  sess = req.session;
  var redirect = sess.redirect
  var locals = {};
  User.authenticate(email, password, "member", function (error, user) {
    if (error || !user || user.length <= 0) {
      var err = {
        status: 400,
        message: 'Signin failed, invalid Email/Password'
      }
      return res.send(JSON.stringify(err))
    } else {
      if (user.status == 'active') {
        locals.user = user;
        locals.user_name = user;
        var sessionData = {
          token: crypto.createHash('md5').update(req.sessionID + (new Date(user.expire))).digest('hex'),
          user: user,
          createdDate: Date.now(),
        }
        var options = {
          upsert: true,
          new: true,
          setDefaultsOnInsert: true
        }
        Session.findOneAndUpdate({
          user: ObjectId(user._id)
        }, sessionData, options, function (error, session) {
          if (!error && session != null) {
            console.log("token:", session.token)
            console.log(user)
            console.log('Login Berhasil')
            var data = {
              status: 200,
              data: sessionData
            }
            console.log("Login Success")
            return res.send(JSON.stringify(data))
          } else {
            var err = {
              status: 400,
              message: 'Signin failed, Your Session is expired'
            }
            return res.send(JSON.stringify(err))
          }
        })
      } else {
        var err = {
          status: 400,
          message: "Your account isn't active yet, please check your email to activate it"
        }
        return res.send(JSON.stringify(err))
      }
    }
  })
}

//signout
exports.signout = function (req, res) {
  if (req.headers['token']) {
    Session.findOneAndRemove({
      token: req.headers['token']
    }, function (error) {
      if (error) {
        console.log('Login Berhasil')
      } else {
        var err = {
          status: 200,
          message: 'Logout Success, please wait..'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//my profile
exports.my_profile = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        var data = {
          status: 200,
          data: session
        }
        return res.send(JSON.stringify(data))
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//signup
exports.signup = function (req, res) {
  var locals = {}
  if (req.body.name && req.body.email && req.body.password) {
    //check if email is already registered
    User.findOne({
      email: req.body.email
    }, function (err, user) {
      if (err) {
        var err = {
          status: 400,
          message: 'Something went wrong'
        }
        return res.send(JSON.stringify(err))
      } else if (user) {
        var err = {
          status: 400,
          message: 'Email is already registered'
        }
        return res.send(JSON.stringify(err))
      } else {
        var dataCreate = {
          headOfficeRef: req.headers['headofficeref'],
          name: req.body.name,
          email: req.body.email,
          password: crypto.createHash('md5').update(req.body.password).digest('hex'),
          role: 'member',
          status: 'unactive',
          totalPoint: 0,
          createdDate: Date.now()
        }
        User.create(dataCreate, function (err, user) {
          if (err || user == null) {
            var err = {
              status: 400,
              message: 'Something went wrong'
            }
            return res.send(JSON.stringify(err))
          } else {
            Setting.findOne({}, function (err, settings) {
              if (err) {
                console.log(err)
                var err = {
                  status: 400,
                  message: "Something went wrong"
                }
                return res.send(JSON.stringify(err))
              } else {
                //Send Email for verification
                var BASE_DIR = __dirname.replace('\controllers', '')
                readHTMLFile(BASE_DIR + '/assets/html_template/verification.html', function (err, html) {
                  var template = handlebars.compile(html);
                  var replacements = {
                    user_name: user.name,
                    user_email: user.email,
                    appName: appName,
                    confirmation_url: webURL + "verification/" + user._id,
                    year: new Date().getFullYear(),
                    email_admin: settings.email,
                    phone_admin: settings.phone,
                    address_admin: settings.address
                  };
                  var htmlToSend = template(replacements);
                  var mailOptions = {
                    form: 'Administrator from @myquest.id <'+settings.email+'>',
                    to: user.email,
                    subject: appName + ' Account Activation',
                    html: htmlToSend
                  }
                  transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                      var err = {
                        status: 400,
                        message: error
                      }
                      return res.send(JSON.stringify(err))
                    } else {
                      var err = {
                        status: 200,
                        message: 'Register Successful, Please check your email to activate your account'
                      }
                      return res.send(JSON.stringify(err))
                    }
                  })
                })
              }
            })
          }
        })
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'All fields is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//update profile
exports.update_profile = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {

        var file_url = req.file ? ('/image/' + res.req.file.filename) : ''
        var dataUpdate = {
          name: req.body.name,
          phone: req.body.phone,
          birthPlace: req.body.birthPlace,
          birthDate: req.body.birthDate,
          gender: req.body.gender,
          religion: req.body.religion,
          address: req.body.address,
          updatedDate: Date.now(),
          updatedBy: session.user._id
        }
        if (file_url) dataUpdate.imageProfile = file_url
        User.findOneAndUpdate({
          _id: session.user._id
        }, dataUpdate, function (err, user) {
          if (err || user == null) {
            console.log(err)
            var err = {
              status: 400,
              message: 'Something went wrong'
            }
            return res.send(JSON.stringify(err))
          } else {
            var err = {
              status: 200,
              message: 'Update User Successful'
            }
            return res.send(JSON.stringify(err))
          }
        })

      } else {
        var data = {
          status: 400,
          message: 'Session invalid'
        }
        return res.send(JSON.stringify(data))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//dashbaord
exports.dashboard = function (req, res) {
  console.log("bangsat")
  var locals = {}
  var token = req.headers['token']
  var limit = 5
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        Quest.find({
          userRef: session.user._id
        }).count().exec(function (err, quest) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            UserQuest.find({
              userRef: session.user._id
            }).count().exec(function (err, user_quest) {
              if (err) {
                var err = {
                  status: 400,
                  message: 'Server error'
                }
                return res.send(JSON.stringify(err))
              } else {
                Order.find({
                  userRef: session.user._id
                }).count().exec(function (err, order) {
                  if (err) {
                    var err = {
                      status: 400,
                      message: 'Server error'
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var data_in = {
                      session: session,
                      order: order,
                      user_quest: user_quest,
                      quest: quest
                    }
                    var data = {
                      status: 200,
                      data: data_in
                    }
                    return res.send(JSON.stringify(data))
                  }
                })
              }
            })
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//my quest
exports.my_quest = function (req, res) {
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch Quest List
        Quest.find({
          userRef: session.user._id
        }).sort({
          createdDate: -1
        }).exec(function (err, quest) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data = {
              status: 200,
              data: quest
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//my order
exports.my_order = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  var limit = 5
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch News & info list
        Order.find({
          userRef: session.user._id
        }).sort({
          createdDate: -1
        }).exec(function (err, order) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data = {
              status: 200,
              data: order
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//news
exports.news = function (req, res) {
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch Rewards list
        News.find({}).sort({
          createdDate: -1
        }).exec(function (err, news) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data = {
              status: 200,
              data: news
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//single news
exports.single_news = function (req, res) {
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch Rewards list
        News.findById(req.params.id).sort({
          createdDate: -1
        }).exec(function (err, news) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data = {
              status: 200,
              data: news
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//promotions
exports.promotions = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  var limit = 5
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch promotion list
        Article.find({
          type: 'promotion',
          headOfficeRef: req.headers['headofficeref']
        }).sort({
          createdDate: -1
        }).limit(limit).exec(function (err, promotions) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              session: session,
              promotions: promotions
            }
            var data = {
              status: 200,
              data: data_in
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//product categories
exports.product_categories = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  var limit = 5
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch product categories list
        ProductCategory.find({
          headOfficeRef: req.headers['headofficeref']
        }).sort({
          createdDate: -1
        }).limit(limit).exec(function (err, product_categories) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              session: session,
              product_categories: product_categories
            }
            var data = {
              status: 200,
              data: data_in
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//products
exports.products = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch product categories list
        Product.find({
          productcategory: req.params.productcategory
        }).sort({
          createdDate: -1
        }).exec(function (err, products) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              session: session,
              products: products
            }
            var data = {
              status: 200,
              data: data_in
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//single product
exports.single_product = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch product categories list
        Product.findById(req.params.id).exec(function (err, product) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              session: session,
              product: product
            }
            var data = {
              status: 200,
              data: data_in
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//outlet
exports.outlet = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch product categories list
        console.log(req.headers['headofficeref'])
        Brand.find({
          headOfficeRef: req.headers['headofficeref']
        }).exec(function (err, brands) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              session: session,
              outlet: brands
            }
            var data = {
              status: 200,
              data: data_in
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

// forgot password
exports.forgot_password = function (req, res, next) {
  if (req.body.email) {
    Setting.findOne({}, function (err, settings) {
      if (err) console.log(err)
      User.findOne({
        email: req.body.email
      }, {}, function (err, user) {
        if (err || !user) {
          var err = {
            status: 400,
            message: 'Email is not registered'
          }
          return res.send(JSON.stringify(err))
        } else {
          var newpass = random_password()
          var hashed_newpass = crypto.createHash('md5').update(newpass).digest('hex');
          User.findOneAndUpdate({
            _id: user._id
          }, {
            password: hashed_newpass
          }, function (error, usr) {
            if (error) {
              var err = {
                status: 400,
                message: error
              }
              return res.send(JSON.stringify(err))
            } else {
              var BASE_DIR = __dirname.replace('\controllers', '')
              readHTMLFile(BASE_DIR + '/assets/html_template/forgot_password.html', function (err, html) {
                var template = handlebars.compile(html);
                var replacements = {
                  user_name: usr.name,
                  appName: appName,
                  user_email: req.body.email,
                  new_password: newpass,
                  year: new Date().getFullYear(),
                  email_admin: settings.email,
                  phone_admin: settings.phone,
                  address_admin: settings.address
                };
                var htmlToSend = template(replacements);
                var mailOptions = {
                  form: 'Administrator from @myquest.id <'+settings.email+'>',
                  to: req.body.email,
                  subject: appName + ' Account Forgot Password',
                  html: htmlToSend
                }
                transporter.sendMail(mailOptions, function (error, info) {
                  if (error) {
                    var err = {
                      status: 400,
                      message: error
                    }
                    return res.send(JSON.stringify(err))
                  } else {
                    var ret = {
                      status: 200,
                      message: 'Email Sent. Please open your email to see your new password'
                    }
                    return res.send(JSON.stringify(ret))
                  }
                })
              })
            }
          })
        }
      })
    })
  } else {
    var err = {
      status: 400,
      message: 'Email is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//change password
exports.change_password = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //check old password
        var password = crypto.createHash('md5').update(req.body.old_password).digest('hex');
        User.findOne({
          _id: session.user._id,
          password: crypto.createHash('md5').update(req.body.old_password).digest('hex')
        }, function (err, user_check) {
          if (err || user_check == null) {
            var err = {
              status: 400,
              message: 'Old password doensn\'t match'
            }
            return res.send(JSON.stringify(err))
          } else {
            var dataUpdate = {
              password: crypto.createHash('md5').update(req.body.new_password).digest('hex'),
              updatedDate: Date.now(),
              updatedBy: session.user._id
            }
            User.findOneAndUpdate({
              _id: session.user._id
            }, dataUpdate, function (err, user) {
              if (err || user == null) {
                console.log(err)
                var err = {
                  status: 400,
                  message: 'Something went wrong'
                }
                return res.send(JSON.stringify(err))
              } else {
                var err = {
                  status: 200,
                  message: 'Update Password Successful'
                }
                return res.send(JSON.stringify(err))
              }
            })
          }
        })
      } else {
        var data = {
          status: 400,
          message: 'Session invalid'
        }
        return res.send(JSON.stringify(data))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//settings
exports.settings = function (req, res) {
  var locals = {}
  Setting.find({}).exec(function (err, setting) {
    if (err) {
      var err = {
        status: 400,
        message: 'Server error'
      }
      return res.send(JSON.stringify(err))
    } else {
      var data = {
        status: 200,
        data: setting
      }
      return res.send(JSON.stringify(data))
    }
  })
}

//Home
exports.home = function (req, res) {
  var locals = {}
  //Fetch setting
  Setting.findOne({}).exec(function (err, setting) {
    if (err) {
      var err = {
        status: 400,
        message: 'Server error'
      }
      return res.send(JSON.stringify(err))
    } else {
      //Fetch Testimonial
      Testimonial.find({}).exec(function (err, testimonial) {
        if (err) {
          var err = {
            status: 400,
            message: 'Server error'
          }
          return res.send(JSON.stringify(err))
        } else {
          //Fetch feature list
          Feature.find({}).exec(function (err, feature) {
            if (err) {
              var err = {
                status: 400,
                message: 'Server error'
              }
              return res.send(JSON.stringify(err))
            } else {
              var data_in = {
                feature: feature,
                setting: setting,
                testimonial: testimonial
              }
              var data = {
                status: 200,
                data: data_in
              }
              return res.send(JSON.stringify(data))
            }
          })
        }
      })
    }
  })
}

//activate challenge
exports.activate_challenge = function (req, res) {
  var locals = {}
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Check if data is already exist
        UserChallenge.findOne({
          userRef: session.user._id,
          challangeRef: req.body.challangeRef
        }).exec(function (err, challenges) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else if (challenges != null) {
            var err = {
              status: 400,
              message: 'Challenge is already active'
            }
            return res.send(JSON.stringify(err))
          } else {
            var dataCreate = {
              userRef: session.user._id,
              challangeRef: req.body.challangeRef,
              createdDate: Date.now(),
              createdBy: session.user._id
            }
            UserChallenge.create(dataCreate, function (err, user_challenge) {
              if (err || user_challenge == null) {
                var err = {
                  status: 400,
                  message: 'Activate Challenge failed'
                }
                return res.send(JSON.stringify(err))
              } else {
                var data = {
                  status: 200,
                  data: user_challenge,
                  message: 'Activate Challenge success'
                }
                return res.send(JSON.stringify(data))
              }
            })
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//Generate Point
exports.generate_point = function (req, res) {
  HeadOffice.findById(req.headers['headofficeref'], function (err, headoffice) {
    if (err || headoffice == null) {
      var err = {
        status: 400,
        message: 'Invalid Head Office ID'
      }
      return res.send(JSON.stringify(err))
    } else {
      var dataCreate = {
        userRef: null,
        headOfficeRef: headoffice._id,
        pointValue: req.body.pointValue,
        status: 'unactive',
        createdDate: Date.now(),
        createdBy: null
      }
      UserPoint.create(dataCreate, function (err, user_point) {
        if (err || user_point == null) {
          console.log(err)
          var err = {
            status: 400,
            message: 'Generate point failed'
          }
          return res.send(JSON.stringify(err))
        } else {
          var pointURL = webURL + "activate_point/" + user_point._id
          QRCode.toDataURL(pointURL, function (err, url) {
            var err = {
              status: 200,
              message: 'Generate point success',
              qrcode: url
            }
            return res.send(JSON.stringify(err))
          })
        }
      })
    }
  })
}

//Activate Point
exports.activate_point = function (req, res) {
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        console.log("STEP 1")
        var dataUpdate = {
          userRef: session.user._id,
          status: 'active',
          updatedDate: Date.now(),
          updatedBy: session.user._id
        }
        UserPoint.findOneAndUpdate({
          _id: req.params.id,
          status: 'unactive'
        }, dataUpdate, function (err, user_point) {
          if (err || user_point == null) {
            var err = {
              status: 400,
              message: 'Invalid QRCode'
            }
            return res.send(JSON.stringify(err))
          } else {
            console.log("STEP 2")
            //Update Order
            var dataUpdate = {
              userRef: session.user._id,
              updatedDate: Date.now(),
              updatedBy: session.user._id
            }
            Order.findOneAndUpdate({
              userPointRef: req.params.id
            }, dataUpdate, function (err, order) {
              console.log("STEP 3")
              var dataUpdate = {
                $inc: {
                  totalPoint: user_point.pointValue
                },
                updatedDate: Date.now(),
                updatedBy: session.user._id
              }
              User.findByIdAndUpdate(session.user._id, dataUpdate, function (err, user) {
                if (err || user == null) {
                  var err = {
                    status: 400,
                    message: 'Activate Point failed'
                  }
                  return res.send(JSON.stringify(err))
                } else {
                  console.log("STEP 4")
                  //Check Challenge
                  UserChallenge.find({
                    userRef: session.user._id,
                    status: "uncompleted"
                  }).populate('challangeRef').exec(function (err, user_challenge) {
                    if (err) {
                      var err = {
                        status: 400,
                        message: 'Activate Point failed'
                      }
                      return res.send(JSON.stringify(err))
                    } else if (user_challenge.length > 0) {
                      // if (user_challenge.length > 0) {
                      console.log("STEP 5")
                      Order.find({
                        userRef: session.user._id,
                        headOfficeRef: session.user.headOfficeRef
                      }).populate('products.product').exec(function (err, order) {
                        if (err) {
                          var err = {
                            status: 400,
                            message: 'Activate Point failed'
                          }
                          return res.send(JSON.stringify(err))
                        } else {
                          if (order.length > 0) {
                            console.log("STEP 6")
                            var promises = []
                            user_challenge.map((o) => {
                              promises.push(new Promise((resolve, reject) => {
                                var promises2 = []
                                o.challangeRef.requirements.map((p) => {
                                  promises2.push(new Promise((resolve, reject) => {
                                    if (p.type == "product") {
                                      var found = order.find(q => q.products.find(r => r._id.toString() == p.productRef.toString()))
                                      if (found) resolve(found)
                                    } else if (p.type == "product_group") {
                                      var found = order.find(q => q.products.find(r => r.productgroup.toString() == p.productGroupRef.toString()))
                                      if (found) resolve(found)
                                    } else if (p.type == "product_category") {
                                      var found = order.find(q => q.products.find(r => r.productgroup.toString() == p.productGroupRef.toString()))
                                      if (found) resolve(found)
                                    } else if (p.type == "minimum_payment") {
                                      var found = order.find(q => q.total >= p.minimumPayment)
                                      if (found) resolve(found)
                                    } else if (p.type == "number_of_visit") {
                                      var found = order.find(q => {
                                        if (q.createdDate >= p.validFrom && p.createdDate <= p.validTo) {
                                          if (order.length >= p.numberOfVisit) return q
                                        }
                                      })
                                      if (found) resolve(found)
                                    } else if (p.type == "visit_day") {
                                      var found = order.find(q => {
                                        if (q.createdDate >= p.validFrom && p.createdDate <= p.validTo) {
                                          var visitDay = new Date(q.createdDate)
                                          var weekday = new Array(7)
                                          weekday[0] = "sunday"
                                          weekday[1] = "monday"
                                          weekday[2] = "tuesday"
                                          weekday[3] = "wednesday"
                                          weekday[4] = "thursday"
                                          weekday[5] = "friday"
                                          weekday[6] = "saturday"
                                          var n = weekday[visitDay.getDay()]
                                          if (n == p.visitDay.toString()) return q
                                        }
                                      })
                                      if (found) resolve(found)
                                    }
                                  }))
                                })
                                Promise.all(promises2).then(function (val) {
                                  if (val.length >= o.challangeRef.requirements.length) {
                                    var dataCreate = {
                                      userRef: session.user._id,
                                      headOfficeRef: session.user.headOfficeRef,
                                      pointValue: o.challangeRef.pointsOffered,
                                      status: 'active',
                                      createdDate: Date.now(),
                                      createdBy: session.user._id,
                                    }
                                    UserPoint.create(dataCreate, function (err, user_point) {
                                      if (err || user_point == null) {
                                        console.log(err)
                                        reject('Failed to create point')
                                      } else {
                                        //update user point
                                        var dataUpdate = {
                                          $inc: {
                                            totalPoint: o.challangeRef.pointsOffered
                                          },
                                          updatedDate: Date.now(),
                                          updatedBy: session.user._id
                                        }
                                        User.findByIdAndUpdate(session.user._id, dataUpdate, function (err, user) {
                                          if (err || user == null) {
                                            console.log(err)
                                            reject('Failed to update user point')
                                          } else {
                                            resolve(user)
                                          }
                                        })
                                      }
                                    })
                                  }
                                }).catch(err => {
                                  console.log(err)
                                  reject("Something went wrong")
                                })
                              }))
                            })
                            Promise.all(promises).then(function (val) {
                              var err = {
                                status: 200,
                                message: 'Activate Point success'
                              }
                              return res.send(JSON.stringify(err))
                            }).catch(err => {
                              var err = {
                                status: 400,
                                message: 'Activate Point failed'
                              }
                              return res.send(JSON.stringify(err))
                            })
                          } else {
                            var err = {
                              status: 200,
                              message: 'Activate Point success'
                            }
                            return res.send(JSON.stringify(err))
                          }
                        }
                      })
                      // }
                    } else {
                      var err = {
                        status: 200,
                        message: 'Activate Point success'
                      }
                      return res.send(JSON.stringify(err))
                    }
                  })
                }
              })
            })
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//point history
exports.point_history = function (req, res) {
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        //Fetch product categories list
        UserPoint.find({
          userRef: session.user._id,
          status: 'active'
        }).exec(function (err, user_point) {
          if (err) {
            var err = {
              status: 400,
              message: 'Server error'
            }
            return res.send(JSON.stringify(err))
          } else {
            var data_in = {
              session: session,
              user_point: user_point
            }
            var data = {
              status: 200,
              data: data_in
            }
            return res.send(JSON.stringify(data))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Token Invalid'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//redeem reward
exports.redeem_reward = function (req, res) {
  var token = req.headers['token']
  if (token) {
    Session.findOne({
      token: token
    }).populate("user").exec(function (error, session) {
      if (!error && session != null) {
        var dataCreate = {
          userRef: ObjectId(session.user._id),
          rewardRef: req.params.id,
          rewardCode: random_password(),
          headOfficeRef: session.user.headOfficeRef,
          status: "pending",
          createdDate: Date.now(),
          createdBy: session.user._id
        }
        UserReward.create(dataCreate, function (error, result) {
          if (error || result == null) {
            console.log(error)
            var err = {
              status: 400,
              message: 'Redeem Reward Failed'
            }
            return res.send(JSON.stringify(err))
          } else {
            var err = {
              status: 200,
              message: 'Redeem Reward Success',
              data: result
            }
            return res.send(JSON.stringify(err))
          }
        })
      } else {
        var err = {
          status: 400,
          message: 'Invalid token'
        }
        return res.send(JSON.stringify(err))
      }
    })
  } else {
    var err = {
      status: 400,
      message: 'Token is required'
    }
    return res.send(JSON.stringify(err))
  }
}

//send message
exports.send_message = function(req, res){
  if(req.body.subject && req.body.email && req.body.name && req.body.message){
    var dataCreate = {
      name:req.body.name,
      email:req.body.email,
      subject:req.body.subject,
      message:req.body.message,
      createdDate:Date.now()
    }
    Message.create(dataCreate, function(err, message){
      if(err || message==null){
        var ret = {
          status:400,
          message:"Sending message failed"
        }
        return res.send(JSON.stringify(ret))
      } else {
        var ret = {
          status:200,
          message:"Message sent!"
        }
        return res.send(JSON.stringify(ret))
      }
    })
  } else {
    var ret = {
      status:400,
      message:"All field is required"
    }
    return res.send(JSON.stringify(ret))
  }
}

//create MidtransTransaction
exports.createMidtransTrasaction = function(req, res){
  var user_data = req.session.user_data
  var token = req.headers['token']
  var today = new Date()
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()
  var gmt = "+0700"
  var dateTime = date+' '+time+' '+gmt;
  Setting.findOne({}, function(err, setting){
    if(err) console.log(err)
  // prepare Snap API parameter ( refer to: https://snap-docs.midtrans.com ) minimum parameter example:
    let parameter = {
      "transaction_details": {
          "order_id": random_password(),
          "gross_amount": (setting?setting.price:20000),
      }, "credit_card":{
          "secure" : true
      },
      "expiry": {
          "start_time": dateTime,
          "unit": "minute",
          "duration": 1440
      }
    };

// create transaction
snap.createTransaction(parameter)
  .then((transaction)=>{
      // transaction token
      let transactionToken = transaction.token;
      console.log('transactionToken:',transactionToken);

      // transaction redirect url
      let transactionRedirectUrl = transaction.redirect_url;
      console.log('transactionRedirectUrl:',transactionRedirectUrl);
      //create Order
      
  if(user_data){
    var tomorrow = new Date(date+" "+time)
    tomorrow.setDate(tomorrow.getDate() + 1);

      var dataCreate = {
        orderId: parameter.transaction_details.order_id,
        userRef:user_data._id,
        questRef:null,
        status:"unpaid",
        token:transactionToken,
        priceTotal:setting.price,
        redirectURL: transactionRedirectUrl,
        expireTime: tomorrow,
        used:false,
        createdDate: Date.now(),
        createdBy: user_data._id
      }
      Order.create(dataCreate, function(err, order){
        if(err || order==null){
          console.log(err)
          var ret = {
            status:400,
            message:"Create MidTrans Transaction Failed"
          }
          return res.send(JSON.stringify(ret))
        } else {
          var ret = {
            status:200,
            message:"Create MidTrans Transaction Successful",
            data:order,
            redirect:transactionRedirectUrl
          }
          return res.send(JSON.stringify(ret))
        }
      })
    }  else if(token){
      Session.findOne({
        token: token
      }).populate("user").exec(function (error, session) {
        if (!error && session != null) {
          var user_data = session.user
          var dataCreate = {
            orderId: parameter.transaction_details.order_id,
            userRef:user_data._id,
            questRef:null,
            status:"unpaid",
            token:transactionToken,
            createdDate: Date.now(),
            createdBy: user_data._id
          }
          Order.create(dataCreate, function(err, order){
            if(err || order==null){
              var ret = {
                status:400,
                message:"Create MidTrans Transaction Failed"
              }
              return res.send(JSON.stringify(ret))
            } else {
              var ret = {
                status:200,
                message:"Create MidTrans Transaction Successful",
                data:order,
                redirect:transactionRedirectUrl
              }
              return res.send(JSON.stringify(ret))
            }
          })
        } else {
          var ret = {
            status:400,
            message:"Invalid Token"
          }
          return res.send(JSON.stringify(ret))
        }
      })
    } else {
      var ret = {
        status:400,
        message:"You Have no accesss for this action"
      }
      return res.send(JSON.stringify(ret))
    }
  })
  .catch((e)=>{
      console.log('Error occured:',e.message);
      var ret = {
        status:400,
        message:"Failed to create Midtrans Transaction"
      }
      return res.send(JSON.stringify(ret))
  });
  
})

}

//Random Password Generator
function random_password() {
  var pass = (Math.random().toString(36).substr(2)).toUpperCase()
  pass = pass.substr(0, 5);
  return pass
}