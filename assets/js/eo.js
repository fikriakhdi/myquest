jQuery(document).ready(function() {
    $(".clickable-row").click(function() {
        thisdata = $(this).attr('data-href');
        window.location.href = thisdata;
    });
    $(".clickable-row td").hover(function() {
        $(this).css('cursor','pointer');
    });
});
$( function() {
  $("#map-attr").hide();
  $(".datepickerrange_duo").daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
  } );
  $('.clockpicker').clockpicker();
  $("#map").change(function(){
    if($("#map").is(':checked')) {
      $("#map-attr").show(); 
    }
    else {
      $("#map-attr").hide();
    }
  });
  $(document).ready(function(){
      $(".clockpicker-button").html('Complete');
    $('.datepickerrange').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true, 
        locale: {format: 'YYYY-MM-DD'}
    });
    var url =  window.location.href;
    var hash = url.substring(url.indexOf('#')); // '#foo'
    if(hash!=url)
    $(hash+"_button").click();
  });

