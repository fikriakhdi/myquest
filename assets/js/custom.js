 //Tab Style
 
$('.copy-btn').on("click", function () {
  value = $(this).data('clipboard-text'); //Upto this I am getting value
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val(value).select();
  document.execCommand("copy");
  $temp.remove();
  alert("Link Copied");
});

//  $(".mode-toggle").next('note-editable').focus(function(e){
// 	e.preventDefault()
//   $(this).prev('#plain').removeClass('hidden')
//   $(this).prev('#plain').prev('#rte').removeClass('hidden')
//   $("#content_edit").summernote('code', data.data.content);
//   })
//   $(".mode-toggle").focusout(function(e){
// 	e.preventDefault()
//   $(this).prev('#plain').addClass('hidden')
//   $(this).prev('#plain').prev('#rte').addClass('hidden')
//   })

$(".previewQuest").click(function (e) {
  e.preventDefault();
  var submit_url = $(this).data('url')
  console.log("MANTAB")
  var questions = $("#questions").val()
  var profiling = $("#profiling").val()
  var title = $("#title").val()
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var datastring        = form.serialize();
  var targetRespondent = $("#targetRespondent").val()
  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function (response) {
      var result = jQuery.parseJSON(JSON.stringify(response));
      if (result.status == "200") {
        window.open(result.redirect);
      } else {
        //Reset Form
        // $(form).trigger("reset")
        swal({
          title: "Information",
          text: result.message,
          icon: "error",
        });

      }
    },
    dataType: "json"
  });
})


$(".paid_quest").click(function (e) {
  e.preventDefault()
  var submit_url = $(this).data("url")
  $.ajax({
    type: "POST",
    url: submit_url,
    data: {},
    success: function (response) {
      var hasil = jQuery.parseJSON(JSON.stringify(response));
      if (hasil.status == "200") {
        // window.location = result.redirect
        snap.pay(hasil.data.token, {
          // Optional
          onSuccess: function (result) {
            var submit_url = "/paymentSuccess/" + hasil.data.orderId
            $.ajax({
              type: "POST",
              url: submit_url,
              data: {},
              success: function (response) {
                var hasil2 = jQuery.parseJSON(response);
                console.log(hasil2)
                if (hasil2.status == 200) {
                  var redirect = "/create_quest/paid/" + hasil.data.orderId
                  swal({
                    title: "Information",
                    text: "Payment Success",
                    icon: "success",
                  }).then(function () {
                    window.location = redirect;
                  })
                } else {
                  swal({
                    title: "Information",
                    text: "Payment Failed",
                    icon: "error",
                  })
                }
              }
            })
          },
          // Optional
          onPending: function (result) {
            var redirect = "/waiting/" + hasil.data.orderId
            swal({
              title: "Information",
              text: "Waiting For Payment",
              icon: "info",
            }).then(function () {
              window.location = redirect;
            })
          },
          // Optional
          onError: function (result) {
            wal({
              title: "Information",
              text: "Failed to make a payment",
              icon: "error",
            })
          }
        });
      } else {
        swal({
          title: "Information",
          text: "Failed to connect the Payment Gateway",
          icon: "error",
        })
      }
    },
    dataType: "json"
  })
})
$("#profiling_type").change(function () {
  var element = $(this).find('option:selected');
  var type = element.data("type");
  var id = element.val()
  if (type == "number" || type == "date") {
    $("#value_type_bucket").show()
  } else $("#value_type_bucket").hide()
  if (id != "") {
    $(".survey_profiling").addClass("hidden")
    $("." + id).removeClass("hidden")
    $(".single").removeClass("hidden")
    $(".range").addClass("hidden")
  }
})
$(".add-profiling").click(function (e) {
  e.preventDefault();
  var element = $("#profiling_type").find('option:selected');
  var question = element.data("question");
  var val_type = $("#value_type").val();
  var value = ($("input[name='" + element.val() + "'].single-input").val() ? $("input[name='" + element.val() + "'].single-input").val() : $("select[name='" + element.val() + "'].single-input").val())
  var maxValue = ($("input[name='" + element.val() + "'].max").val() ? $("input[name='" + element.val() + "'].max").val() : $("select[name='" + element.val() + "'].max").val())
  var minValue = ($("input[name='" + element.val() + "'].min").val() ? $("input[name='" + element.val() + "'].min").val() : $("select[name='" + element.val() + "'].min").val())
  var id = makeid(5);
  var data = $("#data-profiling")

    if (val_type == "single" || $("#value_type").is(":hidden")) {
      if ($("#profiling_type").val() && value) {
      var obj = {
        _id: element.val(),
        id: id,
        value: value
      }
      console.log("SINI!", obj)
      var template = `<span class="badge badge-primary remove-profiling" id="` + id + `">` + question + ` (` + value + `)` + `</span>`;
      data.append(template)
      var obj = {
        _id: element.val(),
        id: id,
        value: value
      }
      var new_val = $("#profiling").val()
      console.log(obj)
      if (new_val != "") {
        var prev_obj = JSON.parse(new_val)
        prev_obj.push(obj)
        $("#profiling").val(JSON.stringify(prev_obj))
      } else
        $("#profiling").val(JSON.stringify(obj))
      } else {
        swal({
          title: "Information",
          text: "Please fill all of the fields",
          icon: "error",
        })
      }
    } 

    else {
      if (maxValue && minValue) {
        var template = `<span class="badge badge-primary remove-profiling" id="` + id + `">` + question + ` (` + minValue + ` - ` + maxValue + `)` + `</span>`;
        data.append(template)
        var obj = {
          _id: element.val(),
          id: id,
          maxValue: maxValue,
          minValue: minValue
        }
        var new_val = $("#profiling").val()
        console.log(obj)
        if (new_val != "") {
          var prev_obj = JSON.parse(new_val)
          prev_obj.push(obj)
          $("#profiling").val(JSON.stringify(prev_obj))
        } else
          $("#profiling").val(JSON.stringify(obj))
      } else {
        swal({
          title: "Information",
          text: "Please fill all of the fields",
          icon: "error",
        })
      }
    }
})
$(document).delegate(".remove-profiling", "click", function (e) {
  e.preventDefault()
  var id = $(this).attr("id")
  var data = JSON.parse($("#profiling").val())
  data = data.filter(o => o.id.toString() != id.toString())
  $("#profiling").val(JSON.stringify(data))
  $(this).remove()
});

$(".add-question").click(function (e) {
  e.preventDefault();
  var element = $("#question")
  var question = element.val()
  var val_type = $("#survyeProfilingType").val()
  var required = $("#required").val()
  var number = $("#number").val()
  var custom = $("#custom").val()
  if (question && val_type && required && number) {
    var id = makeid(5);
    var data_val = $("#data").val()
    if ($("#data").val() == "" || $("#data_multiply").is(":hidden"))
      var template = `<li class="list-group-item show-question" id="` + id + `">` + number + `:` + question + ` (` + val_type.toUpperCase() + `) </li>`
    else
      var template = `<li class="list-group-item show-question" id="` + id + `">` + number + `: ` + question + ` (` + val_type.toUpperCase() + `)` + `(` + data_val + `)` + `</li>`
    var data = $("#data-questions")
    data.append(template)
    if ($("#data").val() == "" || $("#data_multiply").is(":hidden")) {
      var obj = {
        id: id,
        question: element.val(),
        required: required,
        type: val_type,
        number: number
      }
    } else {
      var obj = {
        id: id,
        question: element.val(),
        required: required,
        type: val_type,
        data: data_val,
        number: number
      }
    }
    if (!$("#custom-group").is(":hidden")) {
      obj.custom = custom
    }
    // console.log("SINI!", obj)
    var new_val = $("#questions").val()
    // console.log(obj)
    if (new_val != "") {
      var prev_obj = JSON.parse(new_val)
      prev_obj.push(obj)
      $("#questions").val(JSON.stringify(prev_obj))
    } else
      $("#questions").val(JSON.stringify(obj))
  } else {
    swal({
      title: "Information",
      text: "Please fill all the fields",
      icon: "error",
    });
  }
})
$(document).delegate(".show-question", "click", function (e) {
  e.preventDefault()
  $('#editQuestion').modal('toggle');
  $("#data_multiply_edit").hide()
  var id = $(this).attr("id")
  var data = JSON.parse($("#questions").val())
  data = data.find(o => o.id.toString() == id.toString())
  $("#question_edit").summernote('code', data.question);
  $("#survyeProfilingType_edit").val(data.type).change()
  $(".delete-question").data("id", id)
  $(".save-question").data("id", id)
  if (data.custom) {
    $("#custom-group-edit").show()
    $("#custom_edit").val(data.custom).change()
  } else {
    $("#custom-group-edit").hide()
  }
  if (data.data) {
    $("#data_multiply_edit").show()
    $('#data_edit').selectize()[0].selectize.destroy()
    console.log("ADA DATA", data.data)
    $("#data_edit").val(data.data)
    $('#data_edit').selectize({
      delimiter: ',',
      persist: false,
      create: function (input) {
        return {
          value: input,
          text: input
        }
      }
    });
  }

  $("#number_edit").val(data.number)
  $("#required_edit").val(data.required).change()
  // $("#questions").val(JSON.stringify(data))
  // $(this).remove()
});
$(document).delegate(".delete-question", "click", function (e) {
  e.preventDefault()
  $(this).parents('.question_content').fadeOut("normal", function () {
    $(this).remove();
  });
})
$(".delete_btn").click(function (e) {
  e.preventDefault();
  var url = $(this).data("url");
  $("#delete_footer").attr("href", url);
});



$(document).delegate(".question-logic", "change", function (e) {
  e.preventDefault()
  var type =$(this).find(':selected').data('type')
  var datain =($(this).find(':selected').data('datain')?$(this).find(':selected').data('datain').toString():"")
  if(type=="text"){
   $(this).parents(".logic_content").find('.question-answer-select').show()
   $(this).parents(".logic_content").find('.question-multiple').hide()
   $(this).parents(".logic_content").find('.question-equal-select').show()
   $(this).parents(".logic_content").find('.question-respond-select').hide()
   $(this).parents(".logic_content").find('.equal-to-group').show()
  } else if(type=="select" || type=="multiple" || type=="radio" || type=="checkbox" ){
   $(this).parents(".logic_content").find('.question-answer-select').show()
   $(this).parents(".logic_content").find('.question-multiple').show()
   $(this).parents(".logic_content").find('.question-equal-select').hide()
   $(this).parents(".logic_content").find('.question-respond-select').show()
   $(this).parents(".logic_content").find('.equal-to-group').hide()
   var data = datain.split(',');
   $(this).parents(".logic_content").find('.question-answer').empty()
   $(this).parents(".logic_content").find('.question-answer').append(
     $('<option/>')  // Create new <option> element
     .val("")            // Set value as "Hello"
     .text("Select Answer"))
   data.map(o=>{
   $(this).parents(".logic_content").find('.question-answer').append(
   $('<option/>')  // Create new <option> element
   .val(o)            // Set value as "Hello"
   .text(o))
   })
  }

})
$(document).delegate(".display-logic-type", "change", function (e) {
  e.preventDefault()
  console.log("haola")
  if ($(this).val() == "device") {
    $(this).parents('.logic_content').find(".is-not-select").show()
    $(this).parents('.logic_content').find(".question-select").hide()
    $(this).parents('.logic_content').find(".is-not-select").show()
    $(this).parents('.logic_content').find(".question-answer-select").hide()
    $(this).parents('.logic_content').find(".question-respond-select").hide()
    $(this).parents('.logic_content').find(".device-list-select").show()
    $(this).parents('.logic_content').find(".equal-to-group").hide()
    
  } else {
   $(this).parents('.logic_content').find(".is-not-select").hide()
   $(this).parents('.logic_content').find(".question-select").show()
   $(this).parents('.logic_content').find(".question-answer-select").hide()
   $(this).parents('.logic_content').find(".question-respond-select").hide()
   $(this).parents('.logic_content').find(".device-list-select").hide()
  }
})

$("#question-select").change(function (e) {
  e.preventDefault()
  $("#question-respond-select").show()
})


$(document).delegate(".carryforward", "click", function (e) {
  e.preventDefault()
  
  var sequence = $(this).data('sequence')
  var type = $("#question_type_"+sequence).val()
  if(type=="select" || type=="multiple" || type=="radio"){
    //show modal
    $("#carryForwardModal").modal('toggle')
  } else {
    
    swal({
      title: "Information",
      text: "You can't add Carry Forward Choices",
      icon: "error",
    })
  }
})
$(document).delegate(".save-display-logic", "click", function (e) {
  var logic_type = []
  var question = []
  var is_not = []
  var or_and = []
  var  is_selected = []
  var device = []
  var data_in = []
  var equal_to = []
  var answer = []
  var equal = [] 
  
  $.each($("select[name='display-logic-type']"), function () {
   logic_type.push($(this).val())
  })
  $.each($("select[name='question-logic']"), function () {
   question.push($(this).children("option:selected").val())
  })
  $.each($("select[name='or-and']"), function () {
   or_and.push($(this).val())
  })
  $.each($("select[name='is-not']"), function () {
   is_not.push($(this).val())
  })
  $.each($("select[name='question-respond']"), function () {
   is_selected.push($(this).children("option:selected").val())
  })
  $.each($("select[name='device-type']"), function () {
   device.push($(this).val())
  })
  $.each($("input[name='equal-to']"), function () {
   equal_to.push($(this).val())
  })
  $.each($("select[name='question-answer']"), function () {
   answer.push($(this).val())
  })
  $.each($("select[name='question-equal']"), function () {
   equal.push($(this).val())
  })
  if(logic_type.length>0){
   logic_type.map((o,i)=>{
     var temp = {
        logic_type:o,
        question :(o.toString()=="question"?question[i]: null),
        is_not: (o.toString()=="device"?is_not[i]: null),
        is_selected : (o.toString()=="question"?is_selected[i]: null),
        device:(o.toString()=="device"?device[i]: null),
        equal : (o.toString()=="question"?equal[i]: null),
        equal_to: (o.toString()=="question"?equal_to[i]: null),
        answer: (o.toString()=="question"?answer[i]: null),
        or_and: (or_and[i]?or_and[i]: null),
        }
        data_in.push(temp)
   })
  }
  var id = $("#display_logic_target").val()
  $("#"+id).val(JSON.stringify(data_in))
  swal({
   title: "Information",
   text: "Display Logic Saved",
   icon: "success",
   })
   $("#displayLogicModal").modal('toggle')
})

$(document).delegate(".add-logic", "click", function (e) {
  e.preventDefault()
  var html = $("#logic-box").html()
  var cpy = html
  cpy = replaceAll(cpy, ' hidden or-and-group', ' or-and-group')
  cpy = replaceAll(cpy, 'hidden" id="remove-logic-box"', '" id="remove-logic-box"')
  cpy = replaceAll(cpy, 'form-group" id="device-list-select"', 'form-group hidden" id="device-list-select"')
temp = document.createElement('div')
  temp.innerHTML = cpy;
  temp.setAttribute("class", "row logic_content")
  // temp.setAttribute("id", "logic_content" + num_of_question)
  $('#logic-bucket').append(temp).show('slow')
  
})
$(document).delegate(".remove-logic", "click", function (e) {
  e.preventDefault()
  // $(this).hide()
  $(this).parents('.logic_content').fadeOut("normal", function () {
   $(this).remove();
 })
})
$(document).delegate(".toggle-disabled", "focus", function (e) {
  e.preventDefault()
  $(this).removeClass('disabled-input');
})
$(document).delegate(".toggle-disabled", "focusout", function (e) {
  e.preventDefault()
  $(this).addClass('disabled-input');
})
$(document).delegate(".save-question", "click", function (e) {
  e.preventDefault()
  //delete previous data
  var id = $(this).data("id")
  var data = JSON.parse($("#questions").val())
  data = data.filter(o => o.id.toString() != id.toString())
  $("#questions").val(JSON.stringify(data))
  //append new data
  var element = $("#question_edit")
  var question = element.val()
  var val_type = $("#survyeProfilingType_edit").val()
  var required = $("#required_edit").val()
  var data_val = $("#data_edit").val()
  var number = $("#number_edit").val()
  var custom = $("#custom_edit").val()
  if ($("#data_multiply_edit").is(":hidden")) {
    var obj = {
      id: id,
      question: element.val(),
      required: required,
      type: val_type,
      number: number
    }
    console.log("DATANYa", obj)
  } else {
    var obj = {
      id: id,
      question: element.val(),
      required: required,
      type: val_type,
      data: data_val,
      number: number
    }
    console.log("DATANYa", obj)
  }
  if (!$("#custom-group-edit").is(":hidden")) {
    obj.custom = custom
  }
  data.push(obj)
  $("#questions").val(JSON.stringify(data))
  if ($("#data_edit").val() == "" || $("#data_multiply_edit").is(":hidden"))
    var template = `<li class="list-group-item show-question" id="` + id + `">` + number + ":" + question + ` (` + val_type.toUpperCase() + `) </li>`
  else
    var template = `<li class="list-group-item show-question" id="` + id + `">` + number + ":" + question + ` (` + val_type.toUpperCase() + `)` + `(` + data_val + `)` + `</li>`
  var data = $("#" + id)
  data.remove()
  var data = $("#data-questions")
  data.append(template)
});

function makeid(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

$("#value_type").change(function () {
  var element = $(this).find('option:selected');
  var val = element.val()
  if (val == "single") {
    $(".single").removeClass("hidden")
    $(".range").addClass("hidden")
  }
  if (val == "range") {
    $(".single").addClass("hidden")
    $(".range").removeClass("hidden")
  }
})





$(document).delegate(".survyeProfilingType", "change", function (e) {
  e.preventDefault()
  var val = $(this).val()
  if (val == "multiple" || val == "select") {
    $(this).parents('.question_content').find("#custom-group").show();
  } else {
    $(this).parents('.question_content').find("#custom-group").hide();
  }
  if (val == "multiple" || val == "radio" || val == "select" || val == "checkbox") {
    $(this).parents('.question_content').find("#data_multiply").show();
    // $("#data").attr("required", true);
  } else {
    $(this).parents('.question_content').find("#data_multiply").hide();
    // $("#data").attr("required", false);
  }
})

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace)
}

$("#delete_selected").click(function () {
  $.each($("input[name='select-question']:checked"), function () {
    $(this).parents('.question_content').fadeOut("normal", function () {
      $(this).remove();
    })
  })
})


$("#select_all").click(function () {
  $.each($("input[name='select-question']"), function () {
    $(this).prop('checked', true);
  })
})

$(".add-new-question").click(function (e) {
  e.preventDefault()
  var html = `
 <div class="row">
<div class="col col-lg-9 border">
 <div class="row ">
   <div class="col col-sm-2">
     <div class="form-check">
       <label class="form-check-label">
       <input type="checkbox" name="select-question" class="form-check-input select-question custom-checkbox"
       value="">
         <input class="form-control toggle-disabled disabled-input"
           name="number" type="text" value="Q1" id="number">
           <input class="hidden" name="display_logic" id="display_logic_1">
           <input class="hidden" name="id_question" id="id_question_1" value="">
           <input class="hidden" name="sequence" id="sequence_1" value="1">
       </label>
     </div>
     <div class="dropdown">
       <button type="button" class="btn btn-default dropdown-toggle dropdown-toggle-padding" data-toggle="dropdown">
         <span class="simple-icon-settings">
       </button>
       <div class="dropdown-menu">
       <a class="dropdown-item add-display-logic" href="javascript:void(0)"  data-toggle="modal"
       data-target="#displayLogicModal" data-id="display_logic_1" data-questionid="id_question_1">Add
         Display Logic</a>
         <a class="dropdown-item carryforward" data-sequence="1" href="javascript:void(0)">Carry Forward Choices</a>
         <a class="dropdown-item copy-question" href="javascript:void(0)">Copy
						Question</a>
         <a class="dropdown-item delete-question" href="javascript:void(0)">Delete Question</a>
       </div>
       </div>
   </div>
   <div class="col-sm-10">
     <textarea name="question"  data-sequence="1" id="question" type="text"
       class="form-control summernote toggle-disabled disabled-input"
       autocomplete="off">Click to write the question text</textarea>
   </div>
 </div>
</div>
<div class="col col-lg-3 border">
 <div class="">
   <div class="form-group">
     <label>Type</label>
     <select name="type" id="question_type_1" class="form-control capitalize survyeProfilingType">
       <option value="">Select Option</option>
       <option class="capitalize" value="text" selected>text</option>
       <option class="capitalize" value="number">number</option>
       <option class="capitalize" value="select">select</option>
       <option class="capitalize" value="date">date</option>
       <option class="capitalize" value="multiple">multiple</option>
       <option class="capitalize" value="radio">radio</option>
       <option class="capitalize" value="textarea">textarea</option>
       <option class="capitalize" value="file">file</option>
       <option class="capitalize" value="email">email</option>
       <option class="capitalize" value="checkbox">checkbox</option>
     </select>
   </div>
   <div class="form-group hidden" id="data_multiply">
     <label>Data</label>
     <input name="data" type="text" id="qestion_data_1"
       class="form-control selectize-custom" autocomplete="off">
   </div>
   <input name="pageBreak" id="pageBreak" class="hidden" value="false">
   <div class="form-group hidden" id="custom-group">
     <label>Custom Input</label>
     <select name="custom" id="custom" class="form-control capitalize"
       required>
       <option class="capitalize" value="true">True</option>
       <option class="capitalize" value="false" selected>False</option>
     </select>
   </div>
   <div class="form-group">
     <label>Required</label>
     <select name="required" id="required" class="form-control capitalize">
       <option value="">Select Option</option>
       <option class="capitalize" value="true" selected>True</option>
       <option class="capitalize" value="false">False</option>
     </select>
   </div>
 </div>
</div>
<div class="col col-lg-12 centered-content hidden" id="page-break-caption"><label class="page-break-label">PAGE BREAK</label><hr class="hr-dashed"></div>
</div>`
  var num_of_question = parseInt($("#num_of_quest").val()) + 1
  temp = document.createElement('div');
  html = replaceAll(html, 'Q1', "Q" + num_of_question)
  html = replaceAll(html, 'display_logic_1', 'display_logic_'+num_of_question)
  html = replaceAll(html, 'id="id_question_1" value=""', 'id="id_question_'+num_of_question+'" value="'+random_password()+'"')
  html = replaceAll(html, 'id_question_1', 'id_question_'+num_of_question)
  html = replaceAll(html, 'id="sequence_1" value="1"', 'id="sequence_1" value="'+num_of_question+'"')
  html = replaceAll(html, 'sequence_1', 'sequence_'+num_of_question)
  html = replaceAll(html, 'selectize-custom', "selectize-custom-"+num_of_question)
  html = replaceAll(html, 'data-sequence="1"', 'data-sequence="'+num_of_question+'"')
  html = replaceAll(html, 'qestion_data_1', 'qestion_data_'+num_of_question)
  html = replaceAll(html, 'question_type_1', 'question_type_'+num_of_question)
  
  temp.innerHTML = html;
  temp.setAttribute("class", "col col-lg-12 question_content")
  temp.setAttribute("id", "question_content_" + num_of_question)
  $('#questions_list').append(temp).show('slow')
  $("#num_of_quest").val(num_of_question)
  $("."+"selectize-custom-"+num_of_question).selectize({
    delimiter: ',',
    persist: false,
    create: function (input) {
      return {
        value: input,
        text: input
      }
    }
  });
  $(".summernote").summernote({
    height: 100
  })
  
  
  // $('.delete_room_type').show()
})

$(document).delegate(".copy-question", "click", function (e) {
	e.preventDefault()
	var html = $("#questions_box").html()
	var num_of_question = parseInt($("#num_of_quest").val()) + 1
 	temp = document.createElement('div');
	 html = replaceAll(html, '<div class="selectize-control form-control selectize-custom multi"><div class="selectize-input items not-full has-options has-items"><div class="item" data-value="Satu">Satu</div><input type="text" autocomplete="off" tabindex="" id="qestion_data_1-selectized" style="width: 4px; opacity: 1; position: relative; left: 0px;"></div><div class="selectize-dropdown multi form-control selectize-custom" style="display: none; width: 229px; top: 40px; left: 12px; visibility: visible;"><div class="selectize-dropdown-content"></div></div></div>', "")
	 html = replaceAll(html, 'Q1', "Q" + num_of_question)
	 html = replaceAll(html, 'display_logic_1', 'display_logic_'+num_of_question)
	 html = replaceAll(html, 'carry_forward_option_1', 'carry_forward_option_'+num_of_question)
	 html = replaceAll(html, 'carry_forward_1', 'carry_forward_'+num_of_question)
	 html = replaceAll(html, 'id="id_question_1" value=""', 'id="id_question_'+num_of_question+'" value="'+random_password()+'"')
	 html = replaceAll(html, 'id_question_1', 'id_question_'+num_of_question)
	 html = replaceAll(html, 'id="sequence_1" value="1"', 'id="sequence_1" value="'+num_of_question+'"')
	 html = replaceAll(html, 'sequence_1', 'sequence_'+num_of_question)
	 html = replaceAll(html, 'data-sequence="1"', 'data-sequence="'+num_of_question+'"')
	 html = replaceAll(html, 'qestion_data_1', 'qestion_data_'+num_of_question)
	 html = replaceAll(html, 'question_type_1', 'question_type_'+num_of_question)
 	temp.innerHTML = html;
 	temp.setAttribute("class", "col col-lg-12 question_content")
 	temp.setAttribute("id", "question_content_" + num_of_question)
 	$('#questions_list').append(temp).show('slow')
 	$("#num_of_quest").val(num_of_question)
 })

$(document).delegate(".add-page-break", "click", function (e) {
  e.preventDefault()
  if ($(this).parents('.question_content').find("#pageBreak").val() == "false") {
    $(this).parents('.question_content').find("#pageBreak").val('true')
    // $('#questions_list').append(template).show('slow')
    $(this).parents('.question_content').find("#page-break-caption").show()
    $(this).hide()
    $(this).parents('.question_content').find(".remove-page-break").show()
  }
})

$(document).delegate(".add-display-logic", "click", function (e) {
  e.preventDefault()
  //reset form
  $("#display-logic-form").trigger("reset")
  var default_data = `<input class="hidden" id="display_logic_target">
  <input class="hidden" id="id_question_target">
  <div class="row logic_content" id="logic-box">
    <div class="col-md-2 hidden or-and-group" >
    <div class="form-group">
      <select class="form-control or-and" name="or-and">
          <option value="and" selected>And</option>
        <option value="or">Or</option>
      </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <select class="form-control display-logic-type" name="display-logic-type">
          <option value="question" >Question</option>
          <option value="device" >Device</option>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group hidden is-not-select">
        <select class="form-control is-not" name="is-not">
          <option value="is">Is</option>
          <option value="is_not">Is Not</option>
        </select>
      </div>
      <div class="form-group question-select">
        <select class="form-control question-logic" name="question-logic" id="question-logic">
          <option value="">Select Question</option>
        </select>
      </div>
    </div>
    <div class="col-md-2 question-answer-select">
      <div class="form-group hidden question-multiple">
        <select class="form-control question-answer" name="question-answer">
          <option value="">Select Answer</option>
        </select>
      </div>
      <div class="form-group hidden question-equal-select">
        <select class="form-control question-equal" name="question-equal">
        <option value="">Select Option</option>
          <option value="is_equal_to">Is Equal To</option>
          <option value="is_not_equal_to">Is Not Equal To</option>
          <option value="is_greater_than">Is Greater Than</option>
          <option value="is_greater_than_or_equal_to">Is Greater Than or Equal To</option>
          <option value="is_less_than">Is Less Than</option>
          <option value="is_les_than_or_equal_to">Is Less Than or Equal To</option>
          <option value="is_empty">Is Empty</option>
          <option value="is_not_empty">Is Not Empty</option>
          <option value="is_contains">Is Contains</option>
          <option value="is_not_contains">Is Not Contains</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group hidden equal-to-group">
        <input class="form-control equal-to" name="equal-to">
      </div>
      <div class="form-group hidden device-list-select">
        <select class="form-control device-type" name="device-type">
        <option value="">Select Option</option>
          <option value="desktop">Desktop</option>
          <option value="phone">Phone</option>
          <option value="tablet">Tablet</option>
          <option value="tv">TV</option>
          <option value="car">Car</option>
        </select>
      </div>
      <div class="form-group hidden question-respond-select">
        <select class="form-control question-respond" name="question-respond">
        <option value="">Select Option</option>
          <option value="is_selected">Is Selected</option>
          <option value="is_not_selected">Is Not Selected</option>
          <option value="is_displayed">Is Displayed</option>
          <option value="is_not_displayed">Is Not Displayed</option>
        </select>
      </div>
    </div>
    
    <div class="col-md-1 hidden" id="remove-logic-box">
    <button type="button " class=" btn btn-danger remove-logic"><span class="simple-icon-trash"></span></button>
    </div>
  </div>`
  $("#logic-bucket").html(default_data)
  //check if the display logic is already filled
 var data_id = $(this).data('id')
 $("#display_logic_target").val(data_id)
 var display_logic = $("#"+data_id).val()
 
 var data_questionid = $(this).data('questionid')
 $("#id_question_target").val(data_questionid)
  var questions = []
  $.each($("textarea[name='question']"), function () {
    var sequence = $(this).data('sequence')
    var temp = {
      caption:$(this).val()
      .replace(/<\/p>/gi, "\n")
      .replace(/<br\/?>/gi, "\n")
      .replace(/<\/?[^>]+(>|$)/g, ""),
      type: $("#question_type_"+sequence).val(),
      data : ($("#qestion_data_"+sequence).val()?$("#qestion_data_"+sequence).val():""),
      id:$("#id_question_"+sequence).val()
    }
   //  console.log(temp)
   questions.push(temp)
 })
  var question_list = $("#question-logic")
  var questions_string = ""
  $("#question-logic").empty();
  question_list.
   append($('<option/>')  // Create new <option> element
   .val("")            // Set value as "Hello"
   .text("Select a question"))
  questions.map((o, i)=>{
   question_list.
   append($('<option data-type="'+o.type+'" data-datain="'+o.data+'"/>')  // Create new <option> element
   .val(o.id)            // Set value as "Hello"
   .text(o.caption));
   
   questions_string+='<option data-type="'+o.type+'" data-datain="'+o.data+'" value="'+o.id+'">'+o.caption+'</option>'
  })
  

  //set data
  if(display_logic){
   var parsed = JSON.parse(display_logic.toString())
   if(parsed){
     parsed.map((o, i)=>{
       if(i==0){
         $(".display-logic-type").val(o.logic_type).change()
         if(o.logic_type=="question"){
           $(".is-not-select").hide()
           $(".question-select").show()
           console.log(o.question)
           $(".question-logic").val(o.question).change()

             if(o.equal_to){
               $(".question-multiple").hide()
               $(".question-equal-select").show()
               $(".question-equal").val(o.equal)
               $(".equal-to-group").show()
               $(".equal-to").val(o.equal_to)
             } else { 
               $(".question-multiple").show()
               $(".equal-to-group").hide()
               $(".question-answer").val(o.answer).change()
               $(".question-respond-select").show()
               $(".question-respond").val(o.is_selected).change()
             }
         } else {
           $(".is-not-select").show()
           $(".question-select").hide()
           $(".device-type").val(o.device).change()
           $(".is-not").val(o.is_not).change()
         }
       } else {
         
         var template = `
         <div class="row logic_content" >
         <div class="col-md-2 or-and-group" >
                       <div class="form-group">
                           <select class="form-control or-and" name="or-and">
                                   <option value="and"` +(o.or_and=="and"?"selected":"")+`>And</option>
                               <option value="or" `+(o.or_and=="or"?"selected":"")+`>Or</option>
                           </select>
                           </div>
                       </div>
                       <div class="col-md-2">
                           <div class="form-group">
                               <select class="form-control display-logic-type" name="display-logic-type">
                                   <option value="question" `+(o.logic_type=="question"?"selected":"")+`>Question</option>
                                   <option value="device" `+(o.logic_type=="device"?"selected":"")+`>Device</option>
                               </select>
                           </div>
                       </div>
                       <div class="col-md-2">
                           <div class="form-group `+(o.logic_type=="device"?"":"hidden")+` is-not-select">
                               <select class="form-control" name="is-not">
                                   <option value="is" `+(o.is_not=="is"?"selected":"")+`>Is</option>
                                   <option value="is_not" `+(o.is_not=="is_not"?"selected":"")+`>Is Not</option>
                               </select>
                           </div>
                           <div class="form-group `+(o.question?"":"hidden")+` question-select">
                               <select class="form-control question-logic" name="question-logic" id="question-logic-`+i+`">
                 <option value="">Select Question</option>
                 `+questions_string+`
               </select>
             </div>
             <script>
             $(document).ready(function(e){
               $("#question-logic-`+i+`").val("`+(o.question?o.question:"")+`")
             })
             </script>
                       </div>
                       <div class="col-md-2 `+(o.logic_type=="question"?"":"hidden")+` question-answer-select">
                           <div class="form-group `+(o.answer?"":"hidden")+` question-multiple">
                               <select class="form-control question-answer" name="question-answer">
                 <option value="">Select Answer</option>
                 
                               </select>
                           </div>
                           <div class="form-group `+(o.equal?"":"hidden")+` question-equal-select">
               <select class="form-control" name="question-equal" id="question-equal-`+i+`">
               <option value="">Select Option</option>
                                   <option value="is_equal_to">Is Equal To</option>
                                   <option value="is_not_equal_to">Is Not Equal To</option>
                                   <option value="is_greater_than">Is Greater Than</option>
                                   <option value="is_greater_than_or_equal_to">Is Greater Than or Equal To</option>
                                   <option value="is_less_than">Is Less Than</option>
                                   <option value="is_les_than_or_equal_to">Is Less Than or Equal To</option>
                                   <option value="is_empty">Is Empty</option>
                                   <option value="is_not_empty">Is Not Empty</option>
                                   <option value="is_contains">Is Contains</option>
                                   <option value="is_not_contains">Is Not Contains</option>
                               </select>
             </div>
             <script>
             $(document).ready(function(){
               $("#question-equal-`+i+`").val("`+(o.equal?o.equal:"")+`")
             })
             </script>
                       </div>
                       <div class="col-md-3">
                           <div class="form-group `+(o.equal_to?"":"hidden")+` equal-to-group">
                               <input class="form-control" name="equal-to" value="`+(o.equal_to?o.equal_to:"")+`">
                           </div>
                           <div class="form-group `+(o.logic_type=="device"?"":"hidden")+` device-list-select">
                               <select class="form-control" name="device-type" id="device-type-`+i+`">
               <option value="">Select Option</option>
                                   <option value="desktop">Desktop</option>
                                   <option value="phone">Phone</option>
                                   <option value="tablet">Tablet</option>
                                   <option value="tv">TV</option>
                                   <option value="car">Car</option>
               </select>
               <script>
               $(document).ready(function(){
                 $("#device-type-`+i+`").val("`+(o.device?o.device:"")+`")
               })
               </script>
                           </div>
                           <div class="form-group `+(o.is_selected?"":"hidden")+` question-respond-select">
                               <select class="form-control" name="question-respond" name="question-respond-`+i+`">
               <option value="">Select Option</option>
                                   <option value="is_selected">Is Selected</option>
                                   <option value="is_not_selected">Is Not Selected</option>
                                   <option value="is_displayed">Is Displayed</option>
                                   <option value="is_not_displayed">Is Not Displayed</option>
                               </select>
               <script>
               $(document).ready(function(){
                 $("#question-respond-`+i+`").val("`+(o.is_selected?o.is_selected:"")+`")
               })
               </script>
                           </div>
                       </div>
                       
                       <div class="col-md-1" id="remove-logic-box">
                       <button type="button" class=" btn btn-danger remove-logic"><span class="simple-icon-trash"></span></button>
           </div></div>`
           $("#logic-bucket").append(template)
       }
     })
   }
 }
})





$(document).delegate(".remove-all-logic", "click", function (e) {
  e.preventDefault()
  //reset form
  var default_data = `
  <div class="row logic_content" id="logic-box">
    <div class="col-md-2 hidden or-and-group" >
    <div class="form-group">
      <select class="form-control or-and" name="or-and">
          <option value="and" selected>And</option>
        <option value="or">Or</option>
      </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <select class="form-control display-logic-type" name="display-logic-type">
          <option value="question" >Question</option>
          <option value="device" >Device</option>
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group hidden is-not-select">
        <select class="form-control is-not" name="is-not">
          <option value="is">Is</option>
          <option value="is_not">Is Not</option>
        </select>
      </div>
      <div class="form-group question-select">
        <select class="form-control question-logic" name="question-logic" id="question-logic">
          <option value="">Select Question</option>
        </select>
      </div>
    </div>
    <div class="col-md-2 question-answer-select">
      <div class="form-group hidden question-multiple">
        <select class="form-control question-answer" name="question-answer">
          <option value="">Select Answer</option>
        </select>
      </div>
      <div class="form-group hidden question-equal-select">
        <select class="form-control question-equal" name="question-equal">
        <option value="">Select Option</option>
          <option value="is_equal_to">Is Equal To</option>
          <option value="is_not_equal_to">Is Not Equal To</option>
          <option value="is_greater_than">Is Greater Than</option>
          <option value="is_greater_than_or_equal_to">Is Greater Than or Equal To</option>
          <option value="is_less_than">Is Less Than</option>
          <option value="is_les_than_or_equal_to">Is Less Than or Equal To</option>
          <option value="is_empty">Is Empty</option>
          <option value="is_not_empty">Is Not Empty</option>
          <option value="is_contains">Is Contains</option>
          <option value="is_not_contains">Is Not Contains</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group hidden equal-to-group">
        <input class="form-control equal-to" name="equal-to">
      </div>
      <div class="form-group hidden device-list-select">
        <select class="form-control device-type" name="device-type">
        <option value="">Select Option</option>
          <option value="desktop">Desktop</option>
          <option value="phone">Phone</option>
          <option value="tablet">Tablet</option>
          <option value="tv">TV</option>
          <option value="car">Car</option>
        </select>
      </div>
      <div class="form-group hidden question-respond-select">
        <select class="form-control question-respond" name="question-respond">
        <option value="">Select Option</option>
          <option value="is_selected">Is Selected</option>
          <option value="is_not_selected">Is Not Selected</option>
          <option value="is_displayed">Is Displayed</option>
          <option value="is_not_displayed">Is Not Displayed</option>
        </select>
      </div>
    </div>
    
    <div class="col-md-1 hidden" id="remove-logic-box">
    <button type="button " class=" btn btn-danger remove-logic"><span class="simple-icon-trash"></span></button>
    </div>
  </div>`
  $("#logic-bucket").html(default_data)
  //check if the display logic is already filled
 
  var questions = []
  $.each($("textarea[name='question']"), function () {
    var sequence = $(this).data('sequence')
    var temp = {
      caption:$(this).val()
      .replace(/<\/p>/gi, "\n")
      .replace(/<br\/?>/gi, "\n")
      .replace(/<\/?[^>]+(>|$)/g, ""),
      type: $("#question_type_"+sequence).val(),
      data : ($("#qestion_data_"+sequence).val()?$("#qestion_data_"+sequence).val():""),
      id:$("#id_question_"+sequence).val()
    }
   //  console.log(temp)
   questions.push(temp)
 })
  var question_list = $("#question-logic")
  $("#question-logic").empty();
  question_list.
   append($('<option/>')  // Create new <option> element
   .val("")            // Set value as "Hello"
   .text("Select a question"))
  questions.map((o, i)=>{
   question_list.
   append($('<option data-type="'+o.type+'" data-datain="'+o.data+'"/>')  // Create new <option> element
   .val(o.id)            // Set value as "Hello"
   .text(o.caption))
  })
})

$(document).delegate(".remove-page-break", "click", function (e) {
  e.preventDefault()
  if ($(this).parents('.question_content').find("#pageBreak").val() == "true") {
    $(this).parents('.question_content').find("#pageBreak").val('false')
    // $('#questions_list').append(template).show('slow')
    $(this).parents('.question_content').find("#page-break-caption").hide()
    $(this).hide()
    $(this).parents('.question_content').find(".add-page-break").show()
  }
})
$("#survyeProfilingType_edit").change(function () {
  var val = $(this).val()
  if (val == "multiple" || val == "select") {
    $("#custom-group-edit").show();
  } else {
    $("#custom-group-edit").hide();
  }
  if (val == "multiple" || val == "radio" || val == "select") {
    $("#data_multiply_edit").show();
    // $("#data_edit").attr("required", true);
  } else {
    $("#data_multiply_edit").hide();
    // $("#data_edit").attr("required", false);
  }
})


jQuery(document).ready(function ($) {

  $('.summernote').summernote({
    height: 100,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'italic', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'hr']],
      ['view', ['fullscreen', 'codeview']],
      ['help', ['help']]
    ],
  });
  $('.summernote-empty').summernote({
    height: 100,
    toolbar: [],
  });
  $(".datepicker").datepicker();
  // $('.star-picker').combostars();
  // $("#data_multiply").hide();
  $("#data_multiply_edit").hide();
  $(".selectize").selectize();
  $(".selectize-custom").selectize({
    delimiter: ',',
    persist: false,
    create: function (input) {
      return {
        value: input,
        text: input
      }
    }
  });
  $("#value_type_bucket").hide();
  "use strict";


});

function random_password() {
   var length = 5
   var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
   var result = '';
   for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
   return result;
 }

 $(document).delegate(".swal-button", "click", function (e) {
   e.preventDefault()
  $(".swal-overlay").hide()
 })

 function imagepreview(input,image_id)
{
  if (input.files && input.files[0])
  {
    var filerdr = new FileReader();
    filerdr.onload = function(e) {
        $('#'+image_id).attr('src', e.target.result);
    };
    filerdr.readAsDataURL(input.files[0]);
  }
}

$(".change_picture").click(function(){
  var attr=$(this).data('target');
  $(attr).modal('toggle');
});
$(".change_picture").hover(function() {
    $(this).css('cursor','pointer');
});





function previewFile(dom_id) {
  var file    = document.querySelector('#'+dom_id).files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    var filename = $('input[type=file]#'+dom_id).val().replace(/.*(\/|\\)/, '')
    if(dom_id == 'image_edit'){
      $('#image_label_edit').text(filename)
      $('#profile_imgsrc_edit').val(reader_edit.result)
    } else {
      $('#image_label').text(filename)
      $('#profile_imgsrc').val(reader.result)
    }
  }, false)

  if (file) {
    reader.readAsDataURL(file);
  }
}

$(".delete_btn").click(function (e) {
  e.preventDefault();
  var url = $(this).data("url");
  $("#delete_footer").attr("href", url);
});

$("body").on("click", ".swal-button--confirm", function(){
  $(".swal-overlay").hide();
});

$(".edit_faq").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#question_edit").val(data.data.question);
    $("#answer_edit").val(data.data.answer);
  });
});

$(".edit_survey_profiling").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#update_survey_profiling").trigger('reset')
    $("#id").val(data.data._id);
    $("#question_edit").val(data.data.question);
    $("#survyeProfilingType_edit").val(data.data.type);
    if(data.data.type.toString()=="select" || data.data.type.toString()=="multiply" || data.data.type.toString()=="radio"){
      $("#data_multiply_edit").show();
      $('#data_edit').selectize()[0].selectize.destroy();
      // $('#data_edit')[0].selectize.destroy();
      $('#data_edit').val(data.data.data.toString());
      $('#data_edit').selectize({delimiter: ',',
      persist: false,
      create: function(input) {
          return {
              value: input,
              text: input
          }
      }});
    } else {
      $("#data_multiply_edit").hide()
    }
    $("#required_edit").val(data.data.required.toString()).change()
  });
});

$(".edit_news").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#title_edit").val(data.data.title);
    $("#subtitle_edit").val(data.data.subtitle);
    $("#content_edit").summernote('code', data.data.content);
  });
});

$(".edit_reward").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#title_edit").val(data.data.title);
    $("#pointsRequired_edit").val(data.data.pointsRequired);
    $("#storeRef_edit").val(data.data.storeRef);
  });
});

$(".edit_head_office").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
  });
});

$(".edit_feature").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#description_edit").val(data.data.description);
  });
});

$(".edit_testimonial").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#caption_edit").val(data.data.caption);
    $("#stars_edit").val(data.data.stars).change();
    $("#confirmed_edit").val(data.data.confirmed).change();
  });
});


$(".edit_product_group").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#brand_edit").val(data.data.brand);
  });
});

$(".edit_product_category").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#productGroup_edit").val(data.data.productGroup);
    $("#brand_edit").val(data.data.brand);
  });
});

$(".edit_product").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#productCode_edit").val(data.data.productCode);
    $("#PLUId_edit").val(data.data.PLUId);
    $("#printerId_edit").val(data.data.printerId);
    $("#price_edit").val(data.data.price);
    $("#validStartDate_edit").val(data.data.validStartDate);
    $("#description_edit").val(data.data.description);
    $("#brand_edit").val(data.data.brand).change();
    $("#productgroup_edit").val(data.data.productgroup).change();
    var i;
    $("#productcategory_edit_"+data.data.productgroup+" option:selected").prop("selected", false);
    for (i = 0; i < data.data.productcategory.length; ++i) {
      console.log(i)
      $("#productcategory_edit_"+data.data.productgroup+" option[value='" + data.data.productcategory[i] + "']").prop("selected", true);
    }
    $('#productcategory_edit_'+data.data.productgroup).select2();    
  });
});

$(".edit_user").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#email_edit").val(data.data.email);
    $("#phone_edit").val(data.data.phone);
    $("#birthplace_edit").val(data.data.birthPlace);
    $("#birthDate_edit").val(data.data.birthDate);
    $("#gender_edit").val(data.data.gender);
    $("#religion_edit").val(data.data.religion);    
    $("#address_edit").val(data.data.address);
    $("#role_edit").val(data.data.role).change();
  });
});

$("select[name='role']").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $(".store-user-req").hide()
  if(val == 'store') $(".store-user-req").show()
});

$("select#headOffice").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select.ho-ref option").hide()
  $("select.ho-ref option."+val).show()
});

$("select#headOffice_edit").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select.ho-ref-edit option").hide()
  $("select.ho-ref-edit option."+val).show()
});

$("select#brand").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select.brand-ref option").hide()
  $("select.brand-ref option."+val).show()
});

$("select#brand_edit").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select#productgroup_edit option").hide()
  $("select#productgroup_edit option."+val).show()
  $("select.brand-ref-edit option").hide()
  $("select.brand-ref-edit option."+val).show()
});

$("select#productgroup").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("#productgroup_ref_temporary").show()
  if ($(".productgroup-ref#"+val).length) $("#productgroup_ref_temporary").hide()
  $(".productgroup-ref").hide()
  $(".productgroup-ref#"+val).show()
})

$(document).ready(function(){
  // $("#data_multiply").hide();
   $("#value_type_bucket").hide();
   
  // $("#data_multiply").hide();
  // $("#data_multiply_edit").hide();
  $('.summernote').summernote({
    dataType: "json"
  });
})


$("#survyeProfilingType").change(function(){
  var val = $(this).val()
  if(val=="multiple" || val=="radio" || val=="select"){
    $("#data_multiply").show();
    // $("#data").attr("required", true);
  } else {
    $("#data_multiply").hide();
    // $("#data").attr("required", false);
  }
})

$(".survyeProfilingType").change(function(e){
  e.preventDefault()
  var id_data = $(this).data("i")
  var data = $(id_data)
  var val = $(this).val()
  if(val=="multiple" || val=="radio" || val=="select"){
    data.show();
    // $("#data").attr("required", true);
  } else {
    data.hide();
    // $("#data").attr("required", false);
  }
})

$("#survyeProfilingType_edit").change(function(){
  var val = $(this).val()
  if(val=="multiple" || val=="radio" || val=="select"){
    $("#data_multiply_edit").show();
    // $("#data_edit").attr("required", true);
  } else {
    $("#data_multiply_edit").hide();
    // $("#data_edit").attr("required", false);
  }
})

  setTimeout(function() { 
    $(".button-add-product").html(`<button type="button" class="btn btn-primary btn-sm mb-1" data-toggle="modal" data-target="#create_modal">ADD MORE PRODUCT</button>`);
  }, 500);  
