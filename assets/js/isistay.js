//room detail


//Edit Facility
$(".edit_facility").click(function(){
    var url = $(this).data('url');
    $.getJSON( url, function( data ) {
      var result =jQuery.parseJSON( JSON.stringify(data ));
      if(result.status==200){
        var data = jQuery.parseJSON(JSON.stringify(result.data));
        $("#id").val(data._id);
        $("#name_edit").val(data.name);
      } else { console.log(result)}
    });
  });

  //Edit User
$(".edit_user").click(function(){
  var url = $(this).data('url');
  $.getJSON( url, function( data ) {
    var result =jQuery.parseJSON( JSON.stringify(data ));
    if(result.status==200){
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#name_edit").val(data.name);
      $("#email_edit").val(data.email);
      $("#phone_edit").val(data.phone);
      $("#id_type").val(data.idType);
      $("#birthplace_edit").val(data.birthPlace);
      $("#id_number").val(data.idNumber)
      $("#role_edit").val(data.role).change();
    } else { console.log(result)}
  });
});

  //Edit Bank List
  $(".edit_bank_list").click(function(){
    var url = $(this).data('url');
    $.getJSON( url, function( data ) {
      var result =jQuery.parseJSON( JSON.stringify(data ));
      if(result.status==200){
        var data = jQuery.parseJSON(JSON.stringify(result.data));
        $("#id").val(data._id);
        $("#bankName_edit").val(data.bankName);
        $("#bankCode_edit").val(data.bankCode);
        $("#ownerName_edit").val(data.ownerName);
        $("#accountNumber_edit").val(data.accountNumber);
        $("#branch_edit").val(data.branch);
      } else { console.log(result)}
    });
  });

    //Edit Payment Method
    $(".edit_payment_method").click(function(){
      var url = $(this).data('url');
      $.getJSON( url, function( data ) {
        var result =jQuery.parseJSON( JSON.stringify(data ));
        if(result.status==200){
          var data = jQuery.parseJSON(JSON.stringify(result.data));
          $("#id").val(data._id);
          $("#name_edit").val(data.name);
        } else { console.log(result)}
      });
    });

  //Edit Attraction
$(".edit_attraction").click(function(){
  var url = $(this).data('url');
  $.getJSON( url, function( data ) {
    var result =jQuery.parseJSON( JSON.stringify(data ));
    if(result.status==200){
      var data = jQuery.parseJSON(JSON.stringify(result.data));
      $("#id").val(data._id);
      $("#name_edit").val(data.name);
      $("#desc_edit").val(data.desc);
      $("#latitude_edit").val(data.lat);
      $("#longitude_edit").val(data.lon);
      var name2 = $("#name_edit").val();
var lat2 = $("#latitude_edit").val();
var lon2 = $("#longitude_edit").val();
  var latLng2 = new google.maps.LatLng(lat2, lon2);
  var title_place = name2;
  var map2 = new google.maps.Map(document.getElementById('map_edit'), {
    zoom: 15,
    center: latLng2,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  function updateMarkerPosition(latLng2) {
    document.getElementById('latitude_edit').value = [latLng2.lat()]
    document.getElementById('longitude_edit').value = [latLng2.lng()]
  }


  var input2 = document.getElementById('nama_lokasi_edit');
  var searchBox2 = new google.maps.places.SearchBox(input2);
  // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map2.addListener('bounds_changed', function() {
    searchBox2.setBounds(map2.getBounds());
  });

  var markers2;
        markers2 = new google.maps.Marker({
        map: map2,
//         icon: icon,
        title: name2,
        position: latLng2,
        draggable: true
      });
  markers2.addListener('drag', function() {
        // ketika marker di drag, otomatis nilai latitude dan longitude
        //menyesuaikan dengan posisi marker
//     alert(this.title);
        updateMarkerPosition(this.getPosition());
      });
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox2.addListener('places_changed', function() {
    if(markers2!=null){
    markers2.setMap(null);
    }
    var places2 = searchBox2.getPlaces();

    if (places2.length == 0) {
      return;
    }

//       markers.setMap(null);
    markers2="";
    // For each place, get the icon, name and location.
    var bounds2 = new google.maps.LatLngBounds();
    places2.forEach(function(place2) {
      if (!place2.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon2 = {
        url: place2.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      
//       markers.setMap(null);
      markers2=new google.maps.Marker({
        map: map,
        // icon: icon,
        title: place2.name,
        position: place2.geometry.location,
        draggable: true
      });
      latLng2 = place2.geometry.location;
      updateMarkerPosition(latLng2);
      markers2.addListener('drag', function() {
        // ketika marker di drag, otomatis nilai latitude dan longitude
        //menyesuaikan dengan posisi marker
        updateMarkerPosition(markers.getPosition());
      });
      if (place2.geometry.viewport) {
        // Only geocodes have viewport.
        bounds2.union(place2.geometry.viewport);
      } else {
        bounds2.extend(place2.geometry.location);
      }
    });
    map2.fitBounds(bounds2);
  });
    } else { console.log(result)}
  });
});

