$(".data_room_detail").click(function (e) {
  e.preventDefault()
  $('#form_change_room').trigger("reset")
  $('#form_add_notes').trigger("reset")
  $("#form_change_room").hide()
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(url)
    var result = jQuery.parseJSON(JSON.stringify(data))
    console.log(result)
    if (result.status == 200) {
      $('#room_number_edit').html("")
      var room = jQuery.parseJSON(JSON.stringify(result.room))
      var checkin_raw = new Date(result.startDate)
      var month = checkin_raw.getMonth() + 1
      var date = checkin_raw.getDate()
      var year = checkin_raw.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var checkin_date = year + "-" + month + "-" + date
      var checkout_raw = new Date(result.endDate)
      var month = checkout_raw.getMonth() + 1
      var date = checkout_raw.getDate()
      var year = checkout_raw.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var checkout_date = year + "-" + month + "-" + date
      $("#id").val(room._id)
      $("#guest_name").text(room.bookingRef.guest.name)
      $("#room_number").text(room.room_number)
      $("#room_type").text(room.room_type.name)
      $("#allocatedby").text(room.agentLinked!=null?room.agentLinked.name:"Kambaniru")
      $("#check_in").html(checkin_date);
      $("#check_out").html(checkout_date);
      $("#status").text(room.status);
      $("#guest_information").show();

      $("#room_id").val(room._id);
      $("#booking_id").val(result.booking_id);

      var href = $("#early_checkout_btn").data('url');
      $("#early_checkout_btn").attr('href', href+room._id)
      
      const samegrade = result.roomtype_samegrade
      const upgrade = result.roomtype_upgrade
      const downgrade = result.roomtype_downgrade
      console.log(samegrade, upgrade, downgrade)
      var list = ""
      $.each(samegrade, function (index, value) {
        $('#select_room_type').append(($('<option></option>').val(value._id).html(value.name)).attr("class", "samegrade"))
      })
      $.each(upgrade, function (index, value) {
        $('#select_room_type').append(($('<option></option>').val(value._id).html(value.name)).attr("class", "upgrade"))
      })
      $.each(downgrade, function (index, value) {
        $('#select_room_type').append(($('<option></option>').val(value._id).html(value.name)).attr("class", "downgrade"))
      })
    } else {
      console.log(result)
    }
  })
})

$(".data_room_detail_empty").click(function (e) {
  e.preventDefault()
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(url)
    var result = jQuery.parseJSON(JSON.stringify(data))
    console.log(result)
    if (result.status == 200) {
      $('#room_number_edit').html("")
      var room = jQuery.parseJSON(JSON.stringify(result.data))
      $("#id").val(room._id)
      $("#room_number").text(room.room_number)
      $("#room_type").text(room.room_type.name)
      $("#status").text(room.status)
      $("#allocatedby").text(room.agentLinked!=null?room.agentLinked.name:"Kambaniru")
      $("#guest_information").hide()
      $("#form_change_room").hide()

      $("#room_id").val(room._id)
      $("#booking_id").val("")
    }
  })
})

$('#btn_edit_room_number').click(function () {
  $('#form_change_room').show()
  $('#form_add_notes').hide()
})

$('#add_notes_btn').click(function () {
  $('#form_add_notes').show()
  $('#form_change_room').hide()
})

//Edit company
$(".edit_company").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
      $("#alamat_edit").val(data.alamat)
      $("#email_edit").val(data.email)
      $("#telpon_edit").val(data.telpon)
    } else {
      console.log(result)
    }
  })
})

//Edit Card
$(".edit_card").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      $("#id").val(data._id)
      $("#card_id_edit").val(data.cardId)
      $("#card_code_edit").val(data.cardCode)
    } else {
      console.log(result)
    }
  })
})

//Edit Event Promo
$(".edit_event_promo").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)

      var period_start = new Date(data.periodStart)
      var month = period_start.getMonth() + 1
      var date = period_start.getDate()
      var year = period_start.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var period_start_date = month + "/" + date + "/" + year

      var period_end = new Date(data.periodEnd)
      var month = period_end.getMonth() + 1
      var date = period_end.getDate()
      var year = period_end.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var period_end_date = month + "/" + date + "/" + year

      var period_range = period_start_date + " - " + period_end_date

      $("#id").val(data._id)
      $("#name_edit").val(data.name)
      $("#code_edit").val(data.code)
      $("#description_edit").val(data.description)
      $("#period_range_edit").val(period_range)
      $("input[name='daterangepicker_start']").val(period_start_date)
      $("input[name='daterangepicker_end']").val(period_end_date)
      $("input[name=mark_type][value=" + data.markType + "]").prop('checked', true)
      $("#value_edit").val(data.value)
      $("#value_type_edit").val(data.valueType)
      $("#mark_type_edit").val(data.markType)
    } else {
      console.log(result)
    }
  })
})

// select transaction item type (service type)
$(document).ready(function () {
  $('select[name="mark_type"]').on('change', function () {
    if (this.value == "discount") {
      $('.promo_group').removeClass('hidden')
    } else {
      $('.promo_group').addClass('hidden')
    }
  })
})

//Edit Testimony
$(".edit_testimony").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#user_id_edit").val(data.user)
      $("#content_edit").val(data.content)
    } else {
      console.log(result)
    }
  })
})

// rating input control
$(".rating .star").on('click', function (event) {
  event.stopPropagation()
  event.stopImmediatePropagation()
  var rating = $(this).data('value')
  $('input:radio[name="stars"][value=' + rating + ']').prop('checked', true)
})

//Edit Review
$(".edit_review").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#user_id_edit").val(data.user)
      $("#room_id_edit").val(data.room_type)
      $("#content_edit").val(data.content)
      $('#ModalUpdateReview input:radio[name="stars"][value=' + data.stars + ']').prop('checked', true)
    } else {
      console.log(result)
    }
  })
})

//Edit Service Stype
$(".edit_service_type").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
    } else {
      console.log(result)
    }
  })
})

//Edit Service Group
$(".edit_service_group").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
    } else {
      console.log(result)
    }
  })
})

//Create/Edit Modal Service
$('#ModalCreateService #name').on('change', function () {
  var val = $(this).val()
  var newval = val.toLowerCase().replace(/\s+/g, '_')
  console.log(val, newval)
  $("#slug").val(newval)
})
$('#ModalUpdateService #name_edit').on('change', function () {
  var slug = $('#ModalUpdateService #slug_edit').val()
  var val = $(this).val()
  var newval = slug != "extra_bed" ? val.toLowerCase().replace(/\s+/g, '_') : slug
  console.log(val, newval)
  $("#slug_edit").val(newval)
})

//Edit Service
$(".edit_service").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
      $("#slug_edit").val(data.slug)
      $("#price_edit").val(data.price)
      $("#type_id_edit").val(data.type)
      $("#group_edit").val(data.type)
      $("#link_type_id_edit").val(data.linkType)
      $("#period_type_id_edit").val(data.periodType)
    } else {
      console.log(result)
    }
  })
})

//Edit Refund Policy
$(".edit_refund_policy").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#days_before_edit").val(data.daysBeforeStay)
      $("#type_edit").val(data.type)
      $("#value_edit").val(data.value)
      $("#value_type_edit").val(data.valueType)
    } else {
      console.log(result)
    }
  })
})

//Edit Room Notes
$(".edit_room_notes").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#notes_edit").val(data.notes)
      $("#priority_level_edit").val(data.priorityLevel)
    } else {
      console.log(result)
    }
  })
})

//add_rooms_agent
$(".add_rooms_agent").click(function () {
  var id = $(this).data('id')
  $("#id").val(id)
})

// remove_rooms_agent
$(".remove_rooms_agent").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      var room_list = data.roomsAllocated.map((ro,i)=>{
        return `
          <div class="col-md-6">
            <div class="checkbox checkbox-gray">
              <label class="">
                <input type="checkbox" name="rooms" value="`+ro._id+`" checked><span class="label-text">`+ro.room_number+`</span>
              </label>
            </div>
          </div>
        `
      })
      $("#id_remove").val(data._id)
      $("#room_list").html(room_list.join(' '))
    } else {
      console.log(result)
    }
  })
})


//Edit travel agent
$(".edit_travel_agent").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
      $("#color_edit").val(data.color)
    } else {
      console.log(result)
    }
  })
})


//Edit Slider
$(".edit_slider").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#title_edit").val(data.title)
      $("#subtitle_edit").val(data.subtitle)
    } else {
      console.log(result)
    }
  })
})

//Edit General Facility
$(".edit_general_facility").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      console.log(data)
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
      $("#content_edit").summernote('code', data.content)
    } else {
      console.log(result)
    }
  })
})

//Edit Booking Package
$(".edit_booking_package").click(function () {
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))

      var period_start = new Date(data.periodStart)
      var month = period_start.getMonth() + 1
      var date = period_start.getDate()
      var year = period_start.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var period_start_date = month + "/" + date + "/" + year

      var period_end = new Date(data.periodEnd)
      var month = period_end.getMonth() + 1
      var date = period_end.getDate()
      var year = period_end.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var period_end_date = month + "/" + date + "/" + year

      var period_range = period_start_date + " - " + period_end_date

      console.log(data)
      $("#id").val(data._id)
      $("#name_edit").val(data.name)
      $("#description_edit").val(data.description)
      $("#period_range_edit").val(period_range)
      $("input[name='daterangepicker_start']").val(period_start_date)
      $("input[name='daterangepicker_end']").val(period_end_date)
      $("#numofroom_edit").val(data.numOfRoom)
      $("#maxnumofguest_edit").val(data.maxNumOfGuest)
      $("#room_type_edit").val(data.roomType)
      $("#promo_edit").val(data.discountRef)
    } else {
      console.log(result)
    }
  })
})

//show booking room detail
$(".book_detail").click(function (e) {
  e.preventDefault()
  var url = $(this).data('url')
  var number = $(this).data('roomnum')
  var status = $(this).data('status')
  var card_number = $(this).data('cardnum')
  $.getJSON(url, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      var data = jQuery.parseJSON(JSON.stringify(result.data))
      var bookingDate = new Date(data.bookCheckInDate)
      var month = bookingDate.getMonth() + 1
      var date = bookingDate.getDate()
      var year = bookingDate.getFullYear()
      if (date < 10) date = "0" + date
      if (month < 10) month = "0" + month
      var checkoutDate = new Date(data.bookCheckOutDate)
      var month_out = checkoutDate.getMonth() + 1
      var date_out = checkoutDate.getDate()
      var year_out = checkoutDate.getFullYear()
      if (date_out < 10) date_out = "0" + date_out
      if (month_out < 10) month_out = "0" + month_out
      var bookingDate_in = year + "-" + month + "-" + date
      var checkoutDate_in = year_out + "-" + month_out + "-" + date_out
      $("#room_number").val(number)
      $("#guest_name").val(data.guest.name)
      $("#checkin").val(bookingDate_in)
      $("#checkout").val(checkoutDate_in)
      $("#status").val(status)
      $("#card_number").val(card_number)
    } else {
      console.log(result)
    }
  })
})

$(document).ready(function () {
  var table = $('.datatable-sourced').DataTable({
    orderCellsTop: true,
    fixedHeader: true,
    ajax: $('.datatable-sourced').data('url'),
    "order": [],
    "columnDefs": [{
      "targets": 'no-sort',
      "orderable": false,
    }],
    dom: 'Bfrtip',
    buttons: [{
      extend: 'print',
      exportOptions: {
        columns: '.view'
      }
    }]
  })

  $('.datatable-sourced thead tr:eq(1) th').each(function () {
    var title = $(this).text()
    $(this).html('<input type="text" placeholder="Search ' + title + '" class="column_search" />')
  })

  // Apply the search
  $('.datatable-sourced thead').on('keyup', ".column_search", function () {
    table
      .column($(this).parent().index())
      .search(this.value)
      .draw()
  })

  //Switch Room Type
  $("#switch_type").change(function () {
    var val = $(this).val()
    $('#select_room_type option').addClass('hidden')
    $('#select_room_type option.initial').removeClass('hidden')
    $('#select_room_type option.' + this.value).removeClass('hidden')
  })

  // room slot change month
  $("#month_opt").change(function () {
    var val = $(this).val()
    var url = $(this).data('url')
    console.log(url+val)
    window.location.href = url+val
  })

  //Change payment method
  $("#paymentMethod").change(function () {
    var slug = $(this).find(':selected').data('slug')
    if(slug == 'cash'){
      $('#paymentStatus').val('paid').attr("style", "pointer-events: none;");
      $('#paymentStatus option:not(:selected)').prop('disabled', true);
    } else {
      $('#paymentStatus').attr("style", "pointer-events: auto;");
      $('#paymentStatus option:not(:selected)').prop('disabled', false);      
    }

    if(slug=='debit_kredit' || slug=='transfer' || slug=='virtual_account'){
      $('#bankAccountContainer').removeClass('hidden')
    } else {
      $('#bankAccountContainer').addClass('hidden')
    }
  })

  //Occupancy 
  $(".daterange_occupancy").change(function (e) {
    e.preventDefault()
    var val = $(this).val()
    table.destroy()
    var date = replaceAll(val, '/', '-')
    date = replaceAll(date, ' - ', '---')
    var url = $(this).data('url')

    $.getJSON(url+date, function (data) {
      var result = jQuery.parseJSON(JSON.stringify(data))
      if (result.status == "200") {
        var htmls = []
        result.data.map((o)=>{
          htmls.push(JSON.stringify(o.html))
        })
        $("#occupancy_box").html(htmls)
      }
    })
  })

  $(".daterange_table").change(function (e) {
    e.preventDefault()
    var val = $(this).val()
    table.destroy()
    var date = replaceAll(val, '/', '-')
    date = replaceAll(date, ' - ', '---')
    table = $('.datatable-sourced').DataTable({
      ajax: $('.datatable-sourced').data('url') + date,
      "order": [],
      "columnDefs": [{
        "targets": 'no-sort',
        "orderable": false,
      }],
      dom: 'Bfrtip',
      buttons: [{
        extend: 'print',
        exportOptions: {
          columns: '.view'
        }
      },
      { extend: 'pdf', className: 'btn-xs' }]
    })
  })
  var oTable = $('#transaction_items_table').DataTable({
    "paging": false,
    "ordering": false,
    "info": false
  })

  // update table data input form
  var tabledt = $('#transaction_items_table').tableToJSON({
    ignoreColumns: [1]
  })
  console.log(tabledt)
  $("#transaction_items").val(JSON.stringify(tabledt))

  // add new item
  $('#btn-add-item-submit').click(function () {
    var item_id = $("select#item option:selected").val()
    var item_name = $("select#item option:selected").html()
    var quantity = $('#quantity').val()
    var price = $("select#item option:selected").data('price')
    var period = ($("#period").val() ? $("#period").val() : 1)
    var roomId = $("#room").val()
    var roomName = $("select#room option:selected").data('name')
    console.log(roomName)
    if (item_id != '' && item_name != '' && quantity != '' && price != '') {
      $('#ModalAddTI').modal('toggle')
      document.getElementById("add_ti_form").reset()

      oTable.row.add([
        '<div class="td_hidden">' + item_id + '</div>',
        item_name,
        '<div class="td_hidden">' + roomId + '</div>',
        (roomName ? roomName : "-"),
        quantity,
        period,
        price,
        price * quantity * period,
        '<button class="btn btn-danger delete_btn" type="button"><span class="fa fa-trash"></span></button>'
      ]).draw()

      // update table data input form
      var tabledt = $('#transaction_items_table').tableToJSON({
        ignoreColumns: [1]
      })

      // hide id column from table
      $("td > div.td_hidden").parent().addClass('hidden')

      console.log(tabledt)
      $("#transaction_items").val(JSON.stringify(tabledt))

    } else {
      alert("Please fill in all required field!")
    }
  })

  // delete add item table row
  $('#transaction_items_table tbody').on('click', '.delete_btn', function () {
    oTable
      .row($(this).parents('tr'))
      .remove()
      .draw()

    // update table data input form
    var tabledt = $('#transaction_items_table').tableToJSON({
      ignoreColumns: [1]
    })
    $("#transaction_items").val(JSON.stringify(tabledt))
  })
})

// select transaction item type (service type)
$('#service_type').on('change', function () {
  console.log('here')
  $('#item option').addClass('hidden')
  $('#item option.initial').removeClass('hidden')
  $('#item option.' + this.value).removeClass('hidden')
})

// var name = $('#5ca94349c5edc02bfc0767b1').data('name')
$("#item").change(function () {
  var linkType = $("#item option:selected").data('linktype')
  var periodType = $("#item option:selected").data('periodtype')
  if (linkType == "room") {
    $("#room_choice").removeClass("hidden")
  } else {
    $("#room_choice").addClass("hidden")
  }

  if (periodType != "onetime") {
    $("#period").attr("required", true)
    $("#period_choice").removeClass("hidden")
  } else {
    $("#period").attr("required", false)
    $("#period_choice").addClass("hidden")
  }
})
// create booking select date update other fields
$(document).ready(function () {
  $('#create_booking_form #checkin').on('change', function () {
    var room_type_url = '/room_type_list'
    var promo_url = '/event_promo_list'
    var checkin = $('.booking-form #checkin').val()
    var checkout = $('.booking-form #checkout').val()
    $.post(room_type_url, {
      checkin: checkin,
      checkout: checkout,
      type: 'minimal'
    }, function (data) {
      var result = JSON.parse(data)
      if (result.status == 200) {
        var data = result.data
        console.log(data)
        $('.booking-form .custom_dropdown.room_type li').addClass('hidden')
        data.map(rt => $('.booking-form .custom_dropdown.room_type li#' + rt.id).removeClass('hidden')
          .html(`
          <div data-capacity=`+rt.capacity+`>
            <h6>` + rt.name + `</h6>
            <p>
              AVAILABLE: ` + rt.avail + ` rooms<br/>
              PRICE: Rp.` + rt.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + `<br/>
              CAPACITY: ` + rt.capacity + ` person<br/>
              MAX EXTRA BED: ` + rt.maxExtraBed.toString() + ` beds
            </p>
          </div>
        `))
      } else {
        console.log(result)
      }
    })
    $.post(promo_url, {
      checkin: checkin,
      checkout: checkout
    }, function (data) {
      var result = JSON.parse(data)
      if (result.status == 200) {
        var data = result.data
        console.log(data)
        $('.booking-form select#discount_id option').addClass('hidden')
        $('.booking-form select#discount_id option.default').removeClass('hidden')
        data.map(pr => $('.booking-form select#discount_id option[value=' + pr._id + ']').removeClass('hidden'))
      } else {
        console.log(result)
      }
    })
  })

  $('#create_booking_form #checkout').on('change', function () {
    var room_type_url = '/room_type_list'
    var promo_url = '/event_promo_list'
    var checkin = $('.booking-form #checkin').val()
    var checkout = $('.booking-form #checkout').val()
    $.post(room_type_url, {
      checkin: checkin,
      checkout: checkout,
      type: 'minimal'
    }, function (data) {
      var result = JSON.parse(data)
      if (result.status == 200) {
        var data = result.data
        console.log(data)
        $('.booking-form .custom_dropdown.room_type li').addClass('hidden')
        data.map(rt => $('.booking-form .custom_dropdown.room_type li#' + rt.id).removeClass('hidden')
          .html(`
          <div data-capacity=`+rt.capacity+`>
            <h6>` + rt.name + `</h6>
            <p>
              AVAILABLE: ` + rt.avail + ` rooms<br>
              PRICE: Rp.` + rt.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + `<br>
              CAPACITY: ` + rt.capacity + ` person<br/>
              MAX EXTRA BED: ` + rt.maxExtraBed.toString() + ` beds
            </p>
          </div>
        `))
      } else {
        console.log(result)
      }
    })
    $.post(promo_url, {
      checkin: checkin,
      checkout: checkout
    }, function (data) {
      var result = JSON.parse(data)
      if (result.status == 200) {
        var data = result.data
        console.log(data)
        $('.booking-form select#discount_id option').addClass('hidden')
        $('.booking-form select#discount_id option.default').removeClass('hidden')
        data.map(pr => $('.booking-form select#discount_id option[value=' + pr._id + ']').removeClass('hidden'))
      } else {
        console.log(result)
      }
    })
  })
})

$(document).ready(function () {
  $("#wait").hide()
  $(".btn-confirm").click(function () {
    console.log($(this).data('url'))
    $('#confirm-btn-submit').attr('href', $(this).data('url'))
  })
  $(".btn-reject").click(function () {
    console.log($(this).data('url'))
    $('#reject-btn-submit').attr('href', $(this).data('url'))
  })
  $(".btn-checkout").click(function () {
    console.log($(this).data('url'))
    $('#checkout-btn-submit').attr('href', $(this).data('url'))
  })
})

$('.pop').on('click', function () {
  $('.imagepreview').attr('src', $(this).find('img').attr('src'))
  $('#imagemodal').modal('show')
})

/*Dropdown Menu*/
$(document).on('click', '.custom_dropdown', function(e) {
  e.preventDefault()
  $(this).attr('tabindex', 1).focus()
  $(this).toggleClass('active')
  $(this).find('.dropdown-menu').slideToggle(300)
})
$('.custom_dropdown').focusout(function () {
  $(this).removeClass('active')
  $(this).find('.dropdown-menu').slideUp(300)
})
$(document).on('click', '.custom_dropdown .dropdown-menu li', function(e) {
  e.preventDefault()
  $(this).parents('.custom_dropdown').find('span').text($(this).data('name'))
  $(this).parents('.custom_dropdown').find('input').attr('value', $(this).attr('id'))
})
$(document).on('change', "input[name='guestnum']", function(e) {
  var dataId = $(this).parents('.mix_room_content').find("input[name='room_type']").val();
  var capacity = $('.custom_dropdown .dropdown-menu li#'+dataId).find('div').data('capacity');
  var guestnum = $(this).parents('.mix_room_content').find("#guestnum").val();
  var roomnum = Math.ceil(parseInt(guestnum) / parseInt(capacity));
  console.log(dataId, capacity, guestnum, roomnum)
  $(this).parents('.mix_room_content').find("#roomnum").val(roomnum).attr('min', roomnum)
})
/*End Dropdown Menu*/

// // setup booking datepicker
var nowTemp = new Date()
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0)

var checkin = $('#checkin').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  startDate: new Date(),
  beforeShowDay: function (date) {
    return date >= now
  },
  autoclose: true
}).on('changeDate', function (ev) {
  if (ev.date > checkout.datepicker("getDate") || !checkout.datepicker("getDate")) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1)
    checkout.datepicker("update", newDate)
  }
  $('#checkout')[0].focus()
})

var checkout = $('#checkout').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  beforeShowDay: function (date) {
    if (!checkin.datepicker("getDate")) {
      return date.valueOf() >= new Date()
    } else {
      return date.valueOf() > checkin.datepicker("getDate").valueOf()
    }
  },
  autoclose: true
}).on('changeDate', function (ev) {})

$('.daterangepicker-input').daterangepicker()

$(".monthrangepicker").datepicker({
  format: "mm-yyyy",
  viewMode: "months",
  minViewMode: "months"
})

$(".modal_show").click(function (e) {
  e.preventDefault()
  var target = $(this).data('target')
  $(target).show()
})

$(".get-room-available").change(function (e) {
  e.preventDefault()
  var id = $(this).val()
  var url = $(this).data('url') + "/" + id
  var target = $(this).data('target')
  $.getJSON(url, function (data) {
    console.log(url)
    var result = jQuery.parseJSON(JSON.stringify(data))
    if (result.status == 200) {
      const items = result.data
      $(target).html('<option value="">Choose Room')
      $.each(items, function (index, value) {
        $(target).append($('<option/>', {
          value: value._id,
          text: value.room_number
        }))
      })
    } else {
      console.log(result)
    }
  })
})

$(".daterange_chart").change(function (e) {
  e.preventDefault()
  var val = $(this).val()
  var chartContainer = $(this).data("chartcontainer")
  var url = $(this).data('url')
  var date = replaceAll(val, '/', '-')
  date = replaceAll(date, ' - ', '---')
  $.getJSON(url + date, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    console.log(data)
    var chart = new CanvasJS.Chart(chartContainer, {
      animationEnabled: true,
      theme: "light1", // "light2", "dark1", "dark2"
      animationEnabled: false, // change to true		
      title: {
        text: "Chart Data"
      },
      data: [{
        // Change type to "bar", "area", "spline", "pie",etc.
        type: "column",
        dataPoints: result
      }]
    })
    chart.render()
  })
})

$("#refresh_chart_data").click(function (e) {
  e.preventDefault()
  console.log("TERPENCET")
  var date = $("#chart_start").val()
  var type = $("#report_type").val()
  if (type == "service") {
    var service_type = $("#service_type").val()
    var service_group = $("#service_group").val()
  } else {
    var service_type = "none"
    var service_group = "none"
  }
  var chartContainer = $(this).data("chartcontainer")
  date = replaceAll(date, '/', '-')
  date = replaceAll(date, ' - ', '---')
  var url = $(this).data('url')
  $.getJSON(url + date + "/" + type + "/" + service_type + "/" + service_group, function (data) {
    var result = JSON.parse(JSON.stringify(data))
    var chart = new CanvasJS.Chart(chartContainer, {
      animationEnabled: true,
      theme: "light1", // "light2", "dark1", "dark2"
      animationEnabled: false, // change to true		
      title: {
        text: "Chart Data"
      },
      data: [{
        // Change type to "bar", "area", "spline", "pie",etc.
        type: "column",
        dataPoints: result
      }]
    })
    chart.render()
  })
})

$("#report_type").change(function () {
  var val = $(this).val()
  if (val == "service") {
    $("#service_type_group").removeClass("hidden")
    $("#service_group_group").removeClass("hidden")
  } else {
    $("#service_type_group").addClass("hidden")
    $("#service_group_group").addClass("hidden")
  }
})

$(".daterange_chart_line").change(function (e) {
  e.preventDefault()
  var val = $(this).val()
  var chartContainer = $(this).data("chartcontainer")
  var url = $(this).data('url')
  var date = replaceAll(val, '/', '-')
  date = replaceAll(date, ' - ', '---')
  $.getJSON(url + date, function (data) {
    var result = jQuery.parseJSON(JSON.stringify(data))
    console.log(data)
    var chart = new CanvasJS.Chart(chartContainer, {
      animationEnabled: true,
      theme: "light1", // "light2", "dark1", "dark2"
      animationEnabled: false, // change to true		
      title: {
        text: "Chart Data"
      },
      data: [{
        // Change type to "bar", "area", "spline", "pie",etc.
        type: "line",
        dataPoints: result
      }]
    })
    chart.render()
  })
})

$(document).ajaxStart(function () {
  $("#wait").show()
})

$(document).ajaxComplete(function () {
  $("#wait").hide()
})

$("#mark_type").change(function () {
  var value = $(this).val()
  if (value == 'markup' || value == 'markup') {
    $("#name").show()
    $("#name").attr("required", "required")
    $("#mark_type").show()
    $("#mark_type").attr("required", "required")
    $("#description").show()
    $("#description").attr("required", "required")
    $("#code").hide()
    $("#code").removeAttr("required")
    $("#image").show()
    $("#image").attr("required", "required")
    $("#period_range").show()
    $("#value_type").show()
    $("#value_type").attr("required", "required")
    $("#company2").hide()
    $("#company").removeAttr("required")
  } else if (value == 'discount') {
    $("#name").show()
    $("#mark_type").show()
    $("#description").show()
    $("#code").show()
    $("#image").show()
    $("#period_range").show()
    $("#value_type").show()
    $("#company2").hide()
    $("#company").removeAttr("required")
  } else if (value == 'package') {
    $("#name").show()
    $("#mark_type").show()
    $("#description").show()
    $("#code").hide()
    $("#code").removeAttr("required")
    $("#image").show()
    $("#period_range").show()
    $("#value_type").show()
    $("#company2").hide()
    $("#company").removeAttr("required")
  } else if (value == 'specialrate') {
    $("#name").show()
    $("#name").attr("required", "required")
    $("#mark_type").show()
    $("#mark_type").attr("required", "required")
    $("#description2").hide()
    $("#description").removeAttr("required")
    $("#code").hide()
    $("#code").removeAttr("required")
    $("#image2").hide()
    $("#image").removeAttr("required")
    $("#period_range").show()
    $("#period_range").attr("required", "required")
    $("#value_type").show()
    $("#value_type").attr("required", "required")
    $("#company2").show()
    $("#company").attr("required", "required")
  }
})

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace)
}

$(function () {
  $('[data-toggle="popover"]').popover()
})

$(".add_room_type").click(function(e) {
  var html = $(".mix_room_content").html()
  temp = document.createElement('div');
  temp.innerHTML = html;
  temp.setAttribute("class","mix_room_content")
  $('#mix_room_field').append(temp)
  $('.delete_room_type').show()
})

$(".delete_room_type").click(function(e) {
  if($('.mix_room_content').length > 1){
    $('#mix_room_field .mix_room_content:last').remove()
    if($('.mix_room_content').length == 1) $('.delete_room_type').hide()
  } 
})
