$(".submit-btn").click(function (e) {
  e.preventDefault();
  var form_id = $(this).attr("form-id");
  var form = $("#" + form_id);
  var submit_url = form.attr('action');
  var refresh_url = form.attr('refresh');
  var warning_id = $(this).attr("warning-id");
  var warning = $("#" + warning_id);
  var datastring = form.serialize();
  $("i.login-spin").removeClass('hidden');

  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function (response) {
      var result = jQuery.parseJSON(JSON.stringify(response));
      if (result.status == "200") {

        $(form).trigger("reset")
        $("i.login-spin").removeClass('hidden');
        //delete class
        warning.removeClass("alert-primary");
        warning.removeClass("alert-secondary");
        warning.removeClass("alert-warning");
        warning.removeClass("alert-danger");
        warning.removeClass("alert-info");
        warning.removeClass("alert-light");
        warning.removeClass("alert-dark");
        warning.removeClass("hidden");
        //add class
        warning.addClass("alert-success");
        warning.text(result.message);
        console.log(result);
        if (refresh_url != "") window.location = refresh_url;
      } else {
        //Reset Form
        // $(form).trigger("reset")

        $("i.login-spin").addClass('hidden');
        //delete class
        warning.removeClass("alert-primary");
        warning.removeClass("alert-secondary");
        warning.removeClass("alert-warning");
        warning.removeClass("alert-success");
        warning.removeClass("alert-info");
        warning.removeClass("alert-light");
        warning.removeClass("alert-dark");
        warning.removeClass("hidden");
        //add class
        warning.addClass("alert-danger");
        warning.text(result.message);
        console.log(result);
        warning.show();
      }
    },
    dataType: "json"
  });
});

$(".submit-btn-sweetalert").click(function (e) {
  e.preventDefault();
  var form_id = $(this).attr("form-id");
  var form = $("#" + form_id);
  var submit_url = form.attr('action');
  var refresh_url = form.attr('refresh');
  var warning_id = $(this).attr("warning-id");
  var warning = $("#" + warning_id);
  var datastring = form.serialize();
  $("i.login-spin").removeClass('hidden');

  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function (response) {
      var result = jQuery.parseJSON(JSON.stringify(response));
      if (result.status == "200") {

        $(form).trigger("reset")
        swal({
          title: "Information",
          text: result.message,
          icon: "success",
          });
      } else {
        //Reset Form
        // $(form).trigger("reset")
        swal({
          title: "Information",
          text: result.message,
          icon: "error",
          });
        
      }
    },
    dataType: "json"
  });
});

$("#termsAndConditionCheck").change(function () {
  if ($(this).is(':checked')) {
   $("#signup").removeAttr('disabled')
  } else {
   $("#signup").attr("disabled", "disabled")
  }
})

$(document).ajaxStart(function () {
  $("#wait").show()
})

$(document).ajaxComplete(function () {
  $("#wait").hide()
})
$(document).ready(function () {
  $("#wait").hide()
  $('.datatable').dataTable({
    "order": [], //Initial no order.
         "aaSorting": [],
  });
})