function previewFile(dom_id) {
  var file    = document.querySelector('#'+dom_id).files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    var filename = $('input[type=file]#'+dom_id).val().replace(/.*(\/|\\)/, '')
    if(dom_id == 'image_edit'){
      $('#image_label_edit').text(filename)
      $('#profile_imgsrc_edit').val(reader_edit.result)
    } else {
      $('#image_label').text(filename)
      $('#profile_imgsrc').val(reader.result)
    }
  }, false)

  if (file) {
    reader.readAsDataURL(file);
  }
}

$(".delete_btn").click(function (e) {
  e.preventDefault();
  var url = $(this).data("url");
  $("#delete_footer").attr("href", url);
});

$("body").on("click", ".swal-button--confirm", function(){
  $(".swal-overlay").hide();
});

$(".edit_department").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#name_edit").val(data.data.name);
  });
});

$(".edit_article").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#title_edit").val(data.data.title);
    $("#subtitle_edit").val(data.data.subtitle);
    $("#type_edit").val(data.data.type);
    $("#content_edit").val(data.data.content);
  });
});

$(".edit_reward").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#title_edit").val(data.data.title);
    $("#pointsRequired_edit").val(data.data.pointsRequired);
    $("#storeRef_edit").val(data.data.storeRef);
  });
});

$(".edit_head_office").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
  });
});

$(".edit_brand").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
  });
});

$(".edit_store").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#brand_edit").val(data.data.brand);
    $("#unitKerja_edit").val(data.data.unitKerja);
    $("#outerJakarta_edit").prop('checked', data.data.outerJakarta == 'true' ? true : false);
  });
});

$(".edit_product_group").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#brand_edit").val(data.data.brand);
  });
});

$(".edit_product_category").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#productGroup_edit").val(data.data.productGroup);
    $("#brand_edit").val(data.data.brand);
  });
});

$(".edit_product").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#productCode_edit").val(data.data.productCode);
    $("#PLUId_edit").val(data.data.PLUId);
    $("#printerId_edit").val(data.data.printerId);
    $("#price_edit").val(data.data.price);
    $("#validStartDate_edit").val(data.data.validStartDate);
    $("#description_edit").val(data.data.description);
    $("#brand_edit").val(data.data.brand).change();
    $("#productgroup_edit").val(data.data.productgroup).change();
    var i;
    $("#productcategory_edit_"+data.data.productgroup+" option:selected").prop("selected", false);
    for (i = 0; i < data.data.productcategory.length; ++i) {
      console.log(i)
      $("#productcategory_edit_"+data.data.productgroup+" option[value='" + data.data.productcategory[i] + "']").prop("selected", true);
    }
    $('#productcategory_edit_'+data.data.productgroup).select2();    
  });
});

$(".edit_user").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    $("#id").val(data.data._id);
    $("#name_edit").val(data.data.name);
    $("#email_edit").val(data.data.email);
    $("#phone_edit").val(data.data.phone);
    $("#birthDate_edit").val(data.data.birthDate);
    $("#gender_edit").val(data.data.gender);
    $("#religion_edit").val(data.data.religion);    
    $("#address_edit").val(data.data.address);
    $("#role_edit").val(data.data.role).change();
    $("#headOffice_edit").val(data.data.headOfficeRef).change();
    $("#brand_edit").val(data.data.brandRef).change();
    $("#store_edit").val(data.data.storeRef).change();
  });
});

$("select[name='role']").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $(".store-user-req").hide()
  if(val == 'store') $(".store-user-req").show()
});

$("select#headOffice").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select.ho-ref option").hide()
  $("select.ho-ref option."+val).show()
});

$("select#headOffice_edit").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select.ho-ref-edit option").hide()
  $("select.ho-ref-edit option."+val).show()
});

$("select#brand").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select.brand-ref option").hide()
  $("select.brand-ref option."+val).show()
});

$("select#brand_edit").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("select#productgroup_edit option").hide()
  $("select#productgroup_edit option."+val).show()
  $("select.brand-ref-edit option").hide()
  $("select.brand-ref-edit option."+val).show()
});

$("select#productgroup").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("#productgroup_ref_temporary").show()
  if ($(".productgroup-ref#"+val).length) $("#productgroup_ref_temporary").hide()
  $(".productgroup-ref").hide()
  $(".productgroup-ref#"+val).show()
});

$("select#productgroup_edit").change(function (e) {
  e.preventDefault();
  var val = $(this).val()
  $("#productgroup_ref_temporary_edit").show()
  if ($("#pgr-"+val).length) $("#productgroup_ref_temporary_edit").hide()
  $(".productgroup-ref-edit").hide()
  $(".productgroup-ref-edit select").prop('dissabled',true)
  $("#pgr-"+val).show()
  $("#pgr-"+val+" select").prop('dissabled',false)
});

$(document).on('change', "select[name='types']", function(e) {
  var val = this.value
  var q = "div[data-type='"+val+"']"
  $(this).parent().siblings().hide()
  $(this).parent().parent().find(q).show()
})

$(document).on('click', ".add_requirements", function(e) {
  var buttontype = $(this).data('type')
  var wrapperid = ''
  if(buttontype == 'create') wrapperid = '#wrapper_requirement_items'
  else if(buttontype == 'update') wrapperid = '#wrapper_requirement_items_edit'

  var html = $("#requirement_item_form_template").html()
  temp = document.createElement('div');
  temp.innerHTML = html;
  temp.setAttribute("class","requirement-items")

  $(wrapperid).append(temp)
  $(wrapperid+' .requirement-items select[name="types"]').attr('required',true)
  $(wrapperid+' + .wrapper-requirement-buttons .remove_requirements').show()
})

$(document).on('click', ".remove_requirements", function(e) {
  var buttontype = $(this).data('type')
  var wrapperid = ''
  if(buttontype == 'create') wrapperid = '#wrapper_requirement_items'
  else if(buttontype == 'update') wrapperid = '#wrapper_requirement_items_edit'

  if($(wrapperid+' .requirement-items').length > 1){
    $(wrapperid+' .requirement-items:last').remove()
    if($(wrapperid+' .requirement-items').length == 1) $(wrapperid+' + .wrapper-requirement-buttons .remove_requirements').hide()
  } 
})

$(".edit_challenge").click(function (e) {
  e.preventDefault();
  var url = $(this).data('url')
  $.getJSON(url, function (data) {
    console.log(data.data)
    var valid_from_raw = new Date(data.data.validFrom)
    var month = valid_from_raw.getMonth() + 1
    var date = valid_from_raw.getDate()
    var year = valid_from_raw.getFullYear()
    if (date < 10) date = "0" + date
    if (month < 10) month = "0" + month
    var valid_from = month + "/" + date + "/" + year

    var valid_to_raw = new Date(data.data.validTo)
    var month = valid_to_raw.getMonth() + 1
    var date = valid_to_raw.getDate()
    var year = valid_to_raw.getFullYear()
    if (date < 10) date = "0" + date
    if (month < 10) month = "0" + month
    var valid_to = month + "/" + date + "/" + year

    var html = $("#requirement_item_form_template").html()
    temp = document.createElement('div');
    temp.innerHTML = html;
    temp.setAttribute("class","requirement-items")
  
    var wrapperid = '#wrapper_requirement_items_edit'
    data.data.requirements.map(r=>{
      var temp2 = temp.cloneNode(true)
      $(wrapperid).append(temp2)
    })

    $(wrapperid+' .requirement-items select[name="types"]').attr('required',true)
    if($(wrapperid+' .requirement-items').length > 1) $(wrapperid+' + .wrapper-requirement-buttons .remove_requirements').show()
    else $(wrapperid+' + .wrapper-requirement-buttons .remove_requirements').hide()

    data.data.requirements.map((r,i)=>{
      var type_input_query = wrapperid+' .requirement-items:nth-child('+(i+1)+') select[name="types"]'
      $(type_input_query).val(r.type).change()

      var ref_input_query = wrapperid+' .requirement-items:nth-child('+(i+1)+')'
      if(r.type=='product') {
        ref_input_query+=' select[name="products"]'
        $(ref_input_query).val(r.productRef).change()
      }
      else if(r.type=='product_group') {
        ref_input_query+=' select[name="productGroups"]'
        $(ref_input_query).val(r.productGroupRef).change()
      }
      else if(r.type=='product_category') {
        ref_input_query+=' select[name="productCategories"]'
        $(ref_input_query).val(r.productCategoryRef).change()
      }
      else if(r.type=='visit_day') {
        ref_input_query+=' select[name="visitDays"]'
        $(ref_input_query).val(r.visitDay).change()
      }
      else if(r.type=='minimum_payment') {
        ref_input_query+=' input[name="minimumPayments"]'
        $(ref_input_query).val(r.minimumPayment)
      }
      else if(r.type=='number_of_visit') {
        ref_input_query+=' input[name="numberOfVisits"]'  
        $(ref_input_query).val(r.numberOfVisit)
      }
    })

    $("#id").val(data.data._id)
    $("#title_edit").val(data.data.title)
    $("#subtitle_edit").val(data.data.subtitle)
    $("#pointsOffered_edit").val(data.data.pointsOffered)
    $("#validFrom_edit").val(valid_from)
    $("#validTo_edit").val(valid_to)
  });
});

$(document).ready(function(){
  setTimeout(function() { 
    $(".button-add-product").html(`<button type="button" class="btn btn-primary btn-sm mb-1" data-toggle="modal" data-target="#create_modal">ADD MORE PRODUCT</button>`);
  }, 500);  
});

$(function () {
  var oTable = $(".data-table-custom-without-search").DataTable({
    sDom: '<"row view-filter ph-5em"<"col-sm-12"<"float-right"l><"float-left button-add-product"><"clearfix">>><"table-responsive pr-5em"t><"row view-pager"<"col-sm-12"<"text-center"ip>>>',
    drawCallback: function () {
      $($(".dataTables_wrapper .pagination li:first-of-type"))
        .find("a")
        .addClass("prev");
      $($(".dataTables_wrapper .pagination li:last-of-type"))
        .find("a")
        .addClass("next");

      $(".dataTables_wrapper .pagination").addClass("pagination-sm");
    },
    language: {
      paginate: {
        previous: "<i class='simple-icon-arrow-left'></i>",
        next: "<i class='simple-icon-arrow-right'></i>"
      },
      lengthMenu: "Items Per Page _MENU_"
    }
  })

  // add new item
  $('.add-product-to-list').click(function () {
    var selected = $('#pre_add_product_form #product').find(':selected')
    var quantity = $('#pre_add_product_form #quantity').val()
    var notes = $('#pre_add_product_form #notes').val()
    quantity = quantity != "" ? parseInt(quantity) : 0
    var product = {id: selected.val(), name: selected.text(), price: selected.data('price')}

    console.log(product,quantity)
    if (product.id != "" && quantity) {
      $('.pre-add-product-modal').modal('toggle');
      document.getElementById("pre_add_product_form").reset();
      oTable.row.add([
        '<div class="td_hidden">' + JSON.stringify({id:product.id, price:product.price}) + '</div>',
        product.name,
        quantity,
        notes != "" ? notes : "-",
        "Rp."+product.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        "Rp."+(quantity*product.price).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"),
        `<button type="button" class="btn btn-primary mb-1 delete_btn">
          <div class="glyph-icon simple-icon-trash"></div>
        </button>`        
      ]).draw()
      $("td > div.td_hidden").parent().addClass('hidden').addClass('position-absolute');
      
      // update table data input form
      var tabledt = $('#create_order_table').tableToJSON({
        ignoreColumns: [1, 4, 5, 6]
      })
      var products = tabledt.map(dt=>{
        var single_product = JSON.parse(dt.PP)
        return {notes: dt.Notes, qty: dt.Quantity, product: single_product.id, price: single_product.price}
      })
      console.log(products)
      $("#create_order_form #products").val(JSON.stringify(products))
    } else {
      alert("Please fill in all required field!")
    }
  })  

  // delete add item table row
  $('#create_order_table tbody').on('click', '.delete_btn', function () {
    oTable
      .row($(this).parents('tr'))
      .remove()
      .draw();

    // update table data input form
    var tabledt = $('#create_order_table').tableToJSON({
      ignoreColumns: [1, 4, 5, 6]
    })
    var products = tabledt.map(dt=>{
      return {notes: dt.Notes, qty: dt.Quantity, product: dt.Id}
    })
    console.log(products)
    $("#create_order_form #products").val(JSON.stringify(products))
  });  
})

$(".redeem_btn").click(function (e) {
  e.preventDefault();
  var url = $(this).data("url");
  console.log(url)
  $("#redeem_form").attr("action", url);
});