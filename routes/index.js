const express = require('express')
const router = express.Router()
var multer  = require('multer')

// Require the controllers
const common_controller = require('../controllers/common.controller')
const admin_controller = require('../controllers/admin.controller')
const api_controller = require('../controllers/api.controller')
// const member_controller = require('../controllers/member.controller')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './assets/image')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/')[1])
  }
})
var upload = multer({ storage: storage })

var storage_file_absen = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './assets/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-ATTENDANCE-' + Date.now() + '.' + file.mimetype.split('/')[1])
  },
  fileFilter: function (req, file, callback) { //file filter
    if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
        return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
}
})

var upload_absen = multer({storage:storage_file_absen})

//// ------------- API --------------- ////

router.post('/api/signin', api_controller.signin);
router.get('/api/signout', api_controller.signout);
router.get('/api/my_profile', api_controller.my_profile);
router.post('/api/signup', api_controller.signup);
router.post('/api/update_profile',upload.any(), api_controller.update_profile);
router.get('/api/my_profile', api_controller.my_profile);
router.get('/api/my_quest', api_controller.my_quest);
router.get('/api/my_order', api_controller.my_order);
router.get('/api/news', api_controller.news);
router.get('/api/news/:id', api_controller.single_news);
router.get('/api/promotions', api_controller.promotions);
router.get('/api/product_categories', api_controller.product_categories);
router.get('/api/products/:productcategory', api_controller.products);
router.get('/api/product/:id', api_controller.single_product);
router.get('/api/outlet', api_controller.outlet);
router.post('/api/forgot_password', api_controller.forgot_password);
router.post('/api/change_password', api_controller.change_password);
router.get('/api/settings', api_controller.settings);
router.get('/api/home', api_controller.home);
router.post('/api/activate_challenge', api_controller.activate_challenge);
router.post('/api/generate_point', api_controller.generate_point);
router.get('/api/activate_point/:id', api_controller.activate_point);
router.get('/api/point_history', api_controller.point_history);
router.post('/api/redeem_reward/:id', api_controller.redeem_reward);
router.post('/createMidtransTrasaction',api_controller.createMidtransTrasaction);
//// ------------- Member --------------- ////
router.get('/', common_controller.main_page);
router.get('/signin/', common_controller.signin);
router.post('/signin', common_controller.signin_process);
router.get('/signup/', common_controller.signup);
router.post('/signup', common_controller.signup_process);
router.get('/forgot_password', common_controller.forgot_password);
router.post('/forgot_password', common_controller.forgot_password_process);
router.get('/forgot_password/reset_password/:email/:rstcode', common_controller.reset_password);
router.post('/forgot_password/reset_password/', common_controller.reset_password_process);
router.get('/signout', common_controller.signout);
router.get('/my_profile', common_controller.my_profile);
router.post('/my_profile', common_controller.update_profile);
router.post('/change_password', common_controller.change_password);
router.get('/verification/:id', common_controller.profiling);
router.post('/verification/:id',upload.array("image", 100),common_controller.profiling_process);
router.get('/news', common_controller.news);
router.get('/news/:id', common_controller.single_news);
router.post('/send_message', common_controller.send_message);
router.get('/plan', common_controller.plan);
router.get('/create_quest', common_controller.create_quest);
router.get('/create_quest/:type', common_controller.create_quest_type);
router.post('/create_quest/:type', common_controller.create_quest_process);
router.get('/create_quest/:type/:id', common_controller.create_quest_type);
router.post('/create_quest/:type/:id', common_controller.create_quest_process);
router.get('/quest/:id', common_controller.quest);
router.post('/quest/:id',upload.any(), common_controller.quest_process);
router.get('/anonymous_profiling/:id', common_controller.anonymous_profiling);
router.post('/anonymous_profiling/:id', common_controller.anonymous_profiling_process);
router.get('/my_quest', common_controller.my_quest);
router.get('/my_order', common_controller.my_order);
router.get('/delete_quest/:id', common_controller.delete_quest_process);
router.get('/view_quest/:id', common_controller.view_quest);
router.get('/edit_quest/:id', common_controller.edit_quest);
router.post('/edit_quest/:id', common_controller.edit_quest_process);
router.get('/view_answer/:id/:skip', common_controller.view_answer);
router.get('/view_my_answer/:id/', common_controller.view_my_answer);
router.get('/download_answer/:id', common_controller.download_answer);
router.get('/publish_quest/:id/', common_controller.publish_quest);
router.get('/answer_quest/:id', common_controller.answer_quest);
router.post('/answer_quest/:id',upload.any(), common_controller.answer_quest_process);
router.post('/paymentSuccess/:id', common_controller.paymentSuccess);
router.get('/waiting/:id', common_controller.waiting);
router.get('/quests', common_controller.quests);
router.post('/writeRating', common_controller.writeRating);
router.get('/preview_quest/:id', common_controller.preview_quest);
router.post('/preview_quest', common_controller.preview_quest_process);
router.post('/change_pp', upload.single('image'),common_controller.change_pp);
router.get('/delete_order/:id', common_controller.delete_order_process);



//// -------------  ADMIN --------------- ////
router.get('/admin', admin_controller.dashboard);
router.get('/admin/forgot_password', admin_controller.forgot_password);
router.post('/admin/forgot_password', admin_controller.forgot_password_process);
router.get('/admin/signin', admin_controller.signin);
router.post('/admin/signin', admin_controller.signin_process);
router.get('/admin/profile', admin_controller.profile);
router.post('/admin/profile', upload.single('image'), admin_controller.change_profile);
router.post('/admin/change_password', admin_controller.change_password);
router.get('/admin/signout', admin_controller.signout);
router.get('/admin/setting', admin_controller.setting);
router.post('/admin/setting/common', upload.fields(
  [
    { 
      name: 'logo', 
      maxCount: 1 
    }, 
    { 
      name: 'headerImage', 
      maxCount: 1 
    }, 
    { 
      name: 'aboutUsImage', 
      maxCount: 1 
    }
  ]
),admin_controller.setting_common_save);

//USER
router.get('/admin/user', admin_controller.user);
router.post('/admin/user', upload.single('image'),admin_controller.create_user_process);
router.post('/admin/update_user', upload.single('image'),admin_controller.update_user_process);
router.get('/admin/user/:id', admin_controller.single_user);
router.get('/admin/delete_user/:id', admin_controller.delete_user_process);
//FEATURE
router.get('/admin/feature', admin_controller.feature);
router.post('/admin/feature', upload.single('image'),admin_controller.create_feature_process);
router.post('/admin/update_feature', upload.single('image'),admin_controller.update_feature_process);
router.get('/admin/feature/:id', admin_controller.single_feature);
router.get('/admin/delete_feature/:id', admin_controller.delete_feature_process);
//TESTIMONIAL
router.get('/admin/testimonial', admin_controller.testimonial);
router.post('/admin/testimonial',upload.single('image'), admin_controller.create_testimonial_process);
router.post('/admin/update_testimonial',upload.single('image'), admin_controller.update_testimonial_process);
router.get('/admin/testimonial/:id', admin_controller.single_testimonial);
router.get('/admin/delete_testimonial/:id', admin_controller.delete_testimonial_process);
//FAQ
router.get('/admin/faq', admin_controller.faq);
router.post('/admin/faq',admin_controller.create_faq_process);
router.post('/admin/update_faq',admin_controller.update_faq_process);
router.get('/admin/faq/:id', admin_controller.single_faq);
router.get('/admin/delete_faq/:id', admin_controller.delete_faq_process);
//NEWS
router.get('/admin/news', admin_controller.news);
router.post('/admin/news',upload.single('image'), admin_controller.create_news_process);
router.post('/admin/update_news', upload.single('image'), admin_controller.update_news_process);
router.get('/admin/news/:id', admin_controller.single_news);
router.get('/admin/delete_news/:id', admin_controller.delete_news_process);
// MESSAGE
router.get('/admin/message', admin_controller.message);
router.get('/admin/delete_message/:id', admin_controller.delete_message_process);
//ARTICLE
router.get('/admin/article', admin_controller.article);
router.post('/admin/article', upload.single('image'),admin_controller.create_article_process);
router.post('/admin/update_article', upload.single('image'),admin_controller.update_article_process);
router.get('/admin/article/:id', admin_controller.single_article);
router.get('/admin/delete_article/:id', admin_controller.delete_article_process);
//REWARD
router.get('/admin/reward', admin_controller.reward);
router.post('/admin/reward', upload.single('image'),admin_controller.create_reward_process);
router.post('/admin/update_reward', upload.single('image'),admin_controller.update_reward_process);
router.get('/admin/reward/:id', admin_controller.single_reward);
router.get('/admin/delete_reward/:id', admin_controller.delete_reward_process);
//SURVEY PROFILING
router.get('/admin/survey_profiling', admin_controller.survey_profiling);
router.post('/admin/survey_profiling', admin_controller.create_survey_profiling_process);
router.post('/admin/update_survey_profiling', admin_controller.update_survey_profiling_process);
router.get('/admin/survey_profiling/:id', admin_controller.single_survey_profiling);
router.get('/admin/delete_survey_profiling/:id', admin_controller.delete_survey_profiling_process);
// QUEST
router.get('/admin/quest', admin_controller.quest);
router.get('/admin/create_quest', admin_controller.create_quest);
router.post('/admin/create_quest', admin_controller.create_quest_process);
router.get('/admin/edit_quest/:id', admin_controller.edit_quest);
router.get('/admin/view_quest/:id', admin_controller.view_quest);
router.post('/admin/edit_quest/:id', admin_controller.update_quest_process);
router.get('/admin/quest/:id', admin_controller.single_quest);
router.get('/admin/delete_quest/:id', admin_controller.delete_quest_process);
router.get('/admin/download_answer/:id', admin_controller.download_answer);
router.post('/admin/preview_quest', admin_controller.preview_quest_process);
//ORDER
router.get('/admin/order', admin_controller.order);
router.post('/admin/order',admin_controller.create_order_process);
router.post('/admin/update_order', upload.single('image'),admin_controller.update_order_process);
router.get('/admin/order/:id', admin_controller.single_order);
router.get('/admin/delete_order/:id', admin_controller.delete_order_process);

// //// ------------- MEMBER --------------- ////
// router.get('/member', member_controller.my_profile);

// router.get('/member/salary', member_controller.salary);
// // router.post('/member/salary', member_controller.salary_create_process);
// // router.post('/member/salary_edit',member_controller.salary_edit_process);
// // router.get('/member/salary_delete/:id', member_controller.salary_delete_process);
// // router.get('/member/salary/:id', member_controller.single_salary);

// router.get('/member/payslip/:id', member_controller.single_salary_payslip);


//// ------------- END OF  ADMIN--------------- ////
module.exports = router;
