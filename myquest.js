const express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const uuid = require('uuid/v4')
const bodyParser = require('body-parser');
const session = require('express-session');
var device = require('express-device');
var flash = require('connect-flash');
var async = require('async');
var hbs = require('hbs');
var fs = require('fs');
var moment = require('moment')
var https = require('https')
var options = {
  key: fs.readFileSync('./assets/ssl/myquest.key'),
  cert: fs.readFileSync('./assets/ssl/myquest.crt')
};


// Redirect from http port 80 to https
var http = require('http');
var indexRouter = require('./routes/index');

// initialize our express app
const app = express();
app.use(device.capture());
// Tell the bodyparser middleware to accept more data
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost/myquest';
// let dev_db_url = 'mongodb://myquest:myquest123@ds237868.mlab.com:37868/myquest';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
global.appName = "MyQuest";
global.webURL = "https://myquest.id/";
// global.webURL = "http://localhost:5000/";

hbs.registerHelper('dateFormat', require('handlebars-dateformat'));

hbs.registerHelper('formatCurrency', function (value) {
  var val = "" + value;
  return val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
});

hbs.registerHelper('formatCurrency0', function (value) {
  if (!value) return 0
  var val = value.toFixed(0)
  return val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
});

hbs.registerHelper('ifEquals', function (arg1, arg2, options) {
  return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('ifMoreThan', function (arg1, arg2, options) {
  return (arg1 > arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('ifNotNull', function (arg1, arg2, options) {
  return (arg1 != null || arg1 != "") ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('ifArrayNotEmpty', function (arg1, options) {
  return (arg1.length > 0) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('notEquals', function (arg1, arg2, options) {
  return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('times', function (n, block) {
  var accum = '';
  for (var i = 0; i < n; ++i)
    accum += block.fn(i);
  return accum;
});

hbs.registerHelper('ifDoEquals', function (arg1, arg2) {
  return (arg1.toString() == arg2.toString()) ? true : false;
});

hbs.registerHelper('ifNotEquals', function (arg1, arg2) {
  return (arg1.toString() != arg2.toString()) ? true : false;
});

hbs.registerHelper('ifMoreThan', function (arg1, arg2, options) {
  return (arg1.toString() > arg2.toString()) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('title', function (strings) {
  return strings.toString().replace('_', ' ').replace(/\b(\w)/g, s => s.toUpperCase());
});

hbs.registerHelper('limitText', function (strings) {
  return strings.substr(0, 50)
});

hbs.registerHelper('toString', function (arrays) {
  return arrays.toString()
});

hbs.registerHelper('substract', function (number, value) {
  return parseInt(number) - value
});

hbs.registerHelper('plus', function (number, value) {
  return parseInt(number) + value
});


hbs.registerHelper('countArray', function (array) {
  if (Array.isArray(array)) return array.length
  else return 0
});

hbs.registerHelper('stringify', function (obj) {
  var ret = JSON.stringify(obj)
  if(ret=="[]")
  return ""
  else return ret
});

hbs.registerHelper('unselectize', function (obj) {
  var ret = ""
  if(obj.length>0){
    obj.map((o,i)=>{
      if(i<(obj.length-1)){
        
      ret+=o+","
      console.log(i)
      }
      else 
      ret+=o
    })
  }
  return ret
});


hbs.registerHelper('codeURL', function (string) {
  return webURL + "quest/" + string
});



hbs.registerHelper('profiling', function (obj_in) {
  var objs = []
  obj_in.map((o => {
    if (o.minValue) {
      var obj = {
        _id: o.surveyprofiling._id.toString(),
        id: o._id.toString(),
        maxValue: o.maxValue,
        minValue: o.minValue
      }
    } else {
      var obj = {
        _id: o.surveyprofiling._id.toString(),
        id: o._id.toString(),
        value: o.value
      }
    }
    objs.push(obj)
  }))
  return JSON.stringify(objs)
});

hbs.registerHelper('question', function (obj_in) {
  var objs = []
  obj_in.map((o => {
    if (o.data) {
      var obj = {
        id: o.id.toString(),
        question: o.question,
        type: o.type,
        required: o.required.toString(),
        data: o.data.toString()
      }
    } else {
      var obj = {
        id: o.id.toString(),
        question: o.question,
        type: o.type,
        required: o.required.toString()
      }
    }
    objs.push(obj)
  }))
  return JSON.stringify(objs)
});

hbs.registerHelper('findProfiling', function (id, profiling) {
  var answer = ""
  profiling.map((o) => {
    if (o.surveyProfiling.toString() == id.toString()) answer = o.answer
  })
  return answer
});


hbs.registerHelper('findAnswer', function (id, answers) {
  var selected = ""
  if (answers) {
    answers.map((o) => {
      console.log(o, "DISINI")
      if (o.id.toString() == id.toString()) {
        selected = o.answer
      }
    })
  }
  return selected
});

hbs.registerHelper('getLogic', function (logicList, current_device) {
  var logic = ""
  if (Array.isArray(logicList)) {

    logicList.map((o, i) => {
      if (i > 0) {
        console.log(o)
        var logic_type = o.logic_type.toString()
        var is_not = o.is_not.toString()
        var is_selected = o.is_selected.toString()
        var device = o.device.toString()
        var equal = o.equal.toString()
        var equal_to = o.equal_to.toString()
        var question = o.question.toString()
        var or_and = o.or_and.toString()
        var answer = o.answer.toString()
        var device = o.device.toString()
        var logic_temp = ""
        //Set Or And
        if (logic_type == "question") {
          if (equal != null & equal != "") {
            if (equal == "is_equal_to") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val().toString() == "`+equal_to+`"`
            } else if (equal == "is_not_equal_to") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() != "`+equal_to+`"`
            } else if (equal == "is_greater_than") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() > "`+equal_to+`"`
            } else if (equal == "is_greater_than_or_equal_to") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() >= "`+equal_to+`"`
            } else if (equal == "is_less_than") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() < "`+equal_to+`"`
            } else if (equal == "is_les_than_or_equal_to") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() <= "`+equal_to+`"`
            } else if (equal == "is_empty") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() == ""`
            } else if (equal == "is_not_empty") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() != ""`
            } else if (equal == "is_contains") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val().toString().includes("`+equal_to+`")`
            } else if (equal == "is_not_contains") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`!$("#`+question+`").val().toString().includes("`+equal_to+`")`
            }
          } else {
            if (is_selected == "is_selected") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() == "`+answer+`"`
            } else if (is_selected == "is_not_selected") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#`+question+`").val() != "`+answer+`"`
            } else if (is_selected == "is_displayed") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#id_" + "`+question+`").is(":visible")`
            } else if (is_selected == "is_not_displayed") {
              if(or_and==="and") logic_temp+=" && "
              else logic_temp+=" || "
              logic_temp+=`$("#id_" + "`+question+`").is(":hidden")`
            }
          }
        } else {
          console.log("MASUP")
          if (is_not == "is_not") {
            console.timeLog("here")
            if(or_and==="and") logic_temp+=" && "
            else logic_temp+=" || "
            logic_temp+=`current_device != "`+device+`"`
            if (current_device != device) tab.addClass("tab")
            else tab.removeClass("tab")
          } else if (is_not == "is" ) {
            console.log("here")
            if(or_and==="and") logic_temp+=" && "
            else logic_temp+=" || "
            logic_temp+=`current_device == "`+device+`"`
          }
        }
        logic+=logic_temp
      }
    })
  }
  return logic
});

hbs.registerHelper('findAnswerFile', function (id, answers) {
  var selected = ""
  if (answers) {
    answers.map((o) => {
      console.log(o, "DISINI")
      if (o.id.toString() == id.toString()) {
        selected = o.answer
      }
    })
  }
  return selected.substring(6);
});

hbs.registerHelper('findProfilingSpecial', function (id, data, profiling, answer) {
  var selected = ""
  profiling.map((o) => {
    if (o.surveyProfiling.toString() == id.toString()) {
      o.answer = o.answer.toString().split(",")
      o.answer.map((p) => {
        if (p == data)
          selected = answer
      })
    }
  })
  return selected
});

hbs.registerHelper('findAnswer_special', function (id, answers, answer) {
  var selected = ""
  answers.map((o) => {

    if (o.id.toString() == id.toString()) {
      o.answer.map(p => {
        if (p.toString() == answer)
          selected = answer
      })
    }
  })
  return selected
});



hbs.registerHelper('minutesToHours', function (val) {
  var time = val
  var minutes = time % 60
  var hours = (time - minutes) / 60
  if (minutes <= 9) minutes = "0" + minutes
  return hours + ":" + minutes
});

hbs.registerHelper('renderRequirements', function (type, payload) {
  console.log(type, payload)
  var val = ''
  if (type == 'product') val = payload.productRef.name
  if (type == 'product_group') val = payload.productGroupRef.name
  if (type == 'product_category') val = payload.productCategoryRef.name
  if (type == 'visit_day') val = payload.visitDay
  if (type == 'minimum_payment') val = 'Rp.' + payload.minimumPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
  if (type == 'number_of_visit') val = payload.numberOfVisit

  type = type.split('_').join(' ')
  return `<h6><span class="badge badge-pill badge-primary mb-1 capitalize">` + type + `: ` + val + `</span></h6>`
});

hbs.registerHelper('shuffleArray', function (a){
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
})

hbs.registerHelper('carryForward', function (question, type){
  var obj = {
    type:type,
    question:question
  }
  return obj;
})


hbs.registerHelper('eachShuffle', function(ary, options) {
  if(!ary || ary.length === 0) {
      return options.inverse(this);
  }
  var data;
  if (options.data) {
      data = hbs.createFrame(options.data);
  }
  var j, x, i;
  //randomize
  for (i = ary.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = ary[i];
      ary[i] = ary[j];
      ary[j] = x;
  }
  var result = [];
  for(var i = 0; i < ary.length; ++i) {
      if (data) {
          data.index = i;
      }
      result.push(options.fn(ary[i], {data: data}));
  }

  return result.join('');
})


//App Use
app.use(flash());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
// app.use(session({
//   genid: (req) => {
//     console.log('Inside the session middleware')
//     console.log(req.sessionID)
//     return uuid() // use UUIDs for session IDs
//   },
//   key: 'myquest',
//   secret: '8c6bf6df17820b7866af63de5d222c55s',
//   cookie: {
//     maxAge: new Date(Date.now() + (1440 * 1000 * 60))
//   }
// }));

app.use(session({
  key: 'myquest',
  secret: '8c6bf6df17820b7866af63de5d222c55s',
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 600000
  }
}));
app.use('/', indexRouter);
app.use(express.static(path.join(__dirname, 'assets')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');
app.use(express.static('assets'));
app.use(cookieParser());
let port = 5000;

// https.createServer(options, function (req, res) {
//   res.end('secure!');
// }).listen(5000);
// http.createServer(function (req, res) {
//     res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
//     res.end();
// }).listen(80);
app.listen(port, () => {
  console.log('Server is up and running on port numner ' + port);
});