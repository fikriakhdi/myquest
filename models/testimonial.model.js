const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let TestimonialScheme = new Schema ({
  name: String,
  caption: String,
  image: String,
  stars: String,
  confirmed:Boolean,
  userRef: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});


// Export the model
module.exports = mongoose.model('testimonial', TestimonialScheme)