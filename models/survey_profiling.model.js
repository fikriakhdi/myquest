const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SurveyProfilingScheme = new Schema ({
    question:String,
    type:{
        type: String,
        enum: ['text', 'number', 'select', 'date', 'multiple', 'radio', 'textarea', 'file', 'email', 'checkbox']
    },
    data:[String],
    required: Boolean,
    createdDate: Date,
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    updatedDate: Date,
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
});


// Export the model
var User = mongoose.model('surveyprofiling', SurveyProfilingScheme)
module.exports = User