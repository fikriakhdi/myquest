const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FeatureScheme = new Schema({
  name : String,
  description:String,
  imageUrlLogo: String,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

// Export the model
module.exports = mongoose.model('feature', FeatureScheme );
