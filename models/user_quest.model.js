const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let UserQuestScheme = new Schema({
  userRef: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  questRef:{
    type: Schema.Types.ObjectId,
    ref: 'quest'
  },
  answers: [{
    id:String,
    answer:String
  }],  
  respondent:{
    type: String,
    enum: ['member', 'anonymous'],
  },
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

// Export the model
module.exports = mongoose.model('userquest', UserQuestScheme );
