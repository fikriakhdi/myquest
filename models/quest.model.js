const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let QuestScheme = new Schema({
  code: String,
  userRef:{
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  number : String,
  title : String,
  description:String,
  questions :[{
    id:String,
    question:String,
    type:{
      type: String,
      enum: ['text', 'number', 'select', 'date', 'multiple', 'radio', 'textarea', 'file', 'email', 'checkbox']
    },
    data:[String],
    required: Boolean,
    number:String,
    custom:Boolean,
    pageBreak:Boolean,
    sequence:Number,
    carryForwardQuestion:String,
    carryForwardType:String,
    randomize:Boolean,
    displayLogic: [{
      logic_type:String,
      question :String,
      is_not: String,
      is_selected :String,
      device:String,
      equal : String,
      equal_to: String,
      answer: String,
      or_and: String,
    }]
  }],
  profiling:[{
    surveyprofiling:{
    type: Schema.Types.ObjectId,
    ref: 'surveyprofiling'
  },
  value:String,
  maxValue:Number,
  minValue:Number}],
  backgroundImage: String,
  status:{
    type: String,
    enum: ['drafted', 'published', 'unpublished', 'canceled']
  },
  type:{
    type: String,
    enum: ['quest', 'dummy']
  },
  questType:{
    type: String,
    enum: ['free', 'paid']
  },
  orderRef:{
    type: Schema.Types.ObjectId,
    ref: 'order'
  },
  targetRespondent:Number,
  totalRespondent:Number,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

// Export the model
module.exports = mongoose.model('quest', QuestScheme );
