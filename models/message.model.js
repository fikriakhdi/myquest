const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MessageScheme = new Schema({
  name : String,
  email : String,
  subject : String,
  message:String,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

// Export the model
module.exports = mongoose.model('message', MessageScheme );
