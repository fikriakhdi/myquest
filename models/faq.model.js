const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FaqScheme = new Schema({
  question : String,
  answer:String,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

// Export the model
module.exports = mongoose.model('faq', FaqScheme );
