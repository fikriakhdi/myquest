const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let NewsSchema = new Schema ({
  title: String,
  subtitle: String,
  content: String,
  image: String,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});


// Export the model
module.exports = mongoose.model('news', NewsSchema)