const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SettingScheme = new Schema ({
  logo:String,
  phone: Number,
  address: String,
  email: String,
  headerTitle: String,
  headerCaption: String,
  headerImage: String,
  aboutUsTitle: String,
  aboutUsCaption: String,
  aboutUsImage: String,
  facebook:String,
  twitter:String,
  instagram:String,
  youtube:String,
  linkedin:String,
  termsAndCondition: String,
  prologue: String,
  price:Number,
  googleAnalytic:String,
  freeQuest:Number,
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});


// Export the model
var User = mongoose.model('setting', SettingScheme)
module.exports = User