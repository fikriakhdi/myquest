const mongoose = require('mongoose');
var crypto = require('crypto');
const Schema = mongoose.Schema;

let UserSchema = new Schema ({
  name: String,
  email: {
    type: String,
    unique: true
  },
  password: String,
  role: {
    type: String,
    enum: ['member', 'admin'],
  },
  imageProfile: String,  
  profiling:[
    {
    surveyProfiling:{
    type: Schema.Types.ObjectId,
    ref: 'surveyprofiling'},
    answer:String
    }
  ],
  status: {
    type: String,
    enum: ['active', 'unactive'],
    default: 'active'
  },
  createdDate: Date,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },
  updatedDate: Date,
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
});

UserSchema.statics.authenticate = (email, password, role, callback) => {
  password = crypto.createHash('md5').update(password).digest('hex');
  console.log(email, password)
  User.findOne({'email': email.toString(), 'password':password.toString(), 'role':role})
    .exec(function (err, user) {
      // console.log('auth:',err,user)
      if (err || !user ) {
          return callback(null, err)
      } else {
          return callback(null, user)
      }
  })
};


// Export the model
var User = mongoose.model('user', UserSchema)
module.exports = User